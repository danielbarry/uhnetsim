

import java.util.*;

public interface UnitGridListener extends EventListener {
    void unitGridPixelSizeChange (UnitGridEvent e);
    void unitGridUnitChanged (UnitGridEvent e);
}