


import javax.swing.*;
import java.io.*;

public class Experiments implements ProgressReportor{

    NetSimFrame parent = null;


    Experiments(NetSimFrame parent){
        this.parent = parent;
    }


    public void pruningTest(){
        String prompt = "Please enter values for the following experiment options, seperated by semi-colons ';'\n"
                        + "Initial pruning coefficient, Final pruning coefficient, Pruning increment\n"
                        + "For example : 0.1; 0.9; 0.1";
        String params       = null;
        String delimiter    = ";";
        float initial       = 0.0f;
        float maximum       = 0.0f;
        float increment     = 0.0f;
        String confirmation = null;
        int confirm         = JOptionPane.NO_OPTION;

        if(parent.getTestSet().numPatterns() <= 0){
            JOptionPane.showMessageDialog(parent, "No test pattern available!", "Pruning test", JOptionPane.ERROR_MESSAGE);
            return;
        }

        do{
            params = JOptionPane.showInputDialog(parent, prompt, "Pruning test", JOptionPane.QUESTION_MESSAGE);
            if(params == null) return;
            try{
                initial = Float.parseFloat(params.substring(0, params.indexOf(delimiter)).trim());
                params = params.substring(params.indexOf(delimiter) + 1);
                maximum = Float.parseFloat(params.substring(0, params.indexOf(delimiter)).trim());
                increment = Float.parseFloat(params.substring(params.indexOf(delimiter) + 1).trim());

                confirmation = "The experiment will use the following parameter values;"
                            + "\nInitial pruning : " + formatDouble(initial)
                            + "\nFinal pruning : " + formatDouble(maximum)
                            + "\nPruning increment : " + formatDouble(increment);
                confirm = JOptionPane.showConfirmDialog(parent, confirmation, "Pruning test", JOptionPane.YES_NO_CANCEL_OPTION);

            }catch(NumberFormatException e){
                JOptionPane.showMessageDialog(parent, "Invalid number format!", "Experiment parameters", JOptionPane.ERROR_MESSAGE);
            }
        }while(confirm == JOptionPane.NO_OPTION);

        if( confirm == JOptionPane.YES_OPTION ){
            int numWeights = parent.getNetwork().numUnits()*parent.getNetwork().numUnits();
            double[] weights = new double[numWeights];

            for(int i = 0; i < numWeights; i++){
                weights[i] = parent.getNetwork().getWeight(i);
            }

            testPruning(initial, maximum, increment);

            for(int i = 0; i < numWeights; i++){
                parent.getNetwork().setWeight(i, weights[i]);
            }
        }
    }


    //-----------------------------------
    // p		w-
    // iTest	iTrain	HD		X
    // p		w-		<3		3-10		>10
    public void testPruning(float lowerP, float upperP, float stepP){

        int numTestPatterns = parent.getTestSet().numPatterns();
      	SummaryReport summary20 = new SummaryReport();
      	SummaryReport summary33 = new SummaryReport();

        int[] borders = {3, 10};
		summary20.setClassBorders(borders);
		summary33.setClassBorders(borders);

        parent.getNetwork().setRunOperation(Network.RUN);

        for(float p = lowerP; p <= upperP; p += stepP){
            parent.getPrunedNetwork().pruneWeakRemote(p);
            parent.getPrunedNetwork().apply();
			float[] entry = {p, (float)parent.getPrunedNetwork().getLengthReduction()};
            summary20.addLine(entry);
            summary33.addLine(entry);
            System.out.println("coefficient : " + entry[0] + "\t wiring reduction : " + entry[1]);

            for(int j = 0; j < numTestPatterns; j++){
                if( parent.getTestSet().getTrainingPatternIndex(j) < 0 ) continue;

                parent.getTestSetDisplay ().setCurrentPattern(j);
                parent.getTestSetDisplay ().makeCurrentState();

                System.out.print( "runnning " + j );
                parent.getNetwork().run();
				int numIncorrect = parent.getNetDisplay ().hiLightIncorrectStates();
                System.out.println("\t" + parent.getTestSet().getTrainingPatternIndex(j)
                    + "\t" + parent.getTestSet().getHammingDistance(j)
                    + "\t" + numIncorrect);
                if( parent.getTestSet().getHammingDistance(j)==20 ){
					summary20.addData(numIncorrect);
                }else{
					summary33.addData(numIncorrect);
                }
            }
        }
		System.out.println("\n" + summary20.toString() + "\n" + summary33.toString());
        parent.getOutputFrame().setText("Pruning test result" + "\n");
        parent.getOutputFrame().appendText("coefficient\tlength-\t<3HD\t3-10HD\t>10HD" + "\n");
        parent.getOutputFrame().appendText(summary20.toString() + "\n");
        parent.getOutputFrame().appendText(summary33.toString());
    }


    public void overloadingAnalysis(){
        String prompt = "Please enter values for the following experiment options, seperated by semi-colons ';'\n"
                        + "No. of runs, Initial No. of patterns, Final No. of patterns, Pattern(s) increment, Experiment name\n"
                        + "For example : 10; 2; 10; 1; Hopfield";
        String params       = null;
        String delimiter    = ";";
        int numRuns         = 0;
        int initial         = 0;
        int maximum         = 0;
        int increment       = 0;
        String name         = null;
        String confirmation = null;
        int confirm         = JOptionPane.NO_OPTION;

        do{
            params = JOptionPane.showInputDialog(parent, prompt, "Overloading analysis", JOptionPane.QUESTION_MESSAGE);
            if(params == null) return;
            try{
                numRuns = Integer.parseInt(params.substring(0, params.indexOf(delimiter)).trim());
                params = params.substring(params.indexOf(delimiter) + 1);
                initial = Integer.parseInt(params.substring(0, params.indexOf(delimiter)).trim());
                params = params.substring(params.indexOf(delimiter) + 1);
                maximum = Integer.parseInt(params.substring(0, params.indexOf(delimiter)).trim());
                params = params.substring(params.indexOf(delimiter) + 1);
                increment = Integer.parseInt(params.substring(0, params.indexOf(delimiter)).trim());
                name = params.substring(params.indexOf(delimiter) + 1).trim();

                confirmation = "The experiment will use the following parameter values;"
                            + "\nNo. of runs : " + numRuns
                            + "\nInitial No. of patterns : " + initial
                            + "\nFinal No. of patterns : " + maximum
                            + "\nPattern(s) increment : " + increment
                            + "\nExperiment name : " + name;
                confirm = JOptionPane.showConfirmDialog(parent, confirmation, "Overloading analysis", JOptionPane.YES_NO_CANCEL_OPTION);

            }catch(NumberFormatException e){
                JOptionPane.showMessageDialog(parent, "Invalid number format!", "Experiment parameters", JOptionPane.ERROR_MESSAGE);
            }
        }while(confirm == JOptionPane.NO_OPTION);

        if( confirm == JOptionPane.YES_OPTION ){
            analyseOverloading(numRuns, initial, maximum, increment, name);
        }
    }


    public void analyseOverloading(int numRuns, int initialPatterns, int finalPatterns, int increment, String name){

        int numTraining = (finalPatterns-initialPatterns) / increment + 1;
        double[][] avg = new double[numTraining * 2][Dynamic.MAXEPOCH + 2];
        String log = "";

        for(int N = 0; N < numRuns; N++){
            for(int i = 0; i < numTraining; i++){
                if(i == 0){
                    removeAllPatterns();
                    parent.getPatternGenerator().generatePattern(initialPatterns, 0.5f, false);
                }else{
                    parent.getPatternGenerator().generatePattern(increment, 0.5f, false);
                }
                parent.getNetwork().train();

                String heading = "\n" + parent.getTrainingSet().numPatterns() + " patterns / " + parent.getNetwork().numUnits() + " units\n";
                System.out.print(heading);

                Dynamic dyn = new Dynamic(parent, this);
                dyn.calculate();
                log += heading;
                log += dyn.getSummary();

                avg[i*2][0] = (avg[i*2][0] * N + dyn.numParasiticAttractors)/(N+1);
                avg[i*2+1][0] = (avg[i*2+1][0] * N + dyn.numTrainedAttractors)/(N+1);
                for(int j = 0; j <= Dynamic.MAXEPOCH; j++){
                    avg[i*2][j+1] = (avg[i*2][j+1] * N + dyn.summary[0][j]) / (N+1);
                    avg[i*2+1][j+1] = (avg[i*2+1][j+1] * N + dyn.summary[1][j]) / (N+1);
                }
                dyn = null;
            }
            saveLog(log, name + N);
            log = "";
        }
        log = "#P, Type, #Attractors, Total, 1Epoch, 2Epoch, ...\n";
        for(int i = 0, numPatterns = initialPatterns; i < avg.length; i++){
            if( (i%2) == 0 ){
                log += numPatterns + ", Parasitic";
            }else{
                log += numPatterns + ", Trained";
                numPatterns += increment;
            }
            for(int j = 0; j < avg[0].length && avg[i][j] != 0.0d; j++){
                log += (", " + formatDouble(avg[i][j]));
            }
            log += "\n";
        }
        saveLog(log, name);
        parent.getOutputFrame().setText(log);
    }


    public void reportProgress(String s){
        System.out.print(">");
    }


    private void removeAllPatterns(){
        while(parent.getTrainingSet().numPatterns() > 0){
            parent.getTrainingSet().removePattern(0);
        }
    }


    private void saveLog(String log, String fileName){
        File file = new File(".." + File.separatorChar +"Data" + File.separatorChar + fileName + ".log");
        try{
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(log);
            fileWriter.close();
        }catch(IOException e){
            System.out.println("Can't open file : " + file.getAbsolutePath());
            System.out.println(log);
        }
    }


    public void pruningAnalysis(){
        String prompt = "Please enter values for the following experiment options, seperated by semi-colons ';'\n"
                        + "No. of runs, No. of patterns, Initial pruning, Final pruning, Pruning increment, Experiment name\n"
                        + "For example : 10; 2; 0.2; 0.8; 0.1; Hopfield";
        String params       = null;
        String delimiter    = ";";
        int numRuns         = 0;
        int numPatterns     = 0;
        float initial       = 0.0f;
        float maximum       = 0.0f;
        float increment     = 0.0f;
        String name         = null;
        String confirmation = null;
        int confirm         = JOptionPane.NO_OPTION;

        do{
            params = JOptionPane.showInputDialog(parent, prompt, "Pruning analysis", JOptionPane.QUESTION_MESSAGE);
            if(params == null) return;
            try{
                numRuns = Integer.parseInt(params.substring(0, params.indexOf(delimiter)).trim());
                params = params.substring(params.indexOf(delimiter) + 1);
                numPatterns = Integer.parseInt(params.substring(0, params.indexOf(delimiter)).trim());
                params = params.substring(params.indexOf(delimiter) + 1);
                initial = Float.parseFloat(params.substring(0, params.indexOf(delimiter)).trim());
                params = params.substring(params.indexOf(delimiter) + 1);
                maximum = Float.parseFloat(params.substring(0, params.indexOf(delimiter)).trim());
                params = params.substring(params.indexOf(delimiter) + 1);
                increment = Float.parseFloat(params.substring(0, params.indexOf(delimiter)).trim());
                name = params.substring(params.indexOf(delimiter) + 1).trim();

                confirmation = "The experiment will use the following parameter values;"
                            + "\nNo. of runs : " + numRuns
                            + "\nNo. of patterns : " + numPatterns
                            + "\nInitial pruning : " + formatDouble(initial)
                            + "\nFinal pruning : " + formatDouble(maximum)
                            + "\nPruning increment : " + formatDouble(increment)
                            + "\nExperiment name : " + name;
                confirm = JOptionPane.showConfirmDialog(parent, confirmation, "Pruning analysis", JOptionPane.YES_NO_CANCEL_OPTION);

            }catch(NumberFormatException e){
                JOptionPane.showMessageDialog(parent, "Invalid number format!", "Experiment parameters", JOptionPane.ERROR_MESSAGE);
            }
        }while(confirm == JOptionPane.NO_OPTION);

        if( confirm == JOptionPane.YES_OPTION ){
            analysePruning(numRuns, numPatterns, initial, maximum, increment, name);
        }
    }


    public void analysePruning(int numRuns, int numPatterns, float initialPruning, float finalPruning, float increment, String name){
        PrunedNetwork pNW = parent.getPrunedNetwork();
        String[] rules = parent.getPruneDisplay().getPruningRules();
        int numPruning = (int)((finalPruning - initialPruning) / increment) + 1;

        parent.getOutputFrame().clear();
        for(int rule = 0; rule < rules.length; rule++){
            double[][] avgReduction = new double[numPruning][3];
            double[][] avgAttraction = new double[numPruning * 2][Dynamic.MAXEPOCH + 2];
            String log = "";

            for(int N = 0; N < numRuns; N++){
                removeAllPatterns();
                parent.getPatternGenerator().generatePattern(numPatterns, 0.5f, false);
                parent.getNetwork().train();
                float p = initialPruning;
                for(int i = 0; i < numPruning; i++, p += increment){
                    pNW.prune(rule, p);
                    pNW.apply();

                    String heading = "\n" + formatDouble(p) + " p ( "
                        + formatPercentage(pNW.axonalReduction) + " Axonal-" + ", "
                        + formatPercentage(pNW.lengthReduction) + " Length-" + ", "
                        + formatPercentage(pNW.synapticReduction) + " Synaptic- )\n";
                    System.out.print(heading);

                    Dynamic dyn = new Dynamic(parent, this);
                    dyn.calculate();

                    log += heading;
                    log += dyn.getSummary() + "\n";

                    avgReduction[i][0] = (avgReduction[i][0] * N + pNW.axonalReduction) / (N+1);
                    avgReduction[i][1] = (avgReduction[i][1] * N + pNW.lengthReduction) / (N+1);
                    avgReduction[i][2] = (avgReduction[i][2] * N + pNW.synapticReduction) / (N+1);
                    avgAttraction[i*2][0] = (avgAttraction[i*2][0] * N + dyn.numParasiticAttractors) / (N+1);
                    avgAttraction[i*2+1][0] = (avgAttraction[i*2+1][0] * N + dyn.numTrainedAttractors) / (N+1);
                    for(int j = 0; j <= Dynamic.MAXEPOCH; j++){
                        avgAttraction[i*2][j+1] = (avgAttraction[i*2][j+1] * N + dyn.summary[0][j]) / (N+1);
                        avgAttraction[i*2+1][j+1] = (avgAttraction[i*2+1][j+1] * N + dyn.summary[1][j]) / (N+1);
                    }
                    dyn = null;
                }
                log = "Pruning rule : " + rules[rule] + "\n\n" + log;
                saveLog(log, rule + name + N);
                log = "";
            }
            log = "Pruning rule : " + rules[rule] + "\n";
            log += "Coefficient, Type, Axonal-, Length-, Synaptic-, #Attractors, Total, 1Epoch, 2Epoch, ...\n";
            float p = initialPruning;
            for(int i = 0; i < avgAttraction.length; i++){
                if( (i%2) == 0 ){
                    log += formatDouble(p) + ", Parasitic";
                }else{
                    log += formatDouble(p) + ", Trained";
                    p += increment;
                }
                log += ", " + formatDouble(avgReduction[i/2][0])
                    + ", " + formatDouble(avgReduction[i/2][1])
                    + ", " + formatDouble(avgReduction[i/2][2]);
                for(int j = 0; j < avgAttraction[0].length && avgAttraction[i][j] != 0.0d; j++){
                    log += (", " + formatDouble(avgAttraction[i][j]));
                }
                log += "\n";
            }
            saveLog(log, rule + name);
            parent.getOutputFrame().appendText(log + "\n");
        }
    }


    public static String formatPercentage(double d){
         return formatDouble(d) + " %";
    }


    public static String formatDouble(double d){
        int whole, part;
        whole = (int)d;
        part = ((int)(d*100)) - (whole*100);
        return whole + "." + part;
    }

}
