

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

public class TrainingSetDisplay extends JInternalFrame implements UnitGridListener {
    private JToolBar tlbControl     = null;
    private JButton btnSmaller      = null;
    private JButton btnLarger       = null;
    private JButton btnAdd          = null;
    private JButton btnRemove       = null;
    private JButton btnGenerate     = null;
    private JButton btnPrevious     = null;
    private JButton btnNext         = null;
    private JButton btnCurrent      = null;
    private JTextField txfStatusBar = null;
    private NetSimFrame parent      = null;
    private UnitGrid unitGrid       = null;
    private int currentPattern      = 0;

    //-----------------------------------
    public TrainingSetDisplay (NetSimFrame parent) {
        super ("Training Set", false, false, false, true);

        this.parent = parent;
        this.initToolBar ();
        this.initStatusBar ();
        this.initTrainingSetDisplay ();
        this.pack ();
    }

    //-----------------------------------
    public void unitGridPixelSizeChange (UnitGridEvent e) {
        this.setSize (0, 0);
        this.pack ();
    }

    //-----------------------------------
    public void unitGridUnitChanged (UnitGridEvent e) {
        int unitIndex = (e.unitY * this.unitGrid.getUnitDimensions ().width) + e.unitX;

        // this code just changes a single unit
        this.parent.getTrainingSet ().toggleUnit (this.currentPattern, unitIndex);
    }

    //-----------------------------------
    private void initToolBar () {
        this.tlbControl = new JToolBar (JToolBar.HORIZONTAL);
        String resPath = System.getProperty("user.dir") + "/res/";
        try {
            this.btnSmaller = new JButton (new ImageIcon (resPath + "ZoomOut16.gif"));
            this.btnLarger = new JButton (new ImageIcon (resPath + "ZoomIn16.gif"));
            this.btnAdd = new JButton (new ImageIcon (resPath + "New16.gif"));
            this.btnRemove = new JButton (new ImageIcon (resPath + "Remove16.gif"));
            this.btnGenerate = new JButton (new ImageIcon (resPath + "Generate16.gif"));
            this.btnPrevious = new JButton (new ImageIcon (resPath + "Back16.gif"));
            this.btnNext = new JButton (new ImageIcon (resPath + "Forward16.gif"));
            this.btnCurrent = new JButton (new ImageIcon (resPath + "Import16.gif"));
        }catch(NullPointerException e){ System.err.println("Done fuqed up."); }
        
        this.btnSmaller.setToolTipText ("Zoom out");
        this.btnLarger.setToolTipText ("Zoom in");
        this.btnAdd.setToolTipText ("Add pattern");
        this.btnRemove.setToolTipText ("Remove pattern");
        this.btnGenerate.setToolTipText ("Generate patterns");
        this.btnPrevious.setToolTipText ("Previous pattern");
        this.btnNext.setToolTipText ("Next pattern");
        this.btnCurrent.setToolTipText ("Make current state");

		this.btnSmaller.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                unitGrid.decreaseUnitPixelSize ();
			}
		});

		this.btnLarger.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
				unitGrid.increaseUnitPixelSize ();
			}
		});

        this.btnAdd.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
/*
                // correct the currentPattern value if the trainingSet is empty
                if (currentPattern < 0) currentPattern = 0;
*/
                // add the new pattern to the set
                parent.getTrainingSet ().addPattern ();
                currentPattern = parent.getTrainingSet().numPatterns()-1;
                unitGrid.setUnits (parent.getTrainingSet ().getBooleanPattern (currentPattern));
                refreshStatusBar ();
                refreshUnitGrid ();

                // make sure appropriate buttons are enabled
                if (parent.getTrainingSet ().numPatterns () > 1) {
                    btnPrevious.setEnabled (true);
                    btnNext.setEnabled (true);
                }
                btnSmaller.setEnabled (true);
                btnLarger.setEnabled (true);
                btnRemove.setEnabled (true);
                btnGenerate.setEnabled (true);
                btnCurrent.setEnabled (true);
			}
		});

        this.btnRemove.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                // remove the current pattern from the set
                parent.getTrainingSet ().removePattern (currentPattern);
                // make sure the current pattern is numbered correctly
                if (currentPattern >= parent.getTrainingSet ().numPatterns ()) currentPattern = parent.getTrainingSet ().numPatterns () - 1;

                // if the training set is empty don't try and retrieve currentPattern
                if (parent.getTrainingSet ().numPatterns () != 0) {
                    unitGrid.setUnits (parent.getTrainingSet ().getBooleanPattern (currentPattern));
                }

                // make sure the appropriate buttons are enabled
                if (parent.getTrainingSet ().numPatterns () <= 1) {
                    btnPrevious.setEnabled (false);
                    btnNext.setEnabled (false);
                }
                if (parent.getTrainingSet ().numPatterns () == 0) {
                    btnSmaller.setEnabled (false);
                    btnLarger.setEnabled (false);
                    btnRemove.setEnabled (false);
                    btnPrevious.setEnabled (false);
                    btnNext.setEnabled (false);
                    btnCurrent.setEnabled (false);
                }
                refreshStatusBar ();
                refreshUnitGrid ();
			}
		});

        this.btnGenerate.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                parent.getPatternGenerator ().display ();
            }
        });

        this.btnPrevious.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                showPreviousPattern();
            }
		});

        this.btnNext.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                showNextPattern();
            }
		});

        this.btnCurrent.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                parent.getNetwork ().setUnits (parent.getTrainingSet ().getPattern (currentPattern));
                parent.getNetDisplay ().setUnits (parent.getTrainingSet ().getBooleanPattern (currentPattern));
                parent.getNetDisplay ().refreshUnitGrid ();
                try {
                    parent.getNetDisplay ().setSelected (true);
                } catch (PropertyVetoException e2) {
                    System.out.println ("PropertyVetoException in TrainingSetDisplayFrame.initToolBar ()");
                    Thread.dumpStack ();
                    System.exit (2);
                }
            }
        });

        // add the buttons to the toolbar
        this.tlbControl.add (this.btnSmaller);
        this.tlbControl.add (this.btnLarger);
        this.tlbControl.addSeparator ();
        this.tlbControl.add (this.btnAdd);
        this.tlbControl.add (this.btnRemove);
        this.tlbControl.addSeparator ();
        this.tlbControl.add (this.btnGenerate);
        this.tlbControl.addSeparator ();
        this.tlbControl.add (this.btnPrevious);
        this.tlbControl.add (this.btnNext);
        this.tlbControl.addSeparator ();
        this.tlbControl.add (this.btnCurrent);

        // we don't want people dragging the toolbar around
        this.tlbControl.setFloatable (false);

        // add the toolbar to the frame
        this.getContentPane ().add (this.tlbControl, BorderLayout.NORTH);
    }

    //-----------------------------------
    private void initTrainingSetDisplay () {
        this.unitGrid = new UnitGrid (this.parent.getTrainingSet ().getPatternWidth ());
        this.unitGrid.addUnitGridListener (this);
        this.getContentPane ().add (this.unitGrid, BorderLayout.CENTER);

        // get the first pattern from the training set
        this.unitGrid.setUnits (this.parent.getTrainingSet ().getBooleanPattern (this.currentPattern));
        this.refreshStatusBar ();
        this.refreshUnitGrid ();
    }

    //-----------------------------------
    private void initStatusBar () {
        this.txfStatusBar = new JTextField ();

        // add the statusbar to the frame
        this.getContentPane ().add (this.txfStatusBar, BorderLayout.SOUTH);

        this.txfStatusBar.setEditable (false);
        this.refreshStatusBar ();
    }

    //-----------------------------------
    public void refreshStatusBar () {
        this.txfStatusBar.setText ((this.currentPattern + 1) + "/" + this.parent.getTrainingSet ().numPatterns () + " patterns");
    }

    //-----------------------------------
    public void refreshUnitGrid () {
        if (this.parent.getTrainingSet ().numPatterns () != 0) {
            this.unitGrid.setVisible (true);
            this.unitGrid.repaint ();
        } else {
            this.unitGrid.setVisible (false);
        }
    }

    //-----------------------------------
    public void setEnabled (boolean state) {
        int numPatterns = this.parent.getTrainingSet().numPatterns ();

        this.btnAdd.setEnabled (state);
        this.btnGenerate.setEnabled (state);
        this.btnSmaller.setEnabled (state && numPatterns > 0);
        this.btnLarger.setEnabled (state && numPatterns > 0);
        this.btnRemove.setEnabled (state && numPatterns > 0);
        this.btnCurrent.setEnabled (state && numPatterns > 0);
        this.btnPrevious.setEnabled (state && numPatterns > 1);
        this.btnNext.setEnabled (state && numPatterns > 1);
/*
        if (state == true) {
            this.btnSmaller.setEnabled (true);
            this.btnLarger.setEnabled (true);
            this.btnAdd.setEnabled (true);
            this.btnRemove.setEnabled (true);
            this.btnGenerate.setEnabled (true);
            this.btnPrevious.setEnabled (true);
            this.btnNext.setEnabled (true);
            this.btnCurrent.setEnabled (true);

            if (this.parent.getTrainingSet ().numPatterns () <= 1) {
                this.btnPrevious.setEnabled (false);
                this.btnNext.setEnabled (false);
            } else if (this.parent.getTrainingSet ().numPatterns () == 0) {
                this.btnSmaller.setEnabled (false);
                this.btnLarger.setEnabled (false);
                this.btnRemove.setEnabled (false);
                this.btnCurrent.setEnabled (false);
            }
        } else {
            this.btnSmaller.setEnabled (false);
            this.btnLarger.setEnabled (false);
            this.btnAdd.setEnabled (false);
            this.btnRemove.setEnabled (false);
            this.btnGenerate.setEnabled (false);
            this.btnPrevious.setEnabled (false);
            this.btnNext.setEnabled (false);
            this.btnCurrent.setEnabled (false);
        }
*/
    }

    //-----------------------------------
    public int getCurrentPattern () {
        return this.currentPattern;
    }

    //-----------------------------------
    public void setCurrentPattern (int patternNum) {
        if (patternNum < 0 || patternNum >= this.parent.getTrainingSet ().numPatterns ()) {
            System.out.println ("Attempted to set current pattern to one that does not exist in TrainingSetDisplayFrame.setCurrentPattern ()");
            Thread.dumpStack ();
            System.exit (2);
        }

        this.currentPattern = patternNum;

        // get the requested pattern from the training set
        this.unitGrid.setUnits (this.parent.getTrainingSet ().getBooleanPattern (this.currentPattern));
        this.refreshStatusBar ();
        this.refreshUnitGrid ();
    }

    //-----------------------------------
    public void showPreviousPattern(){
        if (currentPattern == 0) {
            currentPattern = parent.getTrainingSet ().numPatterns () - 1;
        } else {
            currentPattern--;
        }
        // get the new pattern from the training set
        unitGrid.setUnits (parent.getTrainingSet ().getBooleanPattern (currentPattern));
        refreshStatusBar ();
        refreshUnitGrid ();
    }

    //-----------------------------------
    public void showNextPattern(){
        if (currentPattern == parent.getTrainingSet ().numPatterns () - 1) {
           currentPattern = 0;
       } else {
           currentPattern++;
       }
       // get the new pattern from the training set
       unitGrid.setUnits (parent.getTrainingSet ().getBooleanPattern (currentPattern));
       refreshStatusBar ();
       refreshUnitGrid ();
    }

}