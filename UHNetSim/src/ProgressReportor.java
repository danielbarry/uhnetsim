


/**
 * Write a description of interface ProgressReportor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

public interface ProgressReportor
{
	/**
	 * An example of a method header - replace this comment with your own
	 * 
	 * @param  y	a sample parameter for a method
	 * @return		the result produced by sampleMethod 
	 */
	void reportProgress(String s);
}
