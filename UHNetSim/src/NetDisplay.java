

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class NetDisplay extends JInternalFrame implements UnitGridListener {
    private JToolBar tlbZoom            = null;
    private JButton btnSmaller          = null;
    private JButton btnLarger           = null;
    private JButton btnTrain            = null;
    private JButton btnAllStable        = null;
    private JButton btnHiLight          = null;
    private JButton btnDynamic          = null;
    private JButton btnRunOne           = null;
    private JButton btnRun              = null;
    private JButton btnStop             = null;
    private JComboBox cboUpdateType     = null;
    private String[] updateTypes        = {"Synchronous", "Asynchronous sequential", "Asyn. random (remove)", "Asyn. random (replace)"};
    private byte defaultUpdateType      = 2;
    private UnitGrid unitGrid           = null;
    private NetSimFrame parent          = null;
    private Thread networkThread        = null;
    private boolean running             = false;

    //-----------------------------------
    public NetDisplay (NetSimFrame parent) {
        super ("Network state", false, false, false, true);

        this.parent = parent;
        this.initToolBar ();
        this.initNetDisplay ();
        this.pack ();
    }

    //-----------------------------------
    public void setUnit (int unitX, int unitY, boolean state) {
        this.unitGrid.setUnit (unitX, unitY, state);
        this.parent.getNetwork ().setUnit (unitX, unitY, state);
    }

    //-----------------------------------
    public void unitGridPixelSizeChange (UnitGridEvent e) {
        this.setSize (0, 0);
        this.pack ();
    }

    //-----------------------------------
    public void unitGridUnitChanged (UnitGridEvent e) {
        int unitIndex = (e.unitY * this.unitGrid.getUnitDimensions ().width) + e.unitX;

        this.parent.getNetwork ().toggleUnit (unitIndex);
    }

    //-----------------------------------
    private void initToolBar () {
        JPanel comboHolder = new JPanel (new BorderLayout ());

        this.tlbZoom = new JToolBar (JToolBar.HORIZONTAL);
        
        //String resPath = "." + java.io.File.separatorChar + "res" + java.io.File.separatorChar;
        String resPath = System.getProperty("user.dir") + "/res/";
        try{
            this.btnSmaller = new JButton (new ImageIcon (resPath + "ZoomOut16.gif"));
            this.btnLarger = new JButton (new ImageIcon (resPath + "ZoomIn16.gif"));
            this.btnTrain = new JButton (new ImageIcon (resPath + "Edit16.gif"));
            this.btnAllStable = new JButton (new ImageIcon (resPath + "Tick16.gif"));
            this.btnHiLight = new JButton (new ImageIcon (resPath + "Check16.gif"));
            this.btnDynamic = new JButton (new ImageIcon (resPath + "Dynamic16.gif"));
            this.btnRunOne = new JButton (new ImageIcon (resPath + "StepForward16.gif"));
            this.btnRun = new JButton (new ImageIcon (resPath + "Play16.gif"));
            this.btnStop = new JButton (new ImageIcon (resPath + "Stop16.gif"));
        }catch(NullPointerException e){ System.err.println("Done fuqed up."); }
        
        this.cboUpdateType = new JComboBox (this.updateTypes);
        this.cboUpdateType.setSelectedIndex(this.defaultUpdateType);
        comboHolder.add (this.cboUpdateType, BorderLayout.CENTER);

        this.btnSmaller.setToolTipText ("Zoom out");
        this.btnLarger.setToolTipText ("Zoom in");
        this.btnTrain.setToolTipText ("Train the network");
        this.btnAllStable.setToolTipText ("Check all patterns are stable");
        this.btnHiLight.setToolTipText("Compare with trained pattern");
        this.btnDynamic.setToolTipText("Display network's state transition diagram");
        this.btnRunOne.setToolTipText ("Run one iteration");
        this.btnRun.setToolTipText ("Run to convergence");
        this.btnStop.setToolTipText ("Stop");

		this.btnSmaller.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                unitGrid.decreaseUnitPixelSize ();
			}
		});

		this.btnLarger.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
				unitGrid.increaseUnitPixelSize ();
			}
		});

        this.btnTrain.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                parent.getNetwork ().setRunOperation (Network.TRAIN);
                networkThread = new Thread (parent.getNetwork ());
                networkThread.setPriority (Thread.MIN_PRIORITY);
                networkThread.start ();
            }
		});

        this.btnAllStable.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                parent.getNetwork ().setRunOperation (Network.ALL_STABLE);
                networkThread = new Thread (parent.getNetwork ());
                networkThread.setPriority (Thread.MIN_PRIORITY);
                networkThread.start ();
            }
		});

        this.btnHiLight.addActionListener(new ActionListener(){
            public void actionPerformed (ActionEvent e) {
                hiLightIncorrectStates();
            }
        });

        this.btnDynamic.addActionListener(new ActionListener(){
            public void actionPerformed (ActionEvent e) {
                displayDynamic();
            }
        });

        this.btnRunOne.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                parent.getNetwork ().setRunOperation (Network.RUN_ONE);
                networkThread = new Thread (parent.getNetwork ());
                networkThread.setPriority (Thread.MIN_PRIORITY);
                networkThread.start ();
            }
		});

        this.btnRun.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                running = true;
                parent.getNetwork ().setRunOperation (Network.RUN);
                networkThread = new Thread (parent.getNetwork ());
                networkThread.setPriority (Thread.MIN_PRIORITY);
                networkThread.start ();
            }
		});

        this.btnStop.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                parent.getNetwork ().stopRun ();
            }
		});

        this.cboUpdateType.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                parent.getNetwork ().setUpdateType (cboUpdateType.getSelectedIndex ());
            }
		});

        // add the buttons to the toolbar
        this.tlbZoom.add (this.btnSmaller);
        this.tlbZoom.add (this.btnLarger);
        this.tlbZoom.addSeparator ();
        this.tlbZoom.add (this.btnTrain);
        this.tlbZoom.add (this.btnAllStable);
        this.tlbZoom.add (this.btnHiLight);
        this.tlbZoom.add (this.btnDynamic);
        this.tlbZoom.addSeparator ();
        this.tlbZoom.add (this.btnRunOne);
        this.tlbZoom.add (this.btnRun);
        this.tlbZoom.add (this.btnStop);
        this.tlbZoom.addSeparator ();
        this.tlbZoom.add (comboHolder);

        // we don't want people dragging the toolbar around
        this.tlbZoom.setFloatable (false);

        // add the toolbar to the frame
        this.getContentPane ().add (this.tlbZoom, BorderLayout.NORTH);
    }

    //-----------------------------------
    private void initNetDisplay () {
        this.unitGrid = new UnitGrid (this.parent.getNetwork ().numUnits ());
        this.unitGrid.addUnitGridListener (this);
        this.getContentPane ().add (this.unitGrid, BorderLayout.CENTER);
    }

    //-----------------------------------
    public Dimension getUnitDimensions () {
        return this.unitGrid.getUnitDimensions ();
    }

    //-----------------------------------
    public void setEnabled (boolean state) {
        this.btnSmaller.setEnabled (state);
        this.btnLarger.setEnabled (state);
        this.btnTrain.setEnabled (state);
        this.btnAllStable.setEnabled (state);
        this.btnHiLight.setEnabled (state);
        this.btnDynamic.setEnabled (state);
        this.btnRunOne.setEnabled (state);
        this.btnRun.setEnabled (state);
        if (this.running == false && state == false) {
            this.btnStop.setEnabled (false);
        } else if (state == true || this.running == true) {
            this.btnStop.setEnabled (true);
        }
        this.cboUpdateType.setEnabled (state);
        this.unitGrid.setEnabled (state);
    }

    //-----------------------------------
    public void refreshUnitGrid () {
        this.unitGrid.repaint ();
    }

    //-----------------------------------
    public boolean[] getUnits () {
        return this.unitGrid.getUnits ();
    }

    //-----------------------------------
    public void setUnits (boolean[] units) {
        this.unitGrid.setUnits (units);
    }

    //-----------------------------------
    public void setRunning (boolean state) {
        this.running = state;
    }

    //-----------------------------------
	public String getUpdateType(){
		return this.cboUpdateType.getSelectedItem().toString();
    }

    //-----------------------------------
    public int hiLightIncorrectStates(){
        int currentPattern = parent.getTrainingSetDisplay().getCurrentPattern();
        boolean[] incorrectUnits = new boolean[parent.getNetwork().numUnits()];
        int numIncorrect = 0;

        for(int i=0; i<parent.getNetwork().numUnits(); i++){
            incorrectUnits[i] = parent.getNetwork().getBooleanUnit(i)
                != parent.getTrainingSet().getBooleanUnit(currentPattern, i);
            numIncorrect += (incorrectUnits[i]? 1: 0);
        }
        this.unitGrid.setGrayUnits(incorrectUnits);
        this.unitGrid.repaint ();

        return (numIncorrect * 2 > incorrectUnits.length ? incorrectUnits.length - numIncorrect : numIncorrect);
    }

    //-----------------------------------
    public void displayDynamic(){
        if( parent.getNetwork().numUnits() > 30 ){
            JOptionPane.showMessageDialog(this, "Network dynamic viewer for a network bigger than 30 units is currently under development!");
            return;
        }

    	DynamicViewer dynamicViewer = new DynamicViewer(this.parent);
        parent.addChildFrame(dynamicViewer);
        dynamicViewer.display();
    }
}
