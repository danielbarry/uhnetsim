

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class TrainingNotifyFrame extends JInternalFrame {
    private GridBagConstraints gridBagConstraints = null;
    private JPanel panel                          = null;
    private JLabel action                         = null;
    private JLabel learningRule                   = null;
    private JLabel learningRuleData               = null;
    private JLabel numPatterns                    = null;
    private JLabel numPatternsData                = null;
    private JLabel time                           = null;
    private JLabel timeData                       = null;
    private JLabel iterations                     = null;
    private JLabel iterationsData                 = null;
    private JButton btnClose                      = null;
    private NetSimFrame parent                    = null;

    //-----------------------------------
    public TrainingNotifyFrame (NetSimFrame p) {
        super ("Training network", false, false, false, false);
        
        this.parent = p;
        this.gridBagConstraints = new GridBagConstraints ();

        this.panel = new JPanel ();
        this.panel.setLayout (new GridBagLayout ());

        this.learningRule = new JLabel ("Learning rule :        ");
        this.learningRuleData = new JLabel ();
        this.numPatterns = new JLabel  ("Number of patterns :   ");
        this.numPatternsData = new JLabel ();
        this.time = new JLabel         ("Time taken to train :  ");
        this.timeData = new JLabel ("none");
        this.iterations = new JLabel   ("Number of iterations : ");
        this.iterationsData  = new JLabel ("0");
        this.btnClose = new JButton ("Close");
        
		this.btnClose.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                parent.setMenusEnabled (true);
                parent.getNetDisplay ().setEnabled (true);
                parent.getTrainingSetDisplay ().setEnabled (true);
                setVisible (false);
			}
		});

        this.gridBagConstraints.insets = new Insets (20, 50, 0, 0);
        this.gridBagConstraints.anchor = GridBagConstraints.WEST;
        this.gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        this.panel.add (this.learningRule, this.gridBagConstraints);
        this.gridBagConstraints.insets = new Insets (20, 0, 0, 50);
        this.gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        this.panel.add (this.learningRuleData, this.gridBagConstraints);

        this.gridBagConstraints.insets = new Insets (0, 50, 0, 0);
        this.gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        this.panel.add (this.numPatterns, this.gridBagConstraints);
        this.gridBagConstraints.insets = new Insets (0, 0, 0, 50);
        this.gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        this.panel.add (this.numPatternsData, this.gridBagConstraints);

        this.gridBagConstraints.insets = new Insets (0, 50, 0, 0);
        this.gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        this.panel.add (this.time, this.gridBagConstraints);
        this.gridBagConstraints.insets = new Insets (0, 0, 0, 50);
        this.gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        this.panel.add (this.timeData, this.gridBagConstraints);

        this.gridBagConstraints.insets = new Insets (0, 50, 0, 0);        
        this.gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        this.panel.add (this.iterations, this.gridBagConstraints);
        this.gridBagConstraints.insets = new Insets (0, 0, 0, 50);
        this.gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        this.panel.add (this.iterationsData, this.gridBagConstraints);

        this.gridBagConstraints.insets = new Insets (20, 0, 10, 0);
        this.gridBagConstraints.anchor = GridBagConstraints.CENTER;
        this.gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        this.panel.add (this.btnClose, this.gridBagConstraints);

        this.getContentPane ().add (this.panel, BorderLayout.CENTER);

        this.setSize (500, 300);
        this.setLocation (50, 50);
        this.setVisible (false);
        this.pack ();
    }


    //-----------------------------------
    public void startTraining () {
        this.learningRuleData.setText (this.parent.getTrainingStats ().getLearningRule ());
        this.numPatternsData.setText (this.parent.getTrainingStats ().getNumPatterns ());
        this.timeData.setText ("none");
        this.btnClose.setEnabled (false);
    }

    //-----------------------------------
    public void doneTraining () {
        this.timeData.setText (this.parent.getTrainingStats ().getTrainingTime ());
        this.iterationsData.setText (this.parent.getTrainingStats ().getNumIterations ());
        this.btnClose.setEnabled (true);
    }
}
