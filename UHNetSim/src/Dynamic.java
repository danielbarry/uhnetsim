

/*
 * @(#)Dynamic.java
 * WKAye
 */

import java.util.*;



class Attractor{
    int intValue;
    int attraction = 0;
    Attractor nextAttractor = null;
    ArrayList attractees = null;


    Attractor(int intValue){
        this.intValue = intValue;
        this.attraction = 1;
    }


    public void addAttractee(Attractor a){
        if( attractees == null ) attractees = new ArrayList();
        attractees.add (a);
    }
}


class Memory{
    Attractor attractor = null;
    boolean isParasite  = false;
    int[] summary       = null;


    Memory(Attractor attractor){
        this.attractor = attractor;
        summary = new int[Dynamic.MAXEPOCH + 1];
    }


    public void calcAttraction(){
        calcAttraction(attractor, 1);
    }


    private void calcAttraction(Attractor a, int epoch){
        if(epoch > Dynamic.MAXEPOCH){
            System.out.println("Far away attractor:" + Integer.toHexString(a.intValue) + "@" + epoch);
            return;
        }

        summary[epoch] += a.attraction;
        summary[0] += a.attraction;

        if(a.attractees != null){
            for(int i = 0; i < a.attractees.size (); i++){
                calcAttraction( (Attractor)a.attractees.get(i), epoch + 1 );
            }
        }
    }
}


class Distribution{
    int attraction;
    int frequency;

    Distribution(int a){
        attraction = a;
        frequency = 1;
    }
}


public class Dynamic{

    NetSimFrame parent         = null;
    ProgressReportor reportor  = null;

    int[] transitions          = null;
    Hashtable attractors       = null;
    Hashtable memories         = null;
    int[][] summary            = null;

    int numStates               = 0;
    int numParasiticAttractors  = 0;
    int numTrainedAttractors    = 0;
    int numFixedPtAttractors    = 0;
    static final byte MAXEPOCH  = 10;
    static final short MAXNODES = 1000;
    String result               = "";


    Dynamic(NetSimFrame parent, ProgressReportor reportor){
        this.parent = parent;
        this.reportor = reportor;

        this.attractors = new Hashtable();
        this.memories = new Hashtable();
        this.summary = new int[3][MAXEPOCH + 1];
        this.numStates = (int)Math.pow(2.0, parent.getNetwork().numUnits());
    }


    public void calculate(){

        int[] currentState = parent.getNetwork().getUnits();
        transitions = new int[numStates];

        System.out.println("Working out " + numStates + " state transitions...");
        calculateTransitions();
        parent.getNetwork().setUnits(currentState);

        System.out.println("\nProcessing " + attractors.size() + " intermediate states...");
        abstractMemories();
        transitions = null;

        System.out.println("Calculating attraction for " + memories.size() + " final states...");
        summarizeAttraction();
    }


    private void calculateTransitions(){
        int progress = 0;

        for (int i = 0; i < numStates; i++) {
            if( i == progress){
                reportor.reportProgress(i + " / " + numStates + " ( " + calcPercentage(i, numStates) + " )");
                progress += (numStates / 100 + 1);
            }
            transitions[i] = getNextState(i);
            Attractor a = (Attractor)attractors.get(new Integer(transitions[i]));
            if( a == null ){
                a = new Attractor(transitions[i]);
                attractors.put(new Integer(transitions[i]), a);
            }else{
                a.attraction++;
            }
        }
    }


    public int getNextState(int thisState){
        int nextState;
        parent.getNetwork().setLongState((long)thisState);
        parent.getNetwork().runOneIteration();
        nextState = (int)parent.getNetwork().getLongState();
        //System.out.println(thisState + "\t" + nextState );
        return nextState;
    }


    private void abstractMemories(){
        for (Enumeration e = attractors.elements(); e.hasMoreElements();){
            Attractor a = (Attractor)e.nextElement();
            a.nextAttractor = (Attractor)attractors.get (new Integer( transitions[a.intValue] ));

            if( a.nextAttractor.intValue == a.intValue){
                a.attraction--;
                numFixedPtAttractors++;
                memories.put( new Integer(a.intValue), newMemory(a) );

            }else if(transitions[a.nextAttractor.intValue] == a.intValue){
                memories.put( new Integer(a.intValue), newMemory(a) );

            }else{
                a.nextAttractor.addAttractee (a);
            }
        }
        //printAttractors();
    }


    private Memory newMemory(Attractor a){
        Memory m = new Memory(a);
        m.isParasite = !( parent.getTrainingSet().isTrainingPattern(a.intValue)
                        || parent.getTrainingSet().isTrainingPattern(numStates - a.intValue - 1) );
        if( m.isParasite ){ numParasiticAttractors++; }
        else{ numTrainedAttractors++; }
        return m;
    }


    public void printAttractors(){
        for (Enumeration e = attractors.elements(); e.hasMoreElements();){
            Attractor a = (Attractor)e.nextElement();
            System.out.print(a.nextAttractor.intValue + " <- " + a.intValue + " <- { ");
            for(int i=0; a.attractees != null && i < a.attractees.size (); i++){
                System.out.print(Integer.toHexString(((Attractor)a.attractees.get(i)).intValue) + ", ");
            }
            System.out.println(" }");
        }
    }


    private void summarizeAttraction(){
        for (Enumeration e = memories.elements(); e.hasMoreElements();){
            Memory m = (Memory)e.nextElement();
            m.calcAttraction();

            for(int j = 0; j <= MAXEPOCH; j++){
                if( m.isParasite ){
                    summary[0][j] += m.summary[j];
                }else{
                    summary[1][j] += m.summary[j];
                }
                summary[2][j] += m.summary[j];
            }
        }
    }


    public String getDetails(){
        String details = "";

        for (Enumeration e = memories.elements(); e.hasMoreElements();){
            Memory m = (Memory)e.nextElement();
            details += ((m.isParasite? "?.": "$.") + Integer.toHexString(m.attractor.intValue));

            for(int j = 0; j <= MAXEPOCH && m.summary[j] != 0; j++){
                details += (", " + m.summary[j]);
            }
            details += "\n";
        }
        return details;
    }


    public String getSummary(){
        String summary = "Type, #Attractors, Total, 1Epoch, 2Epoch, 3Epoch, ...\n";
        String[] types = {"Parasitic", "Trained", "Total"};
        int[] numAttractors= {numParasiticAttractors, numTrainedAttractors, numParasiticAttractors+numTrainedAttractors};

        for(int i = 0; i < 3; i++){
            summary += (types[i] + ", " + numAttractors[i]);
            for(int j = 0; j <= MAXEPOCH && this.summary[i][j] != 0; j++){
                summary += (", " + this.summary[i][j]);
            }
            summary += "\n";
        }
        return summary;
    }


    private int calcMinAttraction(){
        int minAttraction = -1;
        Hashtable distributions = new Hashtable();

        for (Enumeration e = attractors.elements(); e.hasMoreElements();){
            Attractor a = (Attractor)e.nextElement();
            Distribution d = (Distribution)distributions.get(new Integer(a.attraction));
            if(d == null){
                distributions.put(new Integer(a.attraction), new Distribution(a.attraction));
            }else{
                d.frequency++;
            }
        }

        int numPicked = 0;
        Integer max = null;
        while(numPicked < MAXNODES){
            int maxAttraction = 0;
            for (Enumeration e = distributions.elements(); e.hasMoreElements();){
                Distribution d = (Distribution)e.nextElement();
                maxAttraction = (d.attraction > maxAttraction ? d.attraction: maxAttraction);
            }
            max = new Integer(maxAttraction);
            numPicked += ((Distribution) distributions.get(max)).frequency;
            distributions.remove(max);
        }
        return max.intValue();
    }


    public ArrayList getEdges(){

        int minAttraction     = 0;
        Hashtable nodes       = new Hashtable();
        int numStableNodes    = 0;
        ArrayList edges       = null;

        calculate();

        if( attractors.size () < MAXNODES ){
            minAttraction = 1;
        }else{
            //minAttraction = (int)(maxAttraction * 0.1) + 1;
            //minAttraction = (int)(numStates * 0.0001) + 1;
            minAttraction = calcMinAttraction();
        }

        System.out.println("Selecting nodes with " + minAttraction + " minimum attraction...");
        for (Enumeration e = attractors.elements(); e.hasMoreElements();){
            Attractor a = (Attractor)e.nextElement();
            if( a.attraction > minAttraction ){
                nodes.put(new Integer(a.intValue), newNode(a));
            }
        }

        System.out.println("Building edges for " + nodes.size() + " nodes...");
        edges = new  ArrayList();
        numStableNodes = buildEdges(nodes, edges);

        System.out.println("Drawing graph for " + numStableNodes + " final states...");

        result = numStates + "(" + minAttraction + "), "
            + nodes.size() + "/" + attractors.size() + " (" + calcPercentage(nodes.size(), attractors.size()) + "), "
            + numStableNodes + "/" + memories.size() + " (" + calcPercentage(numStableNodes, memories.size()) + ")";

        System.out.println(getSummary());
        nodes = null;
        return edges;
    }


    public ArrayList getEdges(int finalState){

        int minAttraction     = 0;
        Hashtable nodes       = new Hashtable();
        int numStableNodes    = 0;
        ArrayList edges       = null;

        Attractor a = (Attractor)attractors.get(new Integer(finalState));
        addNodes(nodes, a);

        if( nodes.size () < MAXNODES ){
            minAttraction = 1;
        }else{
            //minAttraction = (int)(maxAttraction * 0.1) + 1;
            minAttraction = (int)(numStates * 0.0001) + 1;
            for (Enumeration e = nodes.elements(); e.hasMoreElements();) {
                Node n = (Node)e.nextElement();
                if( n.attraction < minAttraction ){
                    nodes.remove(new Integer(n.label));
                }
            }
        }

        edges = new ArrayList();
        numStableNodes = buildEdges(nodes, edges);

        result = numStates + "(" + minAttraction + "), "
            + nodes.size() + "/" + attractors.size() + " (" + calcPercentage(nodes.size(), attractors.size()) + "), "
            + numStableNodes + "/" + memories.size() + " (" + calcPercentage(numStableNodes, memories.size()) + ")";

        nodes = null;
        return edges;
    }


    private Node newNode(Attractor a){
        Node n = new Node(a.intValue);
        n.attraction = a.attraction;

        Memory memory = (Memory)memories.get(new Integer(a.intValue));
        if( memory != null ){ n.isParasite = memory.isParasite; }
        else { n.isParasite = true; }

        return n;
    }


    private void addNodes(Hashtable nodes, Attractor a){
        nodes.put(new Integer(a.intValue), newNode(a));

        if(a.attractees != null){
            for(int i = 0; i < a.attractees.size (); i++){
                addNodes(nodes, (Attractor)a.attractees.get(i));
            }
        }
    }


    private int buildEdges(Hashtable nodes, ArrayList edges){
        int numStableNodes    = 0;

        for (Enumeration n = nodes.elements(); n.hasMoreElements();) {
            Edge e = new Edge();
            e.from = (Node)n.nextElement();
            //System.out.print (e.from.label + "(" + e.from.attraction + ") - ");

            Attractor to = ((Attractor)attractors.get(new Integer(e.from.label))).nextAttractor;

            e.to = (Node) nodes.get( new Integer( to.intValue ) );
            //System.out.println (e.to.label + "(" + e.to.attraction + ").");

            if( e.to == null ){
                e.to = e.from;
                e.isFinal = false;
            }else{
                // determine whether this is final transition
                e.isFinal = to.nextAttractor.intValue == e.from.label;
                //System.out.println( "( " + e.isFinal + " )" );

                if( e.isFinal ){ numStableNodes++; }
            }
            edges.add(e);
        }
        return numStableNodes;
    }


    public String getResult(){
        return result;
    }


    public static String calcPercentage(int above, int bottom){
        byte percent = (byte)(above  * 100 / bottom);
        byte fraction = (byte)(above * 10000 / bottom - percent * 100);
        return percent + "." + fraction + "%";
    }
}
