


import java.awt.*;
import javax.swing.*;


public class WeightsViewer extends JInternalFrame{

    WeightsViewer(NetSimFrame parent) {
        super ("Weight metrix", true, true, true, true);
        setTitle(getTitle() + " ( "
                 + parent.getTrainingSet().numPatterns() + " patterns, "
                 + Experiments.formatPercentage(parent.getPrunedNetwork().getAxonalReduction()) + ", "
                 + Experiments.formatPercentage(parent.getPrunedNetwork().getLengthReduction()) + ", "
                 + Experiments.formatPercentage(parent.getPrunedNetwork().getSynapticReduction()) + " )"
        );

        WeightsPanel metrix = new WeightsPanel(parent);
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add (metrix, BorderLayout.CENTER);
        this.setSize( metrix.DEFAULTWIDTH, metrix.DEFAULTHEIGHT);
        this.setLocation(400, 100);
        parent.addChildFrame(this);
        this.setVisible(false);
     }

}

class WeightsPanel extends JPanel{

    NetSimFrame parent = null;
    int numUnits       = 0;
    double[][] weights = null;
    double maxWeight   = 0.0;

    Image offscreen                = null;
    Dimension offscreensize        = null;
    Graphics offgraphics           = null;
    static final int DEFAULTWIDTH  = 400;
    static final int DEFAULTHEIGHT = 400;

    WeightsPanel(NetSimFrame parent){
        this.parent = parent;
        numUnits = parent.getNetwork().numUnits();
        weights = new double[numUnits][numUnits];
        for(int i = 0; i < numUnits; i++ ){
            for(int j = 0; j < numUnits; j++ ){
                weights[i][j] = parent.getNetwork().getWeight(i, j);
            }
        }
        maxWeight = parent.getNetwork().getStrongestWeight();
    }

    public synchronized void paint(Graphics g) {

    	Dimension d = getSize();
        if((offscreen==null)||(d.width!=offscreensize.width)
               || (d.height!=offscreensize.height)) {
            offscreen = createImage(d.width, d.height);
            offscreensize = d;
            if (offgraphics != null) {
                offgraphics.dispose();
            }
    	    offgraphics = offscreen.getGraphics();
            offgraphics.setFont(getFont());
    	}
        offgraphics.setColor(Color.lightGray);
    	offgraphics.fillRect(0, 0, d.width, d.height);

        int unitWidth = d.width / numUnits;
        int unitHeight = d.height / numUnits;

        for(int i = 0; i < numUnits; i++){
            for(int j = 0; j < numUnits; j++){
                if (weights[i][j] == 0) continue;
                int x, y, width, height;
                if(weights[i][j] > 0 ){
                    offgraphics.setColor(Color.black);
                }else{
                    offgraphics.setColor(Color.white);
                }
                width = (int)(Math.abs(weights[i][j]) / maxWeight * unitWidth);
                height = (int)(Math.abs(weights[i][j]) / maxWeight * unitHeight);
                x = i*unitWidth + (unitWidth-width)/2;
                y = j*unitHeight + (unitHeight-height)/2;

                offgraphics.fillOval (x, y, width, height);
            }
        }
        g.drawImage(offscreen, 0, 0, null);
    }
}
