

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class NavPanel extends JPanel {
    public final static int BACK   = 0;
    public final static int NEXT   = 1;
    public final static int FINISH = 2;

    private int btnNextType                       = NavPanel.NEXT;
    private JButton btnBack                       = null;
    private JButton btnNext                       = null;
    private Insets buttonInsets                   = null;
    private GridBagLayout gridBagLayout           = null;
    private GridBagConstraints gridBagConstraints = null;
    private EventListenerList listenerList        = null;
    private int action                            = NavPanel.NEXT;

    public NavPanel () {
        this.listenerList = new EventListenerList ();

        this.btnBack = new JButton ("<< Back");
        this.btnNext = new JButton ("Next >>");

        this.buttonInsets  = new Insets (0, 10, 0, 10);
        this.gridBagLayout = new GridBagLayout ();
        this.gridBagConstraints = new GridBagConstraints ();
        this.setBorder (new EmptyBorder (10, 10, 10, 10));
        this.setLayout (this.gridBagLayout);

        this.add (this.btnBack, this.gridBagConstraints);
        this.add (this.btnNext, this.gridBagConstraints);

        this.btnBack.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                action = NavPanel.BACK;
                fireActionPerformed ();
            }
        });
        
        this.btnNext.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                if (btnNextType == NavPanel.NEXT) {
                    action = NavPanel.NEXT;
                } else {
                    action = NavPanel.FINISH;
                }
                fireActionPerformed ();
            }
        });
    }

    //-----------------------------------
    public void addActionListener (ActionListener listener) {
        listenerList.add (ActionListener.class, listener);
    }

    //-----------------------------------
    public void removeActionListener (ActionListener listener) {
        listenerList.remove (ActionListener.class, listener);
    }

    //-----------------------------------
    private void fireActionPerformed () {
        int i                   = 0;
        Object[] listeners      = listenerList.getListenerList ();
        ActionEvent actionEvent = new ActionEvent (this, this.action, null);

        for (i=listeners.length-2;i>=0;i-=2) {
            if (listeners[i]==ActionListener.class) {
                ((ActionListener)listeners[i+1]).actionPerformed (actionEvent);
           }
        }
    }

    //-----------------------------------
    public void setBtnBackState (boolean state) {
        this.btnBack.setEnabled (state);
    }
    
    //-----------------------------------
    public void setBtnNextState (boolean state) {
        this.btnNext.setEnabled (state);
    }

    //-----------------------------------
    public void setBtnBackText (String text) {
        this.btnBack.setText (text);
        this.btnBack.setPreferredSize (this.btnNext.getPreferredSize ());
    }

    //-----------------------------------
    public void setBtnNextText (String text) {
        this.btnNext.setText (text);
        this.btnNext.setPreferredSize (this.btnBack.getPreferredSize ());
    }

    //-----------------------------------
    public void setBtnNextType (int type) {
        this.btnNextType = type;
    }

    //-----------------------------------
    public int getAction () {
        return this.action;
    }
}