


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class PruneNetworkFrame extends JInternalFrame {
    private NetSimFrame parent          = null;

    static final int WEAKREMOTE         = 0;
    static final int RANDOM             = 1;
    static final int LONGEST            = 2;
    static final int WEAKEST            = 3;

    private JLabel lblPruningRule       = null;
    private JComboBox cboPruningRule    = null;
    private String[] pruningRules       = {"Weak-remote", "Random", "Longest first", "Weakest first"};
    private byte defaultPruningRule     = WEAKREMOTE;
    private JLabel lblCoefficient       = null;
    private NumericField txfCoefficient = null;
    private JLabel lblAxons             = null;
    private JLabel lblAxonsData         = null;
    private JLabel lblLength            = null;
    private JLabel lblLengthData        = null;
    private JLabel lblSynapses          = null;
    private JLabel lblSynapsesData      = null;
    private JButton btnCalculate        = null;
    private JButton btnApply            = null;
    private JButton btnClose            = null;


    //-----------------------------------
    public PruneNetworkFrame (NetSimFrame parent) {
        super ("Prune the network", false, false, false, false);
        this.parent = parent;

        GridBagConstraints gridBagConstraints = new GridBagConstraints ();
        JPanel panel = new JPanel ();
        panel.setLayout (new GridBagLayout ());

        this.lblPruningRule = new JLabel  ("          Pruning rule                  ");
        this.cboPruningRule = new JComboBox(pruningRules);
        this.cboPruningRule.setSelectedIndex (this.defaultPruningRule);
        this.lblCoefficient  = new JLabel ("          Pruning coefficient (0.###)   ");
        this.txfCoefficient = new NumericField(4);
        this.lblAxons = new JLabel        ("          Reduced number of axons       ");
        this.lblAxonsData = new JLabel ();
        this.lblLength = new JLabel       ("          Axonal length reduction       ");
        this.lblLengthData = new JLabel ();
        this.lblSynapses = new JLabel     ("          Synaptic strength reduction   ");
        this.lblSynapsesData = new JLabel ();

        this.btnCalculate = new JButton ("Calculate");
        this.btnApply = new JButton ("Apply");
        this.btnApply.setPreferredSize (this.btnCalculate.getPreferredSize ());
        this.btnClose = new JButton ("Close");
        this.btnClose.setPreferredSize (this.btnCalculate.getPreferredSize ());


        this.btnCalculate.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                calculate();
            }
        });

        this.btnApply.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                apply();
        	}
        });

        this.btnClose.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                close();
            }
        });

        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.insets = new Insets (20, 0, 0, 20);

        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        panel.add (this.lblPruningRule, gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        panel.add (this.cboPruningRule, gridBagConstraints);

        gridBagConstraints.insets = new Insets (5, 0, 0, 20);
        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        panel.add (this.lblCoefficient, gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        panel.add (this.txfCoefficient, gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        panel.add (this.lblAxons, gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        panel.add (this.lblAxonsData, gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        panel.add (this.lblLength, gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        panel.add (this.lblLengthData, gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        panel.add (this.lblSynapses, gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        panel.add (this.lblSynapsesData, gridBagConstraints);

        JPanel pnlApply = new JPanel();
        pnlApply.add (this.btnCalculate, BorderLayout.WEST);
        pnlApply.add (this.btnApply, BorderLayout.EAST);
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        gridBagConstraints.insets = new Insets (20, 20, 20, 0);
        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;

        panel.add (pnlApply, gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        panel.add (this.btnClose, gridBagConstraints);

        this.getContentPane ().add (panel, BorderLayout.CENTER);

        this.setSize (400, 400);
        this.setLocation (50, 50);
        this.setVisible (false);
        this.pack ();

    }

    //-----------------------------------
    public void display () {
        parent.setMenusEnabled (false);
        parent.getNetDisplay ().setEnabled (false);
        parent.getTrainingSetDisplay ().setEnabled (false);
        parent.getTestSetDisplay ().setEnabled (false);

        init();
        setVisible (true);
    }

    //-----------------------------------
    private void init(){
        txfCoefficient.setText("");
        setResults();
    }

    //-----------------------------------
    private void setResults(){
        this.lblAxonsData.setText( Experiments.formatPercentage(parent.getPrunedNetwork().getAxonalReduction()));
        this.lblLengthData.setText( Experiments.formatPercentage(parent.getPrunedNetwork().getLengthReduction()));
        this.lblSynapsesData.setText( Experiments.formatPercentage(parent.getPrunedNetwork().getSynapticReduction()));
    }

    //-----------------------------------
    private void calculate(){
        parent.getPrunedNetwork().prune(cboPruningRule.getSelectedIndex(), new Float("0." + txfCoefficient.getText()).floatValue());
        setResults();
    }

    //-----------------------------------
    private void apply(){
        parent.getPrunedNetwork().apply();
    }

    //-----------------------------------
    private void close(){
        parent.setMenusEnabled (true);
        parent.getNetDisplay ().setEnabled (true);
        parent.getTrainingSetDisplay ().setEnabled (true);
        parent.getTestSetDisplay ().setEnabled(true);
        setVisible (false);
    }

    //-----------------------------------
    public String[] getPruningRules(){
        return pruningRules;
    }

}

