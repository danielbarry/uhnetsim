

import java.util.Random;

public class PrunedNetwork{

    NetSimFrame parent         = null;

    double[] trainedWeights    = null;
    byte[] prunedConns         = null;
    int numUnits               = 0;
    int gridWidth              = 0;
    double totalLength         = 0;
    double maxDistance         = 0;
    double totalSynapses       = 0;
    double maxStrength         = 0;
    double axonalReduction     = 0;
    double lengthReduction     = 0;
    double synapticReduction   = 0;

    public PrunedNetwork (NetSimFrame parent) {
        this.parent = parent;
    }

    public void rebuild(){
        numUnits = parent.getNetwork().numUnits();
        gridWidth = this.parent.getNetDisplay().getUnitDimensions().width;
        trainedWeights = new double[numUnits*numUnits];
        prunedConns = new byte[numUnits*numUnits];

        maxDistance = 0;
        totalLength = 0;
        maxStrength = 0;
        totalSynapses = 0;

        for (int index = 0; index < numUnits * numUnits; index++) {
            double distance = calculateDistance(index);
            totalLength += distance;
            maxDistance = distance > maxDistance? distance: maxDistance;
            trainedWeights[index] = parent.getNetwork().getWeight(index);
            double absStrength = Math.abs(trainedWeights[index]);
            totalSynapses += absStrength;
            maxStrength = absStrength > maxStrength? absStrength: maxStrength;
        }
        reconnect();
        axonalReduction = 0;
        lengthReduction = 0;
        synapticReduction = 0;
    }

    //-----------------------------------
    private void reconnect(){
        for(int i = 0; i < numUnits * numUnits; i++){
            if( (i % numUnits) == (i / numUnits) ){
                prunedConns[i] = 0;
            }else{
                prunedConns[i] = 1;
            }
        }
    }

    //-----------------------------------
    public double calculateDistance(int index){
        int fromUnit, toUnit;
        int x1, y1, x2, y2, dx, dy;
        double distance = 0;

        fromUnit = index % this.numUnits;
        toUnit = index / this.numUnits;
        x1 = fromUnit % this.gridWidth;
        y1 = fromUnit / this.gridWidth;
        x2 = toUnit % this.gridWidth;
        y2 = toUnit / this.gridWidth;
        dx = x2 - x1;
        dy = y2 - y1;
        distance = Math.sqrt((dx*dx)+(dy*dy));

        //System.out.println( index + "\t" + trainedWeights [index] + "\t" + fromUnit + "\t" + toUnit
        //	+ "\t" + x1 + "\t" + y1 + "\t" + x2 + "\t" + y2 + "\t" + dx + "\t" + dy );

        return distance;
    }

    //-----------------------------------
    public void prune(int pruningRule, float coefficient){
        switch(pruningRule){
        case PruneNetworkFrame.WEAKREMOTE :
            pruneWeakRemote(coefficient);
            break;
        case PruneNetworkFrame.RANDOM:
            pruneRandom(coefficient);
            break;
        case PruneNetworkFrame.LONGEST:
            pruneLongest(coefficient);
            break;
        case PruneNetworkFrame.WEAKEST:
            pruneWeakest(coefficient);
            break;
        }
    }

    //-----------------------------------
    public void pruneWeakRemote(float coefficient){

        int prunedAxons = 0;
        double reducedLength = 0.0;
        double lostSynapses = 0.0;

        for (int index = 0; index < this.numUnits * this.numUnits; index++) {
            double distance= calculateDistance(index);
            if( distance == 0 ) continue;
            if ( Math.abs(trainedWeights[index]) * distance <=  coefficient * maxDistance * maxStrength ){
                prunedConns[index] = 0;
                prunedAxons ++;
                reducedLength += distance;
                lostSynapses += Math.abs(trainedWeights [index]);
            }else{
                prunedConns[index] = 1;
            }
         }

        axonalReduction = ((double)prunedAxons / (numUnits*(numUnits-1))) * 100;
        lengthReduction = reducedLength / this.totalLength * 100;
        synapticReduction = lostSynapses / this.totalSynapses * 100;
    }

    //-----------------------------------
    public void pruneLongest(float coefficient){

        int prunedAxons = 0;
        double reducedLength = 0.0;
        double lostSynapses = 0.0;

        for (int index = 0; index < this.numUnits * this.numUnits; index++) {
            double distance= calculateDistance(index);
            if( distance == 0 )continue;
            if ( distance > maxDistance - (maxDistance * coefficient) ){
                prunedConns[index] = 0;
                prunedAxons ++;
                reducedLength += distance;
                lostSynapses += Math.abs(trainedWeights [index]);
            }else{
                prunedConns[index] = 1;
            }
         }

        axonalReduction = ((double)prunedAxons / (numUnits*(numUnits-1))) * 100;
        lengthReduction = reducedLength / this.totalLength * 100;
        synapticReduction = lostSynapses / this.totalSynapses * 100;
    }

    //-----------------------------------
    public void pruneWeakest(float coefficient){

        int prunedAxons = 0;
        double reducedLength = 0.0;
        double lostSynapses = 0.0;


        for (int index = 0; index < this.numUnits * this.numUnits; index++) {
            double distance= calculateDistance(index);
            if( distance == 0 )continue;
            if ( Math.abs(trainedWeights[index])  <=  maxStrength * coefficient){
                prunedConns[index] = 0;
                prunedAxons ++;
                reducedLength += distance;
                lostSynapses += Math.abs(trainedWeights [index]);
            }else{
                prunedConns[index] = 1;
            }
         }

        axonalReduction = ((double)prunedAxons / (numUnits*(numUnits-1))) * 100;
        lengthReduction = reducedLength / this.totalLength * 100;
        synapticReduction = lostSynapses / this.totalSynapses * 100;
    }

    //-----------------------------------
    public void pruneRandom(float coefficient){

        Random rdn = new Random();
        int targetPruning = (int)(numUnits * (numUnits-1) * coefficient);
        int prunedAxons = 0;
        double reducedLength = 0.0;
        double lostSynapses = 0.0;

        reconnect();

        while( prunedAxons < targetPruning ){
            int i = rdn.nextInt(numUnits * numUnits);
            if( prunedConns[i] == 1 ){
                prunedConns[i] = 0;
                prunedAxons ++;
                reducedLength += calculateDistance(i);
                lostSynapses += Math.abs(trainedWeights [i]);
            }
        }

        axonalReduction = ((double)prunedAxons / (numUnits*(numUnits-1))) * 100;
        lengthReduction = reducedLength / this.totalLength * 100;
        synapticReduction = lostSynapses / this.totalSynapses * 100;
    }

    //-----------------------------------
    public void apply(){
    	for (int index = 0; index < numUnits * numUnits; index++) {
        	parent.getNetwork().setWeight(index, trainedWeights [index] * prunedConns[index]);
        }
    }

    //-----------------------------------
    public double getAxonalReduction(){
        return this.axonalReduction;
    }

    //-----------------------------------
    public double getLengthReduction(){
        return this.lengthReduction;
    }

    //-----------------------------------
    public double getSynapticReduction(){
        return this.synapticReduction;
    }

    //-----------------------------------
    public boolean isPruned(int index){
        return prunedConns[index] == 0;
    }

}
