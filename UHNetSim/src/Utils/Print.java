package Utils;

public abstract class Print{
    private static boolean debug = false;
    
    public static void print(String msg){ System.out.print(msg); }
    
    public static void println(String msg){ print(msg + "\n"); }
    
    public static void assertResult(String msg){ println(msg); }
    
    public static void err(String msg){
        System.err.println("Error::" + msg);
        System.exit(0);
    }
    
    public static void debug(String msg){ if(debug)println(Thread.currentThread().getStackTrace()[2].getClassName() + "::" + msg); }
    
    public static void setDebug(boolean debug){ Print.debug = debug; }
    
    public static boolean getDebug(){ return debug; }
}