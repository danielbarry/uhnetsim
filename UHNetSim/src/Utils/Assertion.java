package Utils;

public abstract class Assertion{
    private static boolean assertState = false;
    
    public static void test(boolean result){
        if(assertState){
            if(result)Print.assertResult("[ OK ]" + Thread.currentThread().getStackTrace()[2]);
            else{
                Print.assertResult("[FAIL]" + Thread.currentThread().getStackTrace()[2]);
                throw new AssertionError(result);
            }
        }
    }
    
    public static void setAssert(boolean assertState){ Assertion.assertState = assertState; }
    
    public static boolean getStatus(){ return assertState; }
}