

/*
 * @(#)DynamicViewer.java
 * WKAye
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class DynamicViewer extends JInternalFrame implements ProgressReportor{
    NetSimFrame parent              = null;
    DynamicDiagram diagram          = null;
    Dynamic dynamic                 = null;

    private JToolBar tlb            = null;
    String resPath                  = null;
    boolean freezed                 = false;
    private JButton btnFreeze       = null;
    private JButton btnNewDynamic   = null;
    private JButton btnAttractors   = null;
    private JCheckBox chkLabel      = null;
    private JCheckBox chkAttraction = null;
    private JTextField txfResult    = null;
    private JButton btnSummary      = null;
    private JButton btnDetails      = null;

    DynamicViewer (NetSimFrame parent){
        super ("N/W dynamic", true, true, true, true);
        this.setTitle(this.getTitle() + "("
            + parent.getNetwork().numUnits() + " N, "
            + parent.getTrainingSet().numPatterns() + " P, "
            + Experiments.formatPercentage(parent.getPrunedNetwork().getAxonalReduction()) + " : "
            + Experiments.formatPercentage(parent.getPrunedNetwork().getLengthReduction()) + " : "
            + Experiments.formatPercentage(parent.getPrunedNetwork().getSynapticReduction()) + " d, "
            + parent.getNetDisplay ().getUpdateType() + " update"
            + ")"
        );
        this.parent = parent;

        initToolbar();
        diagram = new DynamicDiagram(parent, this);

        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add (tlb, BorderLayout.NORTH);
        this.getContentPane().add (diagram, BorderLayout.CENTER);
        this.setSize( DynamicDiagram.PANELWIDTH, DynamicDiagram.PANELHEIGHT);
        this.setLocation(100, 120);
        this.setVisible(false);

        this.addInternalFrameListener(new InternalFrameAdapter(){
            public void internalFrameClosing(InternalFrameEvent e){
                diagram.stop();
            }
    		public void internalFrameIconified(InternalFrameEvent e){
            	diagram.stop();
            }
            public void internalFrameDeiconified(InternalFrameEvent e){
            	if( !freezed ) diagram.start();
            }
        });
    }

    DynamicViewer(NetSimFrame parent, Dynamic dynamic, int finalState){
        this(parent);
        this.setTitle(this.getTitle() + " [ " + Long.toHexString(finalState) + " ]");
        this.dynamic = dynamic;
        diagram.finalState = new Integer(finalState);
    }

    public void display(){
        this.setVisible (true);
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR ));
        if( dynamic == null ){ dynamic = new Dynamic(parent, this); }
        diagram.start();
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR ));
    }

    private void initToolbar(){

    	this.tlb = new JToolBar (JToolBar.HORIZONTAL);
        resPath = new String("." + java.io.File.separatorChar + "res" + java.io.File.separatorChar);
        try {
            this.btnFreeze = new JButton (new ImageIcon (Class.forName ("DynamicViewer").getClassLoader ().getResource ( resPath + "Pause16.gif")));
            this.btnNewDynamic = new JButton(new ImageIcon (Class.forName ("DynamicViewer").getClassLoader ().getResource ( resPath + "Dynamic16.gif")));
            this.btnAttractors = new JButton(new ImageIcon (Class.forName ("DynamicViewer").getClassLoader ().getResource ( resPath + "Attractors16.gif")));
            this.btnSummary = new JButton(" Summary ");
            this.btnDetails = new JButton(" Details ");
        } catch (ClassNotFoundException e) {
        }
        this.btnFreeze.setToolTipText ("Freeze/Defreeze the diagram");
        this.btnNewDynamic.setToolTipText("Open a new window to display a tree of selected node");
        this.btnAttractors.setToolTipText("Display summary of attractors");
        this.btnSummary.setToolTipText("Display summary of the network dynamic");
        this.btnDetails.setToolTipText("Display detail figures of attractors");

        this.chkLabel = new JCheckBox("Labels", false);
        this.chkLabel.setToolTipText("Show/Hide state labels");
        this.chkAttraction = new JCheckBox("Attraction", false);
        this.chkAttraction.setToolTipText("Show/Hide attraction figures");
        this.txfResult = new JTextField(30);
        this.txfResult.setEditable(false);

		this.btnFreeze.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
            	if( freezed ){
                    shake();
                } else {
					freeze();
                }
			}
		});

        this.btnNewDynamic.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                if ( diagram.pick == null ){
                    JOptionPane.showMessageDialog(parent, "Select a node first!");
                    return;
                }
                DynamicViewer newViewer = new DynamicViewer(parent, dynamic, diagram.pick.label);
                parent.addChildFrame(newViewer);
                newViewer.display();
            }
        });

        this.btnAttractors.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                AttractorsViewer av = new AttractorsViewer(dynamic);
                parent.addChildFrame (av);
                av.setVisible(true);
            }
        });

        this.chkLabel.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                if( !diagram.isLabelVisible() && diagram.isAttractionVisible() ){
                    chkAttraction.doClick();
                }
                diagram.toggleLabel();
            }
        });

        this.chkAttraction.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                if( !diagram.isAttractionVisible() && diagram.isLabelVisible() ){
                    chkLabel.doClick();
                }
                diagram.toggleAttraction();
            }
        });

        this.btnSummary.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                parent.getOutputFrame().appendText(dynamic.getSummary ());
			}
		});

        this.btnDetails.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                parent.getOutputFrame().appendText(dynamic.getDetails());
			}
		});

        // add the buttons to the toolbar
        this.tlb.add(this.btnFreeze);
        this.tlb.add(this.btnNewDynamic);
        this.tlb.add(this.btnAttractors);
        this.tlb.addSeparator ();
        this.tlb.add(this.chkLabel);
        this.tlb.add(this.chkAttraction);
        this.tlb.addSeparator ();
        this.tlb.add(this.txfResult);
        this.tlb.addSeparator ();
        this.tlb.add(this.btnSummary);
        this.tlb.add(this.btnDetails);

        // we don't want people dragging the toolbar around
        this.tlb.setFloatable (false);
    }

    public void shake(){
    	freezed = false;
        try{
            btnFreeze.setIcon(new ImageIcon (Class.forName ("DynamicViewer").getClassLoader ().getResource ( resPath + "Pause16.gif")));
        } catch (ClassNotFoundException e) {
        }
        diagram.start();
    }

    public void freeze(){
    	freezed = true;
        try{
    	    btnFreeze.setIcon(new ImageIcon (Class.forName ("DynamicViewer").getClassLoader ().getResource ( resPath + "Play16.gif")));
        } catch (ClassNotFoundException e) {
        }
        diagram.stop();
    }

    public boolean isFreezed(){
    	return freezed;
    }

    public void setResult(String result){
        txfResult.setText(result);
    }

    public void reportProgress(String progress){
        setResult(progress);
    }
}

