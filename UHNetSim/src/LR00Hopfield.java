/**
* A class that implements the 'one shot' Hopfield learning rule
* with a learning rate of 1/N.
* 
* @author Simon Turvey + Steve Hunt 
* @version 1.1
* 
*/

public class LR00Hopfield extends AbstractLearningRule {
	final static String PLUGIN_NAME = "Standard Hopfield";

	//----------------------------------------
	public int train
	  (Network network, TrainingSet trainingSet)
	{
        int iterationCount   = 0;
        int numUnits         = 0;
        int unitCount        = 0;
        int inputCount       = 0;
        int numPatterns      = 0;
        int patternCount     = 0;
        double currentWeight = 0;
        double newWeight     = 0;
        double weightDelta   = 0;

        numUnits = network.numUnits ();
        numPatterns = trainingSet.numPatterns ();

        // for every pattern
        for (patternCount=0;patternCount<numPatterns;patternCount++) {
            // for every unit
            for (unitCount=0;unitCount<numUnits;unitCount++) {
                // for every input
                for (inputCount=0;inputCount<numUnits;inputCount++) {
                    // skip over self-connections
                    if (unitCount == inputCount) continue;

                    // retrieve the current weight for this unit/input pair
                    currentWeight = network.getWeight (unitCount, inputCount);

                    // calculate the weight change
                    weightDelta = trainingSet.getUnit (patternCount, unitCount) * trainingSet.getUnit (patternCount, inputCount);
                    weightDelta = weightDelta / network.numUnits ();

                    // calculate the new weight
                    newWeight = currentWeight + weightDelta;

                    // update the weight for this pair
                    network.setWeight (unitCount, inputCount, newWeight);
                }
            }
        }
        iterationCount++;

        return iterationCount;
	}

	public int train
	  (Network network, TrainingSet trainingSet, int maxIterations, double learningRate, double target)
	{
        //---- This overloading of the 'train' method is included for future expansion only: do not use it
	    return 0;
	}


	//----------------------------------------
	public String getName () {
		return PLUGIN_NAME;
	}
}
