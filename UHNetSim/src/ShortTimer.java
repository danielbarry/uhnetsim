

import java.util.GregorianCalendar;
import java.util.Vector;

public class ShortTimer extends GregorianCalendar {
    private boolean started   = false;
    private int currentStart  = 0;
    private int currentEnd    = 0;
    private Vector splitEnd   = new Vector ();
    private Vector splitStart = new Vector ();

	//----------------------------------------
	public void startTimer () {
        this.currentStart = 0;
        this.currentEnd = 0;
        this.splitEnd.clear ();
        this.splitStart.clear ();
        this.started = true;
        
		this.splitStart.add (this.currentStart++, new Long (System.currentTimeMillis ()));
	}

    //----------------------------------------
	public void split () {		
        Long currentTime = null;

        currentTime = new Long (System.currentTimeMillis ());
        this.splitEnd.add (this.currentEnd++, currentTime);
        this.splitStart.add (this.currentStart++, currentTime);
	}

	//----------------------------------------
    public void splitAndPause () {		
        this.splitEnd.add (this.currentEnd++, new Long (System.currentTimeMillis ()));
    }

	//----------------------------------------
    public void resume () {
        if (started == false) {
            this.currentStart = 0;
            this.currentEnd = 0;
            this.splitEnd.clear ();
            this.splitStart.clear ();
            this.started = true;
        }
        this.splitStart.add (this.currentStart++, new Long (System.currentTimeMillis ()));
    }
    
    //----------------------------------------
	public int numSplits () {
        return this.splitStart.size ();
    }

    //----------------------------------------
	public void stopTimer () {
        this.splitEnd.add (this.currentEnd++, new Long (System.currentTimeMillis ()));
    }
	
	//----------------------------------------
	public String splitTimeAsString (int split) {
		Long startValue  = null;
        Long endValue    = null;
        String outString = null;

		startValue = (Long)splitStart.elementAt (split);
		endValue = (Long)splitEnd.elementAt (split);

        super.setTimeInMillis(endValue.longValue () - startValue.longValue ());
		super.computeFields ();

		outString = fields[HOUR] + ":" + fields[MINUTE] + ":" + fields[SECOND] + "." + fields[MILLISECOND];
        return outString;
	}

    //----------------------------------------
	public String totalTimeAsString () {
        int index         = 0;
        Long startValue   = null;
        Long endValue     = null;
        long currentTotal = 0;
        String outString  = null;

        for (index=0;index<this.splitStart.size ();index++) {
            startValue = (Long)splitStart.elementAt (index);
            endValue = (Long)splitEnd.elementAt (index);
            currentTotal += endValue.longValue () - startValue.longValue ();
        }
		super.setTimeInMillis(currentTotal);
		super.computeFields ();

        outString = fields[HOUR] + ":" + fields[MINUTE] + ":" + fields[SECOND] + "." + fields[MILLISECOND];
        return outString;
    }

    /*------------ BROKEN ----------------------------
	public String splitStdDev () {
        Long currentValue   = null;
        long meanMillis     = 0;
        long sumSquaredDiff = 0;
        double stdDev       = 0;
        int splitCount      = 0;
        String outString    = null;

        currentValue = (Long)splitTimes.elementAt (this.splitTimes.size () - 1);
        meanMillis = (currentValue.longValue () - this.startTimerValue.longValue ()) / this.splitTimes.size ();
        
        for (splitCount=0;splitCount<this.splitTimes.size ();splitCount++) {
            currentValue = (Long)splitTimes.elementAt (splitCount);
            sumSquaredDiff += Math.pow ((currentValue.longValue () - meanMillis), 2);
        }
        stdDev = Math.sqrt (sumSquaredDiff / this.splitTimes.size ());

        super.setTimeInMillis ((long)stdDev);
		super.computeFields ();

        outString = fields[HOUR] + ":" + fields[MINUTE] + ":" + fields[SECOND] + "." + fields[MILLISECOND];
        return outString;
    } */

    //----------------------------------------
	public String splitMean () {
        int index         = 0;
        Long startValue   = null;
        Long endValue     = null;
        long currentTotal = 0;
        long meanMillis   = 0;
        String outString  = null;

        for (index=0;index<this.splitStart.size ();index++) {
            startValue = (Long)splitStart.elementAt (index);
            endValue = (Long)splitEnd.elementAt (index);
            currentTotal += endValue.longValue () - startValue.longValue ();
        }

        meanMillis = currentTotal / this.splitStart.size ();
        super.setTimeInMillis (meanMillis);
		super.computeFields ();

        outString = fields[HOUR] + ":" + fields[MINUTE] + ":" + fields[SECOND] + "." + fields[MILLISECOND];
        return outString;
    }
}
