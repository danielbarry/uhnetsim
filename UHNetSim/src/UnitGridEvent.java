

import java.awt.*;
import java.util.*;

public class UnitGridEvent extends EventObject {
    public int unitX                    = 0;
    public int unitY                    = 0;
    public boolean unitState            = false;
    public Dimension unitGridDimensions = null;

    public UnitGridEvent (Object source) {
        super (source);
    }
}