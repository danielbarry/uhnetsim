

import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

public class Network implements Runnable {
    final static int SYNC                  = 0;
    final static int ASYNC_FIXED           = 1;
    final static int ASYNC_RAND_REMOVE     = 2;
    final static int ASYNC_RAND_REPLACE    = 3;
    final static int NOT_KNOWN             = -1;
    final static int BIPOLAR               = 0;
    final static int BINARY                = 1;
    final static int LOAD_SUCCESS          = -1;
    final static int SAVE_SUCCESS          = -1;
    final static int IO_EXCEPTION          = 0;
    final static int FILE_NOT_FOUND        = 1;
    final static int NO_TRAINING_SET       = 2;
    final static int NO_SIZE               = 3;
    final static int INVALID_SIZE          = 4;
    final static int PREMATURE_END         = 5;
    final static int INVALID_WEIGHT        = 6;
    final static int NO_TRAINING_RULE      = 7;
    final static int INVALID_TRAINING_RULE = 8;
    final static int TRAIN                 = 0;
    final static int RUN_ONE               = 1;
    final static int RUN                   = 2;
    final static int ALL_STABLE            = 3;

    private int[] currentState        = null;
    private int[] newState            = null;
    private double[] weights          = null;
    private int numUnits              = 0;
    private int patternType           = Network.NOT_KNOWN;
    private int updateType            = Network.ASYNC_RAND_REMOVE;
    private int numTrainingIterations = 0;
    private int highValue             = 1;
    private int lowValue              = -1;
    private int runOperation          = -1;
    private boolean stopRun           = false;
    
    private int stableCount = 0; //Stable pattern count

    private String trainingSetFileName = null;
    private ArrayList fullNumberList   = null;
    private Random numberGenerator     = null;
    private AbstractLearningRule learningRule = null;
    private File trainingSetFile       = null;
    private NetSimFrame parent         = null;

    //-----------------------------------
    public Network (NetSimFrame parent) {
        this.parent = parent;
    }
    
    /**
     * Gets the number of stable patterns
     * @return number of stable patterns
     */
    public int getStableCount(){ return stableCount; }

    //-----------------------------------
    public void buildNetwork (int numUnits) {
        int index = 0;

        this.numUnits = numUnits;
        this.weights = new double[numUnits*numUnits];
        this.currentState = new int[numUnits];
        this.newState = new int[numUnits];

        this.fullNumberList = new ArrayList ();
        this.numberGenerator = new Random ();

        for (index=0;index<numUnits*numUnits;index++) {
			if (index < this.numUnits) this.fullNumberList.add (index, new Integer (index));
            this.weights [index] = 0;
        }
    }

    //-----------------------------------
    public void clearCurrentState () {
        Arrays.fill (this.currentState, this.lowValue);
    }

    //-----------------------------------
    public int getUnit (int unitIndex) {
        if (unitIndex >= this.currentState.length) {
            System.out.println ("unitIndex is greater than the number of units in Network.getCurrentUnit (int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        return this.currentState[unitIndex];
    }

    //-----------------------------------
    public int[] getUnits () {
        int unitIndex     = 0;
        int[] tempPattern = new int[this.currentState.length];

        for (unitIndex=0;unitIndex<this.currentState.length;unitIndex++) {
            tempPattern[unitIndex] = this.getUnit (unitIndex);
        }
        return tempPattern;
    }

    //-----------------------------------
    public boolean getBooleanUnit (int unitIndex) {
        if (unitIndex >= this.currentState.length) {
            System.out.println ("Attempted to access unit that does not exist in Network.getBooleanUnit (int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        return (this.currentState[unitIndex] == 1) ? true : false;
    }

    //-----------------------------------
    public boolean[] getBooleanUnits () {
        int unitIndex         = 0;
        boolean[] tempPattern = new boolean[this.currentState.length];

        for (unitIndex=0;unitIndex<this.currentState.length;unitIndex++) {
            tempPattern[unitIndex] = this.getBooleanUnit (unitIndex);
        }
        return tempPattern;
    }

    //-----------------------------------
	public void toggleUnit (int unitIndex) {
        if (unitIndex >= this.currentState.length) {
            System.out.println ("unitIndex is greater than the number of units in Network.toggleUnit (int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        if (this.getUnit (unitIndex) != this.highValue) {
            this.setUnit (unitIndex, this.highValue);
        } else {
            this.setUnit (unitIndex, this.lowValue);
        }
    }

    //-----------------------------------
    public long getLongState(){
        return getLongState(currentState);
    }

    //-----------------------------------
    public static long getLongState(int[] state){
        long decimalState = 0;
        for( int j = 0; j < state.length; j++) {
            decimalState += ((state[j]==1? 1: 0) * Math.pow(2, (state.length - j -1)));
        }
        return decimalState;
    }

    //-----------------------------------
    public void setLongState(long decimalState){

        char[] s = new String(Long.toBinaryString(decimalState)).toCharArray() ;
        for( int i=0; i<numUnits; i++){
            if( i<(numUnits-s.length) ){
                this.currentState [i] = this.lowValue;
            }else{
                if(s[i-(numUnits-s.length)]=='1'){
                    currentState [i] = highValue;
                }else{
                    currentState [i] = lowValue;
                }
            }
        }
    }

    //-----------------------------------
    public void setUnits (int[] newState) {
        int unitIndex = 0;

        if (newState.length != this.currentState.length) {
            System.out.println ("New network state contains the wrong number of units in Network.setCurrentState (boolean[])");
            Thread.dumpStack ();
            System.exit (2);
        }
        for (unitIndex=0;unitIndex<this.currentState.length;unitIndex++) {
            this.currentState[unitIndex] = newState[unitIndex];
        }
    }

    //-----------------------------------
	public void setUnit (int unitIndex, int state) {
        if (unitIndex >= this.currentState.length) {
            System.out.println ("unitIndex is greater than the number of units in Network.setCurrentUnit (int, boolean)");
            Thread.dumpStack ();
            System.exit (2);
        }
        if ((state != this.highValue) && (state != this.lowValue)) {
            System.out.println ("Network state does not match pattern type in Network.setCurrentUnit (int, boolean)");
            Thread.dumpStack ();
            System.exit (2);
        }
        this.currentState [unitIndex] = state;
    }

    //-----------------------------------
    public void setUnit (int unitX, int unitY, boolean state) {
        if (state == true) {
            this.setUnit (this.XYtoX (unitX, unitY), this.highValue);
        } else {
            this.setUnit (this.XYtoX (unitX, unitY), this.lowValue);
        }
    }

    //-----------------------------------
    private int XYtoX (int x, int y) {
        return ((y * this.numUnits) + x);
    }

    //-----------------------------------
    private Dimension XtoXY (int x) {
        return new Dimension (x % this.numUnits, (int)x / this.numUnits);
    }

    //-----------------------------------
    public void setRunOperation (int runOperation) {
        if ((runOperation < Network.TRAIN) || (runOperation > Network.ALL_STABLE)) {
            System.out.println ("Attempted to set an unknown network operation in Network.setRunOperation (int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        this.runOperation = runOperation;
    }

    //-----------------------------------
    public void run () {
        int numChanged   = 1;
        int patternIndex = 0;
        stableCount  = 0;

        switch (this.runOperation) {
            case Network.TRAIN:
                this.parent.setMenusEnabled (false);
                this.parent.getNetDisplay ().setEnabled (false);
                this.parent.getTrainingSetDisplay ().setEnabled (false);

                this.parent.getTrainingStats ().init ();
                this.parent.getTrainingStats ().setLearningRule (learningRule.getName ());
                this.parent.getTrainingStats ().setNumPatterns (this.parent.getTrainingSet ().numPatterns ());
                this.parent.getTrainingNotify ().startTraining ();
                this.parent.getTrainingNotify ().setVisible (true);

                this.parent.getShortTimer ().startTimer ();
                this.parent.getNetwork ().train ();
                this.parent.getShortTimer ().stopTimer ();

                this.parent.getTrainingStats ().setTrainingTime (this.parent.getShortTimer ().splitTimeAsString (0));
                this.parent.getTrainingStats ().setNumIterations (this.getNumTrainingIterations ());
                this.parent.getTrainingNotify ().doneTraining ();

                break;
            case Network.RUN_ONE:
                // disable the windows and menu
                this.parent.setMenusEnabled (false);
                this.parent.getNetDisplay ().setEnabled (false);
                this.parent.getTrainingSetDisplay ().setEnabled (false);

                // run the network for an iteration
                numChanged = this.runOneIteration ();

                this.parent.getNetDisplay ().setUnits (this.getBooleanUnits ());
                this.parent.getNetDisplay ().refreshUnitGrid ();

                // enable the windows and menu
                this.parent.setMenusEnabled (true);
                this.parent.getNetDisplay ().setEnabled (true);
                this.parent.getTrainingSetDisplay ().setEnabled (true);
                break;
            case Network.RUN:
                this.stopRun = false;

                this.parent.setMenusEnabled (false);
                this.parent.getNetDisplay ().setEnabled (false);
                this.parent.getTrainingSetDisplay ().setEnabled (false);

                while (numChanged > 0 && this.stopRun == false) {
                    numChanged = this.runOneIteration ();
                }
                this.parent.getNetDisplay ().setRunning (false);
                this.parent.getNetDisplay ().setUnits (this.getBooleanUnits ());
                this.parent.getNetDisplay ().refreshUnitGrid ();

                this.parent.setMenusEnabled (true);
                this.parent.getNetDisplay ().setEnabled (true);
                this.parent.getTrainingSetDisplay ().setEnabled (true);
                break;
            case Network.ALL_STABLE:
                this.parent.setMenusEnabled (false);
                this.parent.getNetDisplay ().setEnabled (false);
                this.parent.getTrainingSetDisplay ().setEnabled (false);

                stableCount = 0;

                for (patternIndex=0;patternIndex<this.parent.getTrainingSet ().numPatterns ();patternIndex++) {
                    this.setUnits (this.parent.getTrainingSet ().getPattern (patternIndex));
                    this.parent.getNetDisplay ().setUnits (this.parent.getTrainingSet ().getBooleanPattern (patternIndex));
                    this.parent.getNetDisplay ().refreshUnitGrid ();

                    numChanged = this.runOneIteration ();
                    if (numChanged > 0) stableCount++;
                }

                if (stableCount > 0) {
                    JOptionPane.showMessageDialog (null, stableCount + " pattern(s) is/are not stable", "Pattern stability", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog (null, "All patterns are stable", "Pattern stability", JOptionPane.INFORMATION_MESSAGE);
                }

                this.parent.setMenusEnabled (true);
                this.parent.getNetDisplay ().setEnabled (true);
                this.parent.getTrainingSetDisplay ().setEnabled (true);
                break;
            default:
                // do nothing
        }
    }
    
    //-----------------------------------
    public void run (boolean display) {
        int numChanged   = 1;
        int patternIndex = 0;
        stableCount  = 0;

        switch (this.runOperation) {
            case Network.TRAIN:
                this.parent.setMenusEnabled (false);
                this.parent.getNetDisplay ().setEnabled (false);
                this.parent.getTrainingSetDisplay ().setEnabled (false);

                this.parent.getTrainingStats ().init ();
                this.parent.getTrainingStats ().setLearningRule (learningRule.getName ());
                this.parent.getTrainingStats ().setNumPatterns (this.parent.getTrainingSet ().numPatterns ());
                this.parent.getTrainingNotify ().startTraining ();
                this.parent.getTrainingNotify ().setVisible (true);

                this.parent.getShortTimer ().startTimer ();
                this.parent.getNetwork ().train ();
                this.parent.getShortTimer ().stopTimer ();

                this.parent.getTrainingStats ().setTrainingTime (this.parent.getShortTimer ().splitTimeAsString (0));
                this.parent.getTrainingStats ().setNumIterations (this.getNumTrainingIterations ());
                this.parent.getTrainingNotify ().doneTraining ();

                break;
            case Network.RUN_ONE:
                // disable the windows and menu
                this.parent.setMenusEnabled (false);
                this.parent.getNetDisplay ().setEnabled (false);
                this.parent.getTrainingSetDisplay ().setEnabled (false);

                // run the network for an iteration
                numChanged = this.runOneIteration ();

                this.parent.getNetDisplay ().setUnits (this.getBooleanUnits ());
                this.parent.getNetDisplay ().refreshUnitGrid ();

                // enable the windows and menu
                this.parent.setMenusEnabled (true);
                this.parent.getNetDisplay ().setEnabled (true);
                this.parent.getTrainingSetDisplay ().setEnabled (true);
                break;
            case Network.RUN:
                this.stopRun = false;

                this.parent.setMenusEnabled (false);
                this.parent.getNetDisplay ().setEnabled (false);
                this.parent.getTrainingSetDisplay ().setEnabled (false);

                while (numChanged > 0 && this.stopRun == false) {
                    numChanged = this.runOneIteration ();
                }
                this.parent.getNetDisplay ().setRunning (false);
                this.parent.getNetDisplay ().setUnits (this.getBooleanUnits ());
                this.parent.getNetDisplay ().refreshUnitGrid ();

                this.parent.setMenusEnabled (true);
                this.parent.getNetDisplay ().setEnabled (true);
                this.parent.getTrainingSetDisplay ().setEnabled (true);
                break;
            case Network.ALL_STABLE:
                this.parent.setMenusEnabled (false);
                this.parent.getNetDisplay ().setEnabled (false);
                this.parent.getTrainingSetDisplay ().setEnabled (false);

                stableCount = 0;

                for (patternIndex=0;patternIndex<this.parent.getTrainingSet ().numPatterns ();patternIndex++) {
                    this.setUnits (this.parent.getTrainingSet ().getPattern (patternIndex));
                    this.parent.getNetDisplay ().setUnits (this.parent.getTrainingSet ().getBooleanPattern (patternIndex));
                    this.parent.getNetDisplay ().refreshUnitGrid ();

                    numChanged = this.runOneIteration ();
                    if (numChanged > 0) stableCount++;
                }

                if(display){
                    if(stableCount>0)JOptionPane.showMessageDialog (null, stableCount + " pattern(s) is/are not stable", "Pattern stability", JOptionPane.INFORMATION_MESSAGE);
                    else JOptionPane.showMessageDialog (null, "All patterns are stable", "Pattern stability", JOptionPane.INFORMATION_MESSAGE);
                }
                
                this.parent.setMenusEnabled (true);
                this.parent.getNetDisplay ().setEnabled (true);
                this.parent.getTrainingSetDisplay ().setEnabled (true);
                break;
            default:
                // do nothing
        }
    }

    //-----------------------------------
    public void train () {
        // reinitialise the network weight array
        this.weights = new double[numUnits*numUnits];
        // train the network
        this.numTrainingIterations = this.learningRule.train (this, parent.getTrainingSet ());

        this.parent.getPrunedNetwork().rebuild();
    }

    //-----------------------------------
    public int runOneIteration () {
        int unitNumbersIndex = 0;
        int unitIndex        = 0;
        int inputIndex       = 0;
        int changeCount      = 0;
        int updateCount      = 0;
        int currentUnit      = 0;
        double localField    = 0;

        ArrayList unitNumbers = new ArrayList ();
        unitNumbers.addAll (this.fullNumberList);

        while (updateCount < this.numUnits) {
            // pick a unit according to the update method
            if ((this.updateType == Network.ASYNC_RAND_REMOVE) || (this.updateType == Network.ASYNC_RAND_REPLACE)) {
                // pick a unit at random
                unitNumbersIndex = numberGenerator.nextInt (unitNumbers.size ());
                unitIndex = ((Integer)unitNumbers.get (unitNumbersIndex)).intValue ();
                // should we remove this unit from the list of available ones
                if (this.updateType == Network.ASYNC_RAND_REMOVE) {
                    unitNumbers.remove (unitNumbersIndex);
                }
            } else {
                // pick the next unit in line
                unitIndex = updateCount;
            }
            // reinitialise the loop affected variables
            localField = 0;
            // sum the weight-affected inputs
            for (inputIndex=0;inputIndex<this.numUnits;inputIndex++) {
                // add on this input's contribution
                localField += this.weights[this.XYtoX (unitIndex, inputIndex)] * this.currentState[inputIndex];
            }

            // test the local field
            if (localField > 0) {
                if (this.currentState[unitIndex] != this.highValue) {
                    if (this.updateType == Network.SYNC) {
                        this.newState[unitIndex] = this.highValue;
                    } else {
                        this.currentState[unitIndex] = this.highValue;
                    }
                    changeCount++;
                } else {
                    if (this.updateType == Network.SYNC) {
                        this.newState[unitIndex] = this.currentState[unitIndex];
                    }
                }
            } else if (localField < 0) {
                if (this.currentState[unitIndex] != this.lowValue) {
                    if (this.updateType == Network.SYNC) {
                        this.newState[unitIndex] = this.lowValue;
                    } else {
                        this.currentState[unitIndex] = this.lowValue;
                    }
                    changeCount++;
                } else {
                    if (this.updateType == Network.SYNC) {
                        this.newState[unitIndex] = this.currentState[unitIndex];
                    }
                }
            } else {
                if (this.updateType == Network.SYNC) this.newState[unitIndex] = this.currentState[unitIndex];
            }
            updateCount++;
        }

        // make the new state the current one for synchronous updates
        if (this.updateType == Network.SYNC) {
            for (unitIndex=0;unitIndex<this.numUnits;unitIndex++) {
                this.currentState[unitIndex] = this.newState[unitIndex];
            }
        }
        return changeCount;
    }

    //-----------------------------------
    public int loadNetwork (File file) {
        FileReader fileReader           = null;
        StreamTokenizer streamTokenizer = null;
        int weightIndex                 = 0;
        String className                = null;
        Class learningRuleClass         = null;
		LearningRuleLoader loader       = null;

        // open the file if we can
        try {
            fileReader = new FileReader (file);
        } catch (FileNotFoundException e) {
            return Network.FILE_NOT_FOUND;
        }
        streamTokenizer = new StreamTokenizer (fileReader);
        streamTokenizer.eolIsSignificant (false);
        streamTokenizer.wordChars ((int)':', (int)':');
        streamTokenizer.wordChars ((int)'\\', (int)'\\');
        streamTokenizer.wordChars ((int)'/', (int)'/');

        // set the network's pattern type to bipolar until we know what it really is from
        // the training set
        this.patternType = Network.BIPOLAR;

        try {
            // the first line will be the filename of the training set for this network
            if (streamTokenizer.nextToken () != StreamTokenizer.TT_WORD) {
                fileReader.close ();
                return Network.NO_TRAINING_SET;
            }
            this.trainingSetFileName = streamTokenizer.sval;
            this.trainingSetFile = new File (file.getParent() + File.separatorChar + this.trainingSetFileName);

            // the second line holds the name of the learning rule
            if (streamTokenizer.nextToken () != StreamTokenizer.TT_WORD) {
                fileReader.close ();
                return Network.NO_TRAINING_RULE;
            }
            className = streamTokenizer.sval;

  			loader = new LearningRuleLoader (".");
            this.learningRule = loader.loadLearningRule (className + ".class");

            if (this.learningRule == null) return Network.INVALID_TRAINING_RULE;

            // the third line is the number of units in the network
            if (streamTokenizer.nextToken () != StreamTokenizer.TT_NUMBER) {
                fileReader.close ();
                return Network.NO_SIZE;
            }
            this.numUnits = (int)streamTokenizer.nval;

            // validate the network size
            if (this.numUnits <= 0 /*|| this.numUnits % 2 != 0*/) {
                fileReader.close ();
                return Network.INVALID_SIZE;
            }
            this.buildNetwork (this.numUnits);

            // load the weights
            while (true) {
                streamTokenizer.nextToken ();
                if (streamTokenizer.ttype == StreamTokenizer.TT_EOF) {
                    if (weightIndex != this.numUnits * this.numUnits) {
                        fileReader.close ();
                        return Network.PREMATURE_END;
                    } else {
                        fileReader.close ();
                        return Network.LOAD_SUCCESS;
                    }
                } else if (streamTokenizer.ttype != StreamTokenizer.TT_NUMBER) {
                    String s = streamTokenizer.sval;
                    if ( s.indexOf("E") == -1 ) {
                      fileReader.close();
                      return Network.INVALID_WEIGHT;
                    }

                    Double d = Double.valueOf( String.valueOf (this.weights[weightIndex-1]) + s);
                    this.weights[weightIndex-1] = d.doubleValue();
                    continue;
                }
                this.weights[weightIndex++] = streamTokenizer.nval;
            }
        } catch (IOException e) {
            return Network.IO_EXCEPTION;
        }
    }

    //-----------------------------------
    public int saveNetwork (File file) {
        int unitX                   = 0;
        int unitY                   = 0;
        int weightIndex             = 0;
        FileWriter fileWriter       = null;
        Dimension networkDimensions = null;
        String outString            = null;

        try {
        	fileWriter = new FileWriter (file);

            // write out the training set filename for this network
            //fileWriter.write (this.trainingSetFile.getAbsolutePath () + "\n");
            fileWriter.write (this.trainingSetFile.getName() + "\n");

            // write out the name of the learning rule
            //fileWriter.write ("LR" + this.learningRule.getName () + "\n");
            fileWriter.write (this.learningRule.getClass ().getName() + "\n");

            // write out the network size
            fileWriter.write (this.numUnits + "\n");

            networkDimensions = this.XtoXY (this.weights.length - 1);

            for (unitY=0;unitY<=networkDimensions.height;unitY++) {
                for (unitX=0;unitX<=networkDimensions.width;unitX++) {
                    outString = Double.toString (weights[weightIndex++]) + " ";
                    if (outString.toCharArray ()[0] != '-') outString = " " + outString;
                    fileWriter.write (outString);
                }
                fileWriter.write ("\n");
            }
            fileWriter.close ();
            return Network.SAVE_SUCCESS;
        } catch (IOException e) {
            return Network.IO_EXCEPTION;
        }
    }

    //-----------------------------------
    public int numUnits () {
        return this.numUnits;
    }

    //-----------------------------------
    public void setUpdateType (int updateType) {
        switch (updateType) {
            case 0:
                this.updateType = Network.SYNC;
                break;
            case 1:
                this.updateType = Network.ASYNC_FIXED;
                break;
            case 2:
                this.updateType = Network.ASYNC_RAND_REMOVE;
                break;
            case 3:
                this.updateType = Network.ASYNC_RAND_REPLACE;
                break;
            default:
                System.out.println ("Attempted to set unknown update type in Network.setUpdateType (int)");
                Thread.dumpStack ();
                System.exit (2);
        }
    }

    //-----------------------------------
    public void setLearningRule (AbstractLearningRule learningRule) {
        this.learningRule = learningRule;
    }

   //-----------------------------------
    public AbstractLearningRule getLearningRule () {
        return (this.learningRule);
    }

    //-----------------------------------
    public void setPatternType (int patternType) {
        if (patternType != Network.BIPOLAR && patternType != Network.BINARY) {
            System.out.println ("Attempted to set the pattern type to an illegal value in Network.setPatternType (int)");
            Thread.dumpStack ();
            System.exit (2);
        }

        this.patternType = patternType;

        if (patternType == Network.BIPOLAR) {
            this.lowValue = -1;
        } else {
            this.lowValue = 0;
        }
    }

    //-----------------------------------
    public int getPatternType () {
        return this.patternType;
    }

    //-----------------------------------
    public File getTrainingSetFile () {
        return this.trainingSetFile;
    }

    //-----------------------------------
    public void setTrainingSetFile (File trainingSetFile) {
        this.trainingSetFile = trainingSetFile;
    }

    //-----------------------------------
    public int getNumTrainingIterations () {
        return this.numTrainingIterations;
    }

    //-----------------------------------
    public int lowValue () {
        return this.lowValue;
    }

    //-----------------------------------
    public int highValue () {
        return this.highValue;
    }

    //-----------------------------------
    public double getWeight (int to, int from) {
        if ((to < 0) || (to >= this.numUnits) || ((from < 0)) || (to >= this.numUnits)) {
            System.out.println ("Attempted to access weight outside of matrix in Network.getWeight (int, int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        return weights [this.XYtoX (to, from)];
    }

    //-----------------------------------
    public void setWeight (int to, int from, double value) {
        if ((to < 0) || (to >= this.numUnits) || ((from < 0)) || (to >= this.numUnits)) {
            System.out.println ("Attempted to access weight outside of matrix in Network.setWeight (int, int)");
            Thread.dumpStack ();
            System.exit (2);
        } else {
            this.weights [this.XYtoX (to, from)] = value;
        }
    }

    //-----------------------------------
    public double getWeight (int index) {
        if ( (index < 0) || ( index >= this.numUnits * this.numUnits) ){
            System.out.println ("Attempted to access weight outside of matrix in Network.getWeight (int, int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        return weights [index];
    }

    //-----------------------------------
    public void setWeight (int index, double value) {
        if ( (index < 0) || ( index >= this.numUnits * this.numUnits) ){
            System.out.println ("Attempted to access weight outside of matrix in Network.setWeight (int, int)");
            Thread.dumpStack ();
            System.exit (2);
        } else {
            this.weights [index] = value;
        }
    }

    //-----------------------------------
    public double getStrongestWeight(){
        double strongest = 0.0;

        for(int i = 0; i < numUnits*numUnits; i++){
            strongest = ( Math.abs(weights[i]) > strongest ? Math.abs(weights[i]): strongest);
        }

        return strongest;
    }

    //-----------------------------------
    public void stopRun () {
        this.stopRun = true;
    }

    //-----------------------------------
    static public void printIntArray (String prefix, int[] array) {
        System.out.print (prefix + " ");

        for (int i=0;i<array.length;i++) {
            System.out.print (array[i] + " ");
        }
        System.out.println ();
    }

    //-----------------------------------
    static public void printIntArray (String prefix, int width, int height, int[] array) {
        int x, y;
        int outVal;

        System.out.println (prefix + " ");

        for (y=0;y<height;y++) {
            for (x=0;x<width;x++) {
                outVal = array[(y * height) + x];
                if (outVal != -1) System.out.print (" ");
                System.out.print (outVal + " ");
            }
            System.out.println ();
        }
        System.out.println ();
    }

    //-----------------------------------
    static public void printBooleanArray (String prefix, boolean[] array) {
        System.out.print (prefix + " ");

        for (int i=0;i<array.length;i++) {
            System.out.print (array[i] + " ");
        }
        System.out.println ();
    }
}
