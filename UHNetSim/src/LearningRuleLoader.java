

import java.io.*;

public class LearningRuleLoader extends ClassLoader {
	private File learningRuleDir	= null;
    private String m_root			= null;

    //-----------------------------------
    public LearningRuleLoader (String root) throws FileNotFoundException {
        this (LearningRuleLoader.class.getClassLoader (), root);

        this.learningRuleDir = new File (m_root);
    }

    //-----------------------------------
    public LearningRuleLoader(ClassLoader parent, String root) throws FileNotFoundException {
        // ensure we defer to parent appropriately
        super (parent);

        // test to make sure root is a legitimate directory on the local filesystem
        File f = new File (root /*+ File.separatorChar + "classes"*/);
        //File f = new File (".."  + File.separatorChar + "classes");

        if (f.isDirectory ()) {
            m_root = root;
            //m_root = ".." + File.separatorChar + "classes";
        } else {
            throw new FileNotFoundException ();
        }
    }

    //-----------------------------------
    // correct
    public byte[] findClassBytes (String className) {
        try {
            String pathName = m_root + File.separatorChar + className; // .replace ('.', File.separatorChar) + ".class";
            
            // try to open the file and read in its contents
            FileInputStream inFile = new FileInputStream (pathName);
            byte[] classBytes = new byte[inFile.available ()];
            inFile.read (classBytes);

            return classBytes;
        } catch (java.io.IOException ioEx) {
            return null;
        }
    }

     //-----------------------------------
    public Class findClass (String name) throws ClassNotFoundException {
        byte[] classBytes = this.findClassBytes (name);
        if (classBytes == null) {
            throw new ClassNotFoundException();
        } else {
            return super.defineClass (name.substring (0, name.length () - 6) , classBytes, 0, classBytes.length);
        }
    }

    //----------------------------------------
    public AbstractLearningRule loadLearningRule (String fileName) {
    	Class newLRClass = null;
    	AbstractLearningRule newLR	= null;

		try {
    		newLRClass = this.findClass(fileName);
    		if (this.validateClass (newLRClass) == true) newLR = (AbstractLearningRule)newLRClass.newInstance ();
    	} catch (ClassNotFoundException e) {
    		Thread.dumpStack ();
    		System.exit (2);
    	} catch (InstantiationException e) {
    		Thread.dumpStack ();
    		System.exit (2);
    	} catch (IllegalAccessException e) {
    		Thread.dumpStack ();
    		System.exit (2);
    	}

    	return newLR;
    }

    //----------------------------------------
    public String[] findLearningRules () {
        return (this.learningRuleDir.list (new LRFileFilter ()));
    }

    //----------------------------------------
    private boolean validateClass (Class LRClass) {
		String className = null;

		if (LRClass == null) return (false);

		className = LRClass.getSuperclass ().getName ();

		if (className.compareTo ("AbstractLearningRule") != 0) return (false);

        return (true);
	}

    //----------------------------------------
    public class LRFileFilter implements FilenameFilter {
    	public boolean accept(File fileDirectory, String fileName) {
	    	if (fileName.startsWith ("LR")
                && (fileName.indexOf (".class") != -1)

            ){
		    	return true;
    		} else {
        		return false;
            }
    	}
    }
}
