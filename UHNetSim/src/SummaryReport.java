

import java.util.*;

public class SummaryReport{

	String title 				= "";
    String[] columnHeadings 	= null;
	int[] classBorders 			= null;
    int numClasses 				= 1;
    int[] groupBorders          = null;
    int numGroups               = 1;
    ArrayList fixedLines 		= null;
    ArrayList dataLines 		= null;
    ArrayList currentLineData 	= null;
    ArrayList summaryLines 		= null;
    static String colDelimeter  = "\t";


    SummaryReport(){
    	classBorders = new int[0];
    	groupBorders = new int[0];
    	fixedLines = new ArrayList();
        dataLines = new ArrayList();
    }

    public void setTitle(String title){ this.title = title; }
    public String getTitle(){ return this.title; }

    public void setColumnHeadings(String[] headings){
    	this.columnHeadings = new String[headings.length];
        for(int i=0; i<headings.length; i++){
        	this.columnHeadings[i] = headings[i];
        }
    }
    public String getColumnHeadings(){
    	String headings = "";
        for(int i=0; i<this.columnHeadings.length; i++){
			headings += this.columnHeadings[i] + colDelimeter;
        }
        return headings;
    }

    public void setClassBorders(int[] borders){
    	this.numClasses = borders.length + 1;
		classBorders = new int[borders.length];
        for(int i=0; i<borders.length; i++){
        	classBorders[i] = borders[i];
        }
    }

    public void setGroupBorders(int[] borders){
    	this.numGroups = borders.length + 1;
		groupBorders = new int[borders.length];
        for(int i=0; i<borders.length; i++){
        	groupBorders[i] = borders[i];
        }
    }

	public void addLine(float[] entryLine){
		fixedLines.add(entryLine);
        if ( currentLineData != null ) {
        	dataLines.add( currentLineData );
        }
        currentLineData = new ArrayList();
    }

    public void addData(int value){
		currentLineData.add(new Integer(value));
    }

    public String toString(){
    	String body = "";
        int numLines = 0;

        prepareSummary();
		numLines = summaryLines.size();

        for(int i=0; i<numLines; i++){
        	float[] fixCols = (float[])fixedLines.get(i);
			for(int j=0; j<fixCols.length; j++){
            	body += (formatFloat(fixCols[j]) + colDelimeter);
            }
            int[] data = (int[])summaryLines.get(i);
            for(int j=0; j<data.length; j++){
            	body += ( data[j] + colDelimeter);
            }
            body += "\n";
        }

		return body;
    }

    private float formatFloat(float f){
		return ( (float)((int)(f*100))/100 );
    }

    public void prepareSummary(){
        if ( currentLineData != null ) {
        	dataLines.add( currentLineData );
            currentLineData = null;
        }
        int numLines = dataLines.size();
        summaryLines = new ArrayList();

        for(int i=0; i<numLines; i++){
        	int[] summary = new int[numClasses];
            ArrayList data = (ArrayList) dataLines.get(i);
            int numCols = data.size();
            for(int j=0; j<numCols; j++){
            	int classIndex = numClasses-1;
            	for(int k=0; k<classBorders.length; k++){
					if ( ((Integer)data.get(j)).intValue() < classBorders[k]){
                    	classIndex = k;
                        break;
                    }
                }
                summary[classIndex]++;
            }
            summaryLines.add(summary);
        }
    }

}
