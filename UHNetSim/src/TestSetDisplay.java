

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

public class TestSetDisplay extends JInternalFrame implements UnitGridListener {
    private JToolBar tlbControl         = null;
    private JButton btnSmaller          = null;
    private JButton btnLarger           = null;
    private JButton btnAdd              = null;
    private JButton btnRemove           = null;
    private NumericField numHammingUnit = null;
    private JButton btnGenerate         = null;
    private JButton btnPrevious         = null;
    private JButton btnNext             = null;
    private JButton btnCurrent          = null;
    private JTextField txfStatusBar     = null;
    private NetSimFrame parent          = null;
    private UnitGrid unitGrid           = null;
    private int currentPattern          = 0;
    //private TestSet testSet             = null;


    //-----------------------------------
    public TestSetDisplay (NetSimFrame parent) {
        super ("Test Patterns", false, false, false, true);

        this.parent = parent;
/*
        this.testSet = new TestSet();
        this.testSet.setPatternWidth(parent.getNetwork().numUnits());
        this.testSet.setPatternType(parent.getNetwork().getPatternType());
        this.testSet.addPattern();
*/
        this.initToolBar ();
        this.initStatusBar ();
        this.initTestSetDisplay ();
        this.pack ();
    }

    //-----------------------------------
    public void unitGridPixelSizeChange (UnitGridEvent e) {
        this.setSize (0, 0);
        this.pack ();
    }

    //-----------------------------------
    public void unitGridUnitChanged (UnitGridEvent e) {
        int unitIndex = (e.unitY * this.unitGrid.getUnitDimensions ().width) + e.unitX;

        // this code just changes a single unit
        this.parent.getTestSet().toggleUnit (this.currentPattern, unitIndex);

    }

    //-----------------------------------
    private void initToolBar () {
        JPanel numHolder = new JPanel (new BorderLayout ());

        this.tlbControl = new JToolBar (JToolBar.HORIZONTAL);
        this.numHammingUnit = new NumericField(2);
        numHolder.add (this.numHammingUnit, BorderLayout.CENTER);

        String resPath = System.getProperty("user.dir") + "/res/";
        try {
            this.btnSmaller = new JButton (new ImageIcon (resPath + "ZoomOut16.gif"));
            this.btnLarger = new JButton (new ImageIcon (resPath + "ZoomIn16.gif"));
            this.btnAdd = new JButton (new ImageIcon (resPath + "New16.gif"));
            this.btnRemove = new JButton (new ImageIcon (resPath + "Remove16.gif"));
            this.btnGenerate = new JButton (new ImageIcon (resPath + "Generate16.gif"));
            this.btnPrevious = new JButton (new ImageIcon (resPath + "Back16.gif"));
            this.btnNext = new JButton (new ImageIcon (resPath + "Forward16.gif"));
            this.btnCurrent = new JButton (new ImageIcon (resPath + "Import16.gif"));
        }catch(NullPointerException e){ System.err.println("Done fuqed up."); }

        this.btnSmaller.setToolTipText ("Zoom out");
        this.btnLarger.setToolTipText ("Zoom in");
        this.btnAdd.setToolTipText ("Add pattern");
        this.btnRemove.setToolTipText ("Remove pattern");
        this.numHammingUnit.setToolTipText("Enter Hamming Distance to generate test patterns");
        this.btnGenerate.setToolTipText ("Generate a set of test patterns");
        this.btnPrevious.setToolTipText ("Previous pattern");
        this.btnNext.setToolTipText ("Next pattern");
        this.btnCurrent.setToolTipText ("Make current state");

        this.btnSmaller.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                unitGrid.decreaseUnitPixelSize ();
            }
        });

        this.btnLarger.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                unitGrid.increaseUnitPixelSize ();
            }
        });

        this.btnAdd.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                addPattern();
            }
        });

        this.btnRemove.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                removePattern();
            }
        });

        this.btnPrevious.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                showPreviousPattern();
            }
        });

        this.btnNext.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                showNextPattern();
            }
        });

        this.btnGenerate.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                generateTestPatterns();
                if (parent.getTestSet().numPatterns() > 0) {
                    setCurrentPattern(parent.getTestSet().numPatterns()-1);
                }else{
                    refresh();
                }
            }
        });

        this.btnCurrent.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
            	makeCurrentState();
            }
        });

        // add the buttons to the toolbar
        this.tlbControl.add (this.btnSmaller);
        this.tlbControl.add (this.btnLarger);
        this.tlbControl.addSeparator ();
        this.tlbControl.add (this.btnAdd);
        this.tlbControl.add (this.btnRemove);
        this.tlbControl.addSeparator ();
        this.tlbControl.add (numHolder);
        this.tlbControl.add (this.btnGenerate);
        this.tlbControl.addSeparator ();
        this.tlbControl.add (this.btnPrevious);
        this.tlbControl.add (this.btnNext);
        this.tlbControl.addSeparator ();
        this.tlbControl.add (this.btnCurrent);

        // we don't want people dragging the toolbar around
        this.tlbControl.setFloatable (false);

        // add the toolbar to the frame
        this.getContentPane ().add (this.tlbControl, BorderLayout.NORTH);
    }

    private void initTestSetDisplay () {
        this.unitGrid = new UnitGrid (this.parent.getTestSet().getPatternWidth ());
        this.unitGrid.addUnitGridListener (this);
        this.getContentPane ().add (this.unitGrid, BorderLayout.CENTER);

        // get the first pattern from the test set
        this.unitGrid.setUnits (this.parent.getTestSet().getBooleanPattern (this.currentPattern));
        this.refreshStatusBar ();
        this.refreshUnitGrid ();
    }

    //-----------------------------------
    private void initStatusBar () {
        this.txfStatusBar = new JTextField ();

        // add the statusbar to the frame
        this.getContentPane ().add (this.txfStatusBar, BorderLayout.SOUTH);

        this.txfStatusBar.setEditable (false);
        this.refreshStatusBar ();
    }

    //-----------------------------------
    public void addPattern(){
        // add the new pattern to the set
        parent.getTestSet().addPattern ();
        setCurrentPattern(parent.getTestSet().numPatterns()-1);
    }

    //-----------------------------------
    public void removePattern(){
        // remove the current pattern from the set
        parent.getTestSet().removePattern (currentPattern);

        // make sure the current pattern is numbered correctly
        if (currentPattern >= parent.getTestSet().numPatterns ()) {
            currentPattern = parent.getTestSet().numPatterns () - 1;
        }
        if (parent.getTestSet().numPatterns () > 0) {
            //retrieve currentPattern
            unitGrid.setUnits (parent.getTestSet().getBooleanPattern (currentPattern));
        }else{
            //the test set is empty don't try
        }
        refresh();
    }

    //-----------------------------------
    public void showPreviousPattern(){
        if (currentPattern == 0) {
            currentPattern = parent.getTestSet().numPatterns () - 1;
        } else {
            currentPattern--;
        }
        setCurrentPattern(currentPattern);
    }

    //-----------------------------------
    public void showNextPattern(){
        if (currentPattern == parent.getTestSet().numPatterns () - 1) {
            currentPattern = 0;
        } else {
            currentPattern++;
        }
        setCurrentPattern(currentPattern);
    }

    //-----------------------------------
    public void setCurrentPattern (int patternNum) {
        if (patternNum < 0 || patternNum >= this.parent.getTestSet().numPatterns ()) {
            System.out.println ("Attempted to set current pattern to one that does not exist in TestSetDisplayFrame.setCurrentPattern ()");
            Thread.dumpStack ();
            System.exit (2);
        }
        this.currentPattern = patternNum;
        this.unitGrid.setUnits (this.parent.getTestSet().getBooleanPattern (this.currentPattern));
        this.refresh();
    }

    //-----------------------------------
    public void refresh(){
        this.refreshStatusBar ();
        this.refreshUnitGrid ();
        this.setEnabled(this.numHammingUnit.isEnabled());

        // synchronize corresponding training pattern
        if (parent.getTestSet().numPatterns() > 0) {
            int trainingPattern = parent.getTestSet().getTrainingPatternIndex(currentPattern) ;
            if ( trainingPattern >= 0 ){
                parent.getTrainingSetDisplay().setCurrentPattern(trainingPattern);
            }
        }
    }

    //-----------------------------------
    public void refreshStatusBar () {
        this.txfStatusBar.setText ((this.currentPattern + 1) + "/" + this.parent.getTestSet().numPatterns () + " patterns");

        // update hamming distance field
        if (parent.getTestSet().numPatterns() > 0) {
            int trainingPattern = parent.getTestSet().getTrainingPatternIndex(currentPattern) ;
            if ( trainingPattern >= 0 ){
                this.numHammingUnit.setText(
                    new Integer(parent.getTestSet().getHammingDistance(currentPattern)).toString());
            }else{
                this.numHammingUnit.setText("");
            }
        }
    }

    //-----------------------------------
    public void refreshUnitGrid () {
        if (this.parent.getTestSet().numPatterns () > 0 ) {
            this.unitGrid.setVisible (true);
            this.unitGrid.repaint ();
        } else {
            this.unitGrid.setVisible (false);
        }
    }

    //-----------------------------------
    public void setEnabled (boolean state) {
        int numPatterns = this.parent.getTestSet().numPatterns ();

        this.btnAdd.setEnabled (state);
        this.numHammingUnit.setEnabled(state);
        this.btnGenerate.setEnabled (state);

        this.btnSmaller.setEnabled (state && numPatterns > 0);
        this.btnLarger.setEnabled (state && numPatterns > 0);
        this.btnRemove.setEnabled (state && numPatterns > 0);
        this.btnCurrent.setEnabled (state && numPatterns > 0);
        this.btnPrevious.setEnabled (state && numPatterns > 1);
        this.btnNext.setEnabled (state && numPatterns > 1);
    }

    //-----------------------------------
    public int getCurrentPattern () {
        return this.currentPattern;
    }

    //-----------------------------------
    public void generateTestPatterns(){
        int numTrainingPatterns = parent.getTrainingSet().numPatterns();
        int numTestPatterns = this.parent.getTestSet().numPatterns();
        int numUnits = this.parent.getTestSet().getPatternWidth();
        int hu = new Integer("0" + numHammingUnit.getText()).intValue();

        if ( hu <= 0 || hu*2 > numUnits ) {
            JOptionPane.showMessageDialog(this, "Enter Hamming Distance less than or equal to half the number of units!");
            System.out.println ("Attempted to generate test patterns with invalid Hamming Distance in TestSetDisplayFrame.generateTestPatterns ()");
            hu = numUnits/2;
            this.numHammingUnit.setText( new Integer(hu).toString() );
            return;
        }
        int nextFlip = numUnits / hu;
        for(int i=0; i < numTrainingPatterns; i++){
            this.parent.getTestSet().addPattern();
            this.parent.getTestSet().setPattern(numTestPatterns+i, parent.getTrainingSet().getPattern(i));
            this.parent.getTestSet().setMetaPattern(numTestPatterns+i, i, hu);
            int k = 0;
            //System.out.print((numTestPatterns+i) + "\t");
            for (int j=nextFlip-1; j < numUnits && k < hu; j += nextFlip, k++){
                this.parent.getTestSet().toggleUnit(numTestPatterns+i, j);
                //System.out.print(j + " ");
            }
            //System.out.println("\t" + k);
        }
    }

	//-----------------------------------
	public void makeCurrentState(){
    	parent.getNetwork ().setUnits (parent.getTestSet().getPattern (currentPattern));
        parent.getNetDisplay ().setUnits (parent.getTestSet().getBooleanPattern (currentPattern));
        parent.getNetDisplay ().refreshUnitGrid ();
        try {
        	parent.getNetDisplay ().setSelected (true);
        } catch (PropertyVetoException e2) {
        	System.out.println ("PropertyVetoException in TestSetDisplayFrame.initToolBar ()");
            Thread.dumpStack ();
            System.exit (2);
        }
    }

}
