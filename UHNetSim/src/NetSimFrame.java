

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.beans.PropertyVetoException;

public class NetSimFrame extends JFrame implements ActionListener {
    private JDesktopPane dtpWorkSpace                  = null;
    private JMenu networkMenu                          = null;
    private JMenu dataSetMenu                          = null;
    private JMenu toolsMenu                            = null;

    private JTextField txfStatusBar                    = null;

    private NetWizard netWizard                        = null;
    private JFileChooser fileChooser                   = null;
    private Network network                            = null;
    private Network newNetwork                         = null;
    private NetDisplay netDisplay                      = null;
    private PrunedNetwork prunedNetwork                = null;
    private PruneNetworkFrame pruneDisplay             = null;
    private TrainingSet trainingSet                    = null;
    private TrainingSet newTrainingSet                 = null;
    private TrainingSetDisplay trainingSetDisplay      = null;
    private PatternGeneratorFrame patternGenerator     = null;
    private TrainingStats trainingStats                = null;
    private ShortTimer shortTimer                      = null;
    private TrainingNotifyFrame trainingNotify         = null;
    private TestSet testSet                            = null;
    private TestSet newTestSet                         = null;
    private TestSetDisplay testSetDisplay              = null;
    private Experiments experiments                    = null;
    private OutputFrame outputFrame                    = null;

    //-----------------------------------
    public NetSimFrame () {
        super ("UH NetSim v2.2");  // call the parent class' constructor

        this.setSize (800, 600);    // set the initial window size
        this.initWorkSpace ();
        this.initMenuBar ();
        this.initStatusBar ();
        this.initEnvironment ();
        this.initNetwork ();
        this.initPrunedNetwork ();
        this.initTrainingSet ();
        this.initTestSet();
        this.initNetWizard ();
    }

    //-----------------------------------
    public void go () {
        this.setVisible (true);
        this.netWizard.setVisible (true);
    }

    //-----------------------------------
    private void initWorkSpace () {
        this.getContentPane ().setLayout (new BorderLayout ());
        this.dtpWorkSpace = new JDesktopPane ();

        // add the panel to the frame
        this.getContentPane ().add (this.dtpWorkSpace, BorderLayout.CENTER);
    }

    //-----------------------------------
    private void initMenuBar () {
        JMenuBar menuBar = new JMenuBar ();

        this.networkMenu = new JMenu ("Network" );
        this.dataSetMenu = new JMenu ("Data sets");
        this.toolsMenu = new JMenu ("Analysis tools");
        this.networkMenu.setMnemonic (KeyEvent.VK_N);
        this.dataSetMenu.setMnemonic (KeyEvent.VK_D);
        this.toolsMenu.setMnemonic (KeyEvent.VK_A);

        JMenuItem networkNewMenuItem = new JMenuItem ("New network", KeyEvent.VK_N);
        JMenuItem networkOpenMenuItem = new JMenuItem ("Open network", KeyEvent.VK_O);
        JMenuItem networkSaveMenuItem = new JMenuItem ("Save network", KeyEvent.VK_S);
        JMenuItem networkPruneMenuItem = new JMenuItem ("Prune network", KeyEvent.VK_P);
        JMenuItem networkExitMenuItem = new JMenuItem ("Exit", KeyEvent.VK_X);

        JMenuItem trainingSetOpenMenuItem = new JMenuItem ("Open training set", KeyEvent.VK_O);
        JMenuItem trainingSetSaveMenuItem = new JMenuItem ("Save training set", KeyEvent.VK_S);
		JMenuItem testSetOpenMenuItem = new JMenuItem ("Open test set", KeyEvent.VK_P);
        JMenuItem testSetSaveMenuItem = new JMenuItem ("Save test set", KeyEvent.VK_V);

        //JMenuItem toolsKanterMenuItem = new JMenuItem ("Kanter & Sompolinsky", KeyEvent.VK_K);
        //JMenuItem toolsGammaMenuItem = new JMenuItem ("Gamma distribution", KeyEvent.VK_G);
        JMenuItem toolsStatesDiagramMenuItem = new JMenuItem ("State diagram", KeyEvent.VK_S);
        JMenuItem toolsWeightMetrixMenuItem = new JMenuItem ("Weight matrix", KeyEvent.VK_M);
        JMenuItem toolsConnections3DMenuItem = new JMenuItem ("Connections 3D", KeyEvent.VK_C);
        JMenuItem toolsPruningTestMenuItem = new JMenuItem ("Pruning test (HD-classified)", KeyEvent.VK_T);
        JMenuItem toolsOverloadingDynamicMenuItem = new JMenuItem ("Loading analysis (dynamic)", KeyEvent.VK_L);
        JMenuItem toolsPruningDynamicMenuItem = new JMenuItem ("Pruning analysis (dynamic)", KeyEvent.VK_P);
        JMenuItem toolsOutputWindowMenuItem = new JMenuItem ("Output window", KeyEvent.VK_W);

        networkMenu.add (networkNewMenuItem);
        networkMenu.add (networkOpenMenuItem);
        networkMenu.add (networkSaveMenuItem);
        networkMenu.addSeparator ();
        //networkMenu.add (networkPruneMenuItem);
        //networkMenu.addSeparator ();
        networkMenu.add (networkExitMenuItem);

        dataSetMenu.add (trainingSetOpenMenuItem);
        dataSetMenu.add (trainingSetSaveMenuItem);
        dataSetMenu.addSeparator ();
        dataSetMenu.add (testSetOpenMenuItem);
        dataSetMenu.add (testSetSaveMenuItem);

        //toolsMenu.add (toolsKanterMenuItem);
        //toolsMenu.add (toolsGammaMenuItem);
        //toolsMenu.addSeparator();
        toolsMenu.add (toolsStatesDiagramMenuItem);
		toolsMenu.add (toolsWeightMetrixMenuItem);
		toolsMenu.add (toolsConnections3DMenuItem);
        toolsMenu.addSeparator();
        //toolsMenu.add (toolsPruningTestMenuItem);
        //toolsMenu.add (toolsOverloadingDynamicMenuItem);
        //toolsMenu.add (toolsPruningDynamicMenuItem);
        //toolsMenu.addSeparator();
        toolsMenu.add (toolsOutputWindowMenuItem);

        menuBar.add (networkMenu);
        menuBar.add (dataSetMenu);
        menuBar.add (toolsMenu);
        this.setMenusEnabled(false);
        this.setJMenuBar (menuBar);


        // create the listeners for the menu items
		networkNewMenuItem.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                // create a new network and training set
                initNetwork ();
                initPrunedNetwork ();
                initTrainingSet ();
                initTestSet();
                setMenusEnabled(false);
                netDisplay.setVisible (false);
                trainingSetDisplay.setVisible (false);
                testSetDisplay.setVisible(false);
                initNetWizard ();
                go ();
			}
		});

        // create the listeners for the menu items
		networkOpenMenuItem.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                netDisplay.setVisible (false);
                trainingSetDisplay.setVisible (false);
                testSetDisplay.setVisible(false);

                newNetwork = loadNetwork ();
                if (newNetwork != null) {
                    newTrainingSet = loadTrainingSet (newNetwork.getTrainingSetFile ());
                    if (newTrainingSet != null) {
                        network = newNetwork;
                        trainingSet = newTrainingSet;

                        newTestSet = loadTestSet (null);
                        if (newTestSet != null) {
                            testSet = newTestSet;
                        }

                        // set the pattern type of the network according to that of the training set
                        network.setPatternType (trainingSet.getPatternType ());
                        network.clearCurrentState ();

                        initNetDisplay ();
                        initTrainingSetDisplay ();
                        initTestSetDisplay();

                        prunedNetwork.rebuild();
                        initPruneDisplay();
                    }
                }
                netDisplay.setVisible (true);
                trainingSetDisplay.setVisible (true);
                testSetDisplay.setVisible(true);
			}
		});

		networkSaveMenuItem.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                netDisplay.setVisible (false);
                trainingSetDisplay.setVisible (false);
                testSetDisplay.setVisible (false);

                saveNetwork ();

                netDisplay.setVisible (true);
                trainingSetDisplay.setVisible (true);
                testSetDisplay.setVisible(true);
            }
        });

        trainingSetOpenMenuItem.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                netDisplay.setVisible (false);
                trainingSetDisplay.setVisible (false);
                testSetDisplay.setVisible(false);

                newTrainingSet = loadTrainingSet (null);
                if (newTrainingSet != null) {
                    trainingSet = newTrainingSet;
                    trainingSetDisplay.setCurrentPattern (0);
                }

                netDisplay.setVisible (true);
                trainingSetDisplay.setVisible (true);
                testSetDisplay.setVisible(true);
            }
        });

		trainingSetSaveMenuItem.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                netDisplay.setVisible (false);
                trainingSetDisplay.setVisible (false);
                testSetDisplay.setVisible(false);

                saveTrainingSet ();

                netDisplay.setVisible (true);
                trainingSetDisplay.setVisible (true);
                testSetDisplay.setVisible(true);
            }
        });

        testSetOpenMenuItem.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                netDisplay.setVisible (false);
                trainingSetDisplay.setVisible (false);
                testSetDisplay.setVisible(false);

                newTestSet = loadTestSet (null);
                if (newTestSet != null) {
                    testSet = newTestSet;
                    testSetDisplay.setCurrentPattern (0);
                }

                netDisplay.setVisible (true);
                trainingSetDisplay.setVisible (true);
                testSetDisplay.setVisible(true);
            }
        });

		testSetSaveMenuItem.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
                netDisplay.setVisible (false);
                trainingSetDisplay.setVisible (false);
                testSetDisplay.setVisible(false) ;

                saveTestSet ();

                netDisplay.setVisible (true);
                trainingSetDisplay.setVisible (true);
                testSetDisplay.setVisible(true);
            }
        });

        networkPruneMenuItem.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                pruneDisplay.display();
            }
        });

        networkExitMenuItem.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                System.exit (0);
            }
        });

        this.addWindowListener (new ExitListener ());

        toolsStatesDiagramMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e) {
                netDisplay.displayDynamic();
            }
        });

        toolsWeightMetrixMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e) {
                displayWeightMetrix();
            }
        });

        toolsConnections3DMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e) {
                displayConnections();
            }
        });

        toolsPruningTestMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e) {
                experiments.pruningTest();
            }
        });

        toolsOverloadingDynamicMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e) {
                experiments.overloadingAnalysis();
            }
        });

        toolsPruningDynamicMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e) {
                experiments.pruningAnalysis();
            }
        });

        toolsOutputWindowMenuItem.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e) {
                outputFrame.setVisible (true);
                try{
                    outputFrame.setSelected(true);
                }catch(PropertyVetoException ve){
                }
            }
        });
    }

    //-----------------------------------
    private void initStatusBar () {
        this.txfStatusBar = new JTextField ();

        // add the statusbar to the frame
        this.getContentPane ().add (this.txfStatusBar, BorderLayout.SOUTH);

        this.txfStatusBar.setEditable (false);
        this.txfStatusBar.setText ("Welcome to the world of Artificial Neural Networks.");
    }


    //-----------------------------------
    private void initEnvironment () {
        this.fileChooser   = new JFileChooser ();
        this.trainingStats = new TrainingStats ();
        this.shortTimer    = new ShortTimer ();
        this.experiments   = new Experiments(this);
        this.outputFrame   = new OutputFrame(this);
    }

    //-----------------------------------
    private void initNetwork () {
        this.network = new Network (this);
    }

    //-----------------------------------
    private void initPrunedNetwork () {
        this.prunedNetwork = new PrunedNetwork (this);
    }

    //-----------------------------------
    private void initTrainingSet () {
        this.trainingSet = new TrainingSet ();
    }

    //-----------------------------------
    private void initTestSet () {
        this.testSet = new TestSet ();
    }

    //-----------------------------------
    private void initNetWizard () {
        this.netWizard = new NetWizard (this);
        this.dtpWorkSpace.add (this.netWizard);
        this.netWizard.getNavPanel ().addActionListener (this);
        this.netWizard.setLocation (150, 150);
        this.outputFrame.setVisible(false);
    }

    //-----------------------------------
    private void initNetDisplay () {
        this.netDisplay = new NetDisplay (this);

        this.dtpWorkSpace.add (this.netDisplay);
        this.netDisplay.setLocation( 0, 0);
    }

    //-----------------------------------
    private void initTrainingSetDisplay () {
        this.trainingSetDisplay = new TrainingSetDisplay (this);

        this.dtpWorkSpace.add (this.trainingSetDisplay);
        this.trainingSetDisplay.setLocation (0, 230);
    }

    //-----------------------------------
    private void initPruneDisplay () {
        this.pruneDisplay = new PruneNetworkFrame (this);

        this.dtpWorkSpace.add (this.pruneDisplay);
        this.pruneDisplay.setLocation (50, 50);
    }

    //-----------------------------------
    private void initTestSetDisplay(){
        this.testSetDisplay  = new TestSetDisplay (this);

        this.dtpWorkSpace.add (this.testSetDisplay);
        this.testSetDisplay.setLocation(485, 230);
    }

    //-----------------------------------
    private void initTrainingNotify () {
        this.trainingNotify = new TrainingNotifyFrame (this);

        this.dtpWorkSpace.add (this.trainingNotify);
        this.trainingNotify.setLocation(50, 30);
    }

    //-----------------------------------
    private void initPatternGenerator () {
        this.patternGenerator = new PatternGeneratorFrame (this);

        this.dtpWorkSpace.add (this.patternGenerator);
        this.patternGenerator.setLocation(50, 30);
    }

    //-----------------------------------
    public void actionPerformed (ActionEvent e) {
        if (this.netWizard.getNavPanel ().getAction () == NavPanel.FINISH) {
            if (this.netWizard.finishWizard () == false) {
                this.netWizard.updateCards ();
            } else {
                this.initNetDisplay ();
                this.initTrainingSetDisplay ();
                this.initTestSetDisplay ();

                this.prunedNetwork.rebuild();
                this.initPruneDisplay ();

                this.initPatternGenerator ();
                this.initTrainingNotify ();

                this.netWizard.setVisible (false);
                setMenusEnabled (true);
                this.netDisplay.setVisible (true);
                this.trainingSetDisplay.setVisible (true);
                this.testSetDisplay.setVisible(true);
            }
        }
    }

    //-----------------------------------
    public Network getNetwork () {
        return this.network;
    }

    //-----------------------------------
    public NetDisplay getNetDisplay () {
        return this.netDisplay;
    }

    //-----------------------------------
    public TrainingSet getTrainingSet () {
        return this.trainingSet;
    }

    //-----------------------------------
    public TrainingSetDisplay getTrainingSetDisplay () {
        return this.trainingSetDisplay;
    }

    //-----------------------------------
    public TestSet getTestSet () {
        return this.testSet;
    }

    //-----------------------------------
    public TestSetDisplay getTestSetDisplay () {
        return this.testSetDisplay;
    }

   //-----------------------------------
    public PrunedNetwork getPrunedNetwork () {
        return this.prunedNetwork;
    }

   //-----------------------------------
    public PruneNetworkFrame getPruneDisplay () {
        return this.pruneDisplay;
    }

    //-----------------------------------
    public PatternGeneratorFrame getPatternGenerator () {
        return this.patternGenerator;
    }

    //-----------------------------------
    public TrainingStats getTrainingStats () {
        return this.trainingStats;
    }

    //-----------------------------------
    public TrainingNotifyFrame getTrainingNotify () {
        return this.trainingNotify;
    }

    //-----------------------------------
    public ShortTimer getShortTimer () {
        return this.shortTimer;
    }

    //-----------------------------------
    public OutputFrame getOutputFrame(){
        return this.outputFrame;
    }

    //-----------------------------------
    public void addChildFrame(JInternalFrame child){
        this.dtpWorkSpace.add(child);
    }

    //-----------------------------------
    public void setMenusEnabled(boolean enabled){
        this.networkMenu.setEnabled(enabled);
        this.dataSetMenu.setEnabled(enabled);
        this.toolsMenu.setEnabled(enabled);
    }

    //-----------------------------------
    public void displayWeightMetrix(){
        WeightsViewer w = new WeightsViewer(this);
        w.setVisible(true);
    }

    //-----------------------------------
    public void displayConnections(){
        Connections3DViewer v = new Connections3DViewer(this);
        v.setVisible(true);
    }

    //-----------------------------------
    private Network loadNetwork () {
        Network network = new Network (this);
        this.fileChooser.setDialogTitle ("Open network");
        if (this.fileChooser.showOpenDialog ( null) == JFileChooser.APPROVE_OPTION) {
            switch (network.loadNetwork (this.fileChooser.getSelectedFile ())) {
                case Network.FILE_NOT_FOUND:
                    JOptionPane.showMessageDialog (this, "Network file not found", "Loading network", JOptionPane.ERROR_MESSAGE);
                    break;
                case Network.NO_TRAINING_SET:
                    JOptionPane.showMessageDialog (this, "Training set not specified in file", "Loading network", JOptionPane.ERROR_MESSAGE);
                    break;
                case Network.NO_TRAINING_RULE:
                    JOptionPane.showMessageDialog (this, "Training rule not specified in file", "Loading network", JOptionPane.ERROR_MESSAGE);
                    break;
                case Network.INVALID_TRAINING_RULE:
                    JOptionPane.showMessageDialog (this, "Invalid training rule in file", "Loading network", JOptionPane.ERROR_MESSAGE);
                    break;
                case Network.NO_SIZE:
                    JOptionPane.showMessageDialog (this, "Network size not specified in file", "Loading network", JOptionPane.ERROR_MESSAGE);
                    break;
                case Network.INVALID_SIZE:
                    JOptionPane.showMessageDialog (this, "Specified file size was invalid", "Loading network", JOptionPane.ERROR_MESSAGE);
                    break;
                case Network.INVALID_WEIGHT:
                    JOptionPane.showMessageDialog (this, "Weights contain invalid value", "Loading network", JOptionPane.ERROR_MESSAGE);
                    break;
                case Network.PREMATURE_END:
                    JOptionPane.showMessageDialog (this, "Not enough weights in file", "Loading network", JOptionPane.ERROR_MESSAGE);
                    break;
                case Network.IO_EXCEPTION:
                    JOptionPane.showMessageDialog (this, "I/O exception occurred, reason unknown", "Loading network", JOptionPane.ERROR_MESSAGE);
                    break;
                default:
                    return network;
            }
        }
        return null;
    }

    //-----------------------------------
    private void saveNetwork () {
        File networkFile     = null;
        File trainingSetFile = null;

        this.fileChooser.setDialogTitle ("Save network");
        if (this.fileChooser.showSaveDialog (null) != JFileChooser.APPROVE_OPTION) return;
        networkFile = this.fileChooser.getSelectedFile ();

        if (this.network.getTrainingSetFile () == null) {
            this.fileChooser.setDialogTitle ("Save training set");
            if (this.fileChooser.showSaveDialog (null) != JFileChooser.APPROVE_OPTION) return;
            trainingSetFile = this.fileChooser.getSelectedFile ();
            this.network.setTrainingSetFile (trainingSetFile);
        }

        this.network.saveNetwork (networkFile);
        this.trainingSet.saveTrainingSet (this.network.getTrainingSetFile ());
    }

    //-----------------------------------
    private TrainingSet loadTrainingSet (File trainingSetFile) {
        TrainingSet trainingSet = new TrainingSet ();

        if (trainingSetFile == null) {
            this.fileChooser.setDialogTitle ("Open training set");
            if (fileChooser.showOpenDialog (null) == JFileChooser.APPROVE_OPTION) {
                trainingSetFile = fileChooser.getSelectedFile ();
            } else return null;
        }

        switch (trainingSet.loadTrainingSet (trainingSetFile)) {
            case TrainingSet.FILE_NOT_FOUND:
                JOptionPane.showMessageDialog (this, "Training set file not found", "Loading training set", JOptionPane.ERROR_MESSAGE);
                break;
            case TrainingSet.NO_PATTERN_SIZE:
                JOptionPane.showMessageDialog (this, "Training pattern size not found in file", "Loading training set", JOptionPane.ERROR_MESSAGE);
                break;
            case TrainingSet.NO_BIT_FOUND:
                JOptionPane.showMessageDialog (this, "Training pattern bit not found", "Loading training set", JOptionPane.ERROR_MESSAGE);
                break;
            case TrainingSet.INVALID_BIT_VALUE:
                JOptionPane.showMessageDialog (this, "Training pattern bit has incorrect value", "Loading training set", JOptionPane.ERROR_MESSAGE);
                break;
            case TrainingSet.PREMATURE_END:
                JOptionPane.showMessageDialog (this, "Pattern/s incorrect length", "Loading training set", JOptionPane.ERROR_MESSAGE);
                break;
            case TrainingSet.IO_EXCEPTION:
                JOptionPane.showMessageDialog (this, "I/O exception occurred, reason unknown", "Loading training set", JOptionPane.ERROR_MESSAGE);
                break;
            default:
                // check the pattern type
                if (this.newNetwork != null) {
                    // check the width of the patterns
                    if (trainingSet.getPatternWidth () != this.newNetwork.numUnits ()) {
                        JOptionPane.showMessageDialog (this, "Training set patterns and network are different sizes", "Loading training set", JOptionPane.ERROR_MESSAGE);
                        return null;
                    }
                } else {
                    if ((trainingSet.getPatternType () != this.network.getPatternType ()) && (this.network.getPatternType () != Network.NOT_KNOWN)) {
                        JOptionPane.showMessageDialog (this, "Specified training set contains patterns of the wrong type for this network", "Loading training set", JOptionPane.ERROR_MESSAGE);
                        return null;
                    }
                    // check the width of the patterns
                    if (trainingSet.getPatternWidth () != this.network.numUnits ()) {
                        JOptionPane.showMessageDialog (this, "Training set patterns and network are different sizes", "Loading training set", JOptionPane.ERROR_MESSAGE);
                        return null;
                    }
                }
                return trainingSet;
        }
        return null;
    }

    //-----------------------------------
    private void saveTrainingSet () {
        File trainingSetFile = null;

        this.fileChooser.setDialogTitle ("Save training set");
        if (this.fileChooser.showSaveDialog (null) != JFileChooser.APPROVE_OPTION) return;
        trainingSetFile = this.fileChooser.getSelectedFile ();

        this.trainingSet.saveTrainingSet (trainingSetFile);
    }

    //-----------------------------------
    private TestSet loadTestSet (File testSetFile) {
        TestSet testSet = new TestSet ();

        if (testSetFile == null) {
            this.fileChooser.setDialogTitle ("Open test set");
            if (fileChooser.showOpenDialog (null) == JFileChooser.APPROVE_OPTION) {
                testSetFile = fileChooser.getSelectedFile ();
            } else return null;
        }

        switch (testSet.loadTestSet (testSetFile)) {
            case TestSet.FILE_NOT_FOUND:
                JOptionPane.showMessageDialog (this, "Test set file not found", "Loading test set", JOptionPane.ERROR_MESSAGE);
                break;
            case TestSet.NO_PATTERN_SIZE:
                JOptionPane.showMessageDialog (this, "Test pattern size not found in file", "Loading test set", JOptionPane.ERROR_MESSAGE);
                break;
            case TestSet.NO_BIT_FOUND:
                JOptionPane.showMessageDialog (this, "Test pattern bit not found", "Loading test set", JOptionPane.ERROR_MESSAGE);
                break;
            case TestSet.INVALID_BIT_VALUE:
                JOptionPane.showMessageDialog (this, "Test pattern bit has incorrect value", "Loading test set", JOptionPane.ERROR_MESSAGE);
                break;
            case TestSet.PREMATURE_END:
                JOptionPane.showMessageDialog (this, "Pattern/s incorrect length", "Loading test set", JOptionPane.ERROR_MESSAGE);
                break;
            case TestSet.IO_EXCEPTION:
                JOptionPane.showMessageDialog (this, "I/O exception occurred, reason unknown", "Loading test set", JOptionPane.ERROR_MESSAGE);
                break;
            default:
                // check the pattern type
                if (this.newNetwork != null) {
                    // check the width of the patterns
                    if (testSet.getPatternWidth () != this.newNetwork.numUnits ()) {
                        JOptionPane.showMessageDialog (this, "Test set patterns and network are different sizes", "Loading test set", JOptionPane.ERROR_MESSAGE);
                        return null;
                    }
                } else {
                    if ((testSet.getPatternType () != this.network.getPatternType ()) && (this.network.getPatternType () != Network.NOT_KNOWN)) {
                        JOptionPane.showMessageDialog (this, "Specified test set contains patterns of the wrong type for this network", "Loading test set", JOptionPane.ERROR_MESSAGE);
                        return null;
                    }
                    // check the width of the patterns
                    if (testSet.getPatternWidth () != this.network.numUnits ()) {
                        JOptionPane.showMessageDialog (this, "Test set patterns and network are different sizes", "Loading test set", JOptionPane.ERROR_MESSAGE);
                        return null;
                    }
                }
                return testSet;
        }
        return null;
    }

    //-----------------------------------
    private void saveTestSet () {
        File testSetFile = null;

        this.fileChooser.setDialogTitle ("Save test set");
        if (this.fileChooser.showSaveDialog (null) != JFileChooser.APPROVE_OPTION) return;
        testSetFile = this.fileChooser.getSelectedFile ();

        this.testSet.saveTestSet (testSetFile);
    }

}
