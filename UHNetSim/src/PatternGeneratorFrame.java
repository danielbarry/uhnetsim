

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

public class PatternGeneratorFrame extends JInternalFrame implements Runnable {
    private NetSimFrame parent        = null;
    private Thread thread             = null;
    private Random numberGenerator    = null;
    private ArrayList newPatterns     = null;
    private boolean abortGeneration   = false;

    // GUI elements
    private NumericField numPatterns  = null;
    private FloatField patternBias    = null;
    private JCheckBox allowDuplicates = null;
    private JProgressBar progressBar  = null;
    private JButton btnOK             = null;
    private JButton btnCancel         = null;

    //-----------------------------------
    public PatternGeneratorFrame (NetSimFrame parent) {
        super ("Pattern generator", false, false, false, true);

        this.parent = parent;
        this.numberGenerator = new Random ();
        this.newPatterns = new ArrayList ();
        this.initPatternGenerator ();
        this.pack ();
    }

    //-----------------------------------
    private void initPatternGenerator () {
        GridBagLayout gridBagLayout = new GridBagLayout ();
        GridBagConstraints gridBagConstraints = new GridBagConstraints ();

        this.setContentPane (new JPanel ());
        this.getContentPane ().setLayout (gridBagLayout);
        this.numPatterns = new NumericField (6);
        this.patternBias = new FloatField (6, false);
        this.allowDuplicates = new JCheckBox ();
        this.progressBar = new JProgressBar ();
        this.btnOK = new JButton ("OK");
        this.btnCancel = new JButton ("Cancel");
        this.btnOK.setPreferredSize (this.btnCancel.getPreferredSize ());

    	btnOK.addActionListener (new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
                int numPatts   = 0;
                float pattBias = 0;

                // make sure the fields are all completed
                if (numPatterns.getText ().compareTo ("") == 0) {
                    JOptionPane.showMessageDialog (null, "You must select a number of patterns to generate");
                } else if (patternBias.getText ().compareTo ("") == 0) {
                    JOptionPane.showMessageDialog (null, "You must select a pattern bias");
                } else {
                    try {
                        numPatts = Integer.parseInt (numPatterns.getText ());
                        pattBias = Float.parseFloat (patternBias.getText ());
                    } catch (NumberFormatException e2) {
                        System.out.println ("Invalid number format exception in PatternGeneratorFrame.initPatternGenerator ()");
                        Thread.dumpStack ();
                        System.exit (2);
                    }
                    if (numPatts == 0) {
                        JOptionPane.showMessageDialog (null, "Number of patterns must be greater than 0");
                    } else if (pattBias > 1) {
                        JOptionPane.showMessageDialog (null, "Patterns bias must between 0 and 1");
                    } else {
                        thread = new Thread (parent.getPatternGenerator ());
                        thread.setPriority (Thread.MIN_PRIORITY);
                        thread.start ();
                        btnCancel.setText ("Abort");
                    }
                }
            }
	    });

    	btnCancel.addActionListener (new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
                if (btnCancel.getText ().compareTo ("Abort") == 0) {
                    abortGeneration = true;
                }
                parent.setMenusEnabled (true);
                parent.getNetDisplay ().setEnabled (true);
                parent.getTrainingSetDisplay ().setEnabled (true);
                parent.getTestSetDisplay().setEnabled(true);
                setVisible (false);
            }
	    });

        gridBagConstraints.insets = new Insets (20, 30, 0, 30);
        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        this.getContentPane ().add (new JLabel ("Number of patterns                "), gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        this.getContentPane ().add (this.numPatterns, gridBagConstraints);

        gridBagConstraints.insets = new Insets (5, 30, 0, 30);
        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        this.getContentPane ().add (new JLabel ("Pattern bias                      "), gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        this.getContentPane ().add (this.patternBias, gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        this.getContentPane ().add (new JLabel ("Allow duplicates                  "), gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        this.getContentPane ().add (this.allowDuplicates, gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        this.getContentPane ().add (new JLabel ("Progress                          "), gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        this.getContentPane ().add (this.progressBar, gridBagConstraints);

        gridBagConstraints.insets = new Insets (20, 0, 20, 0);
        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        gridBagConstraints.anchor = GridBagConstraints.EAST;
        this.getContentPane ().add (this.btnOK, gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        this.getContentPane ().add (this.btnCancel, gridBagConstraints);

        this.setVisible (false);
    }

    //-----------------------------------
    public void run () {
        int patternsRequired  = 0;
        float bias            = 0;

        this.numPatterns.setEnabled (false);
        this.patternBias.setEnabled (false);
        this.allowDuplicates.setEnabled (false);
        this.btnOK.setEnabled (false);

        try {
            patternsRequired = Integer.parseInt (this.numPatterns.getText ());
            bias = Float.parseFloat (this.patternBias.getText ());
        } catch (NumberFormatException e) {
            System.out.println ("Number format exception in PatternGeneratorFrame.run ()");
            Thread.dumpStack ();
            System.exit (2);
        }
        generatePattern( patternsRequired, bias, this.allowDuplicates.isSelected () );

        this.parent.setMenusEnabled (true);
        this.numPatterns.setEnabled (true);
        this.patternBias.setEnabled (true);
        this.allowDuplicates.setEnabled (true);
        this.btnOK.setEnabled (true);
        this.btnCancel.setEnabled (true);
        this.parent.getNetDisplay ().setEnabled (true);
        this.parent.getTrainingSetDisplay ().setEnabled (true);
        this.parent.getTestSetDisplay() .setEnabled (true);
        try {
            this.parent.getTrainingSetDisplay ().setSelected (true);
        } catch (PropertyVetoException e) {
            // do nothing, not critical failure
        }
        this.setVisible (false);
        this.btnCancel.setText ("Cancel");
    }

    //-----------------------------------
    public void generatePattern (int patternsRequired, float bias, boolean isAllowDuplicate) {
        int patternCount      = 0;
        int patternIndex      = 0;
        int unitIndex         = 0;
        int[] tempPattern     = null;
        boolean rejectPattern = false;

        this.progressBar.setMinimum (1);
        this.progressBar.setMaximum (patternsRequired);
        this.newPatterns.clear ();
        this.abortGeneration = false;

        while ((patternCount < patternsRequired) && (this.abortGeneration == false)) {
            rejectPattern = false;

            // generate a pattern
            tempPattern = new int[this.parent.getTrainingSet ().getPatternWidth ()];
            for (unitIndex=0;unitIndex<tempPattern.length;unitIndex++) {
                if (this.numberGenerator.nextFloat () > bias) {
                    tempPattern[unitIndex] = this.parent.getNetwork ().lowValue ();
                } else {
                    tempPattern[unitIndex] = this.parent.getNetwork ().highValue ();
                }
            }

            if (isAllowDuplicate == false) {
                // compare the pattern with the existing training set
                if (this.parent.getTrainingSet ().numPatterns () != 0) {
                    for (patternIndex=0;patternIndex<this.parent.getTrainingSet ().numPatterns ();patternIndex++) {
                        if (Arrays.equals (tempPattern, this.parent.getTrainingSet ().getPattern (patternIndex)) == true) rejectPattern = true;
                    }
                }

                // compare the pattern with the newly generated set
                if (this.newPatterns.size () != 0) {
                    for (patternIndex=0;patternIndex<this.newPatterns.size ();patternIndex++) {
                        if (Arrays.equals (tempPattern, (int[])this.newPatterns.get (patternIndex)) == true) {
                            rejectPattern = true;
                        }
                    }
                }
            }

            if (rejectPattern == false) {
                this.newPatterns.add (tempPattern);
                patternCount++;
                // move the progress bar
                this.progressBar.setValue (patternCount);
            }
        }

        if (this.abortGeneration == false) {
            for (patternIndex=0;patternIndex<patternsRequired;patternIndex++) {
                this.parent.getTrainingSet ().addPattern ();
                this.parent.getTrainingSet ().setPattern (this.parent.getTrainingSet ().numPatterns () - 1, (int[])this.newPatterns.get (patternIndex));
            }
            this.parent.getTrainingSetDisplay ().refreshStatusBar ();
            this.parent.getTrainingSetDisplay ().refreshUnitGrid ();

            // show the first pattern we generated if there were previously no patterns in the training set
            if (this.parent.getTrainingSetDisplay ().getCurrentPattern () < 0) this.parent.getTrainingSetDisplay ().setCurrentPattern (0);
        }

    }

    //-----------------------------------
    public void display () {
        this.parent.setMenusEnabled (false);
        this.parent.getNetDisplay ().setEnabled (false);
        this.parent.getTrainingSetDisplay ().setEnabled (false);
        this.parent.getTestSetDisplay().setEnabled(false);
        this.numPatterns.setText ("");
        this.patternBias.setText ("");
        this.allowDuplicates.setSelected (false);
        this.progressBar.setValue (1);
        this.setVisible (true);
    }
}