

import javax.swing.*;
import javax.swing.text.*;
 
public class NumericField extends JTextField {
    //-----------------------------------
    public NumericField (int numCols) {
        super (numCols);
    }
 
     //-----------------------------------
     protected Document createDefaultModel () {
 	      return new NumericDocument ();
     }
 
     //-----------------------------------
     static class NumericDocument extends PlainDocument {
         //-----------------------------------
        public void insertString (int offset, String string, AttributeSet attributeSet) throws BadLocationException {
            char numericString[] = null;
            int charIndex        = 0;

            if (string == null) return;
            numericString = string.toCharArray ();
            
            for (charIndex=0;charIndex<numericString.length;charIndex++) {
                if (Character.isDigit (numericString[charIndex]) == false) return;
            }
 	        super.insertString (offset, new String (numericString), attributeSet);
        }
     }
}