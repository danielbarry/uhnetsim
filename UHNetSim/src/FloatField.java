

import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
 
public class FloatField extends JTextField implements DocumentListener, FocusListener {
    private static FloatDocument defaultModel = null;
    private boolean allowNegative             = false;

    //-----------------------------------
    public FloatField (int numCols, boolean allowNegative) {
        super (numCols);
        this.allowNegative = allowNegative;
        this.addFocusListener (this);
    }

    //-----------------------------------
    protected Document createDefaultModel () {
        this.defaultModel = new FloatDocument ();
        this.defaultModel.addDocumentListener (this);
        this.defaultModel.setAllowNegative (this.allowNegative);

        return this.defaultModel;
    }

    //-----------------------------------
    public void focusLost (FocusEvent e) {
        int charIndex        = 0;
        char[] currentString = null;
        
        if (this.defaultModel.getLength () > 0) {
            // get the array of character that represents the current string
            try {
                currentString = this.defaultModel.getText (0, this.defaultModel.getLength ()).toCharArray ();

                if (currentString[0] == '.') {
                    this.defaultModel.insertString (0, "0", this.defaultModel.getAC ().getEmptySet ());
                } else if ((currentString[0] == '-') && (currentString[1] == '.')) {
                    this.defaultModel.insertString (1, "0", this.defaultModel.getAC ().getEmptySet ());
                } else if (currentString[currentString.length - 1] == '.') {
                    this.defaultModel.insertString (this.defaultModel.getLength (), "0", this.defaultModel.getAC ().getEmptySet ());
                }
            } catch (BadLocationException e2) {
                // TODO: catch this error
            }
        }
    }

    //-----------------------------------
    public void removeUpdate (DocumentEvent e) {
        int charIndex        = 0;
        char currentString[] = null;

        this.defaultModel.setHasPoint (false);

        if (this.defaultModel.getLength () > 0) {
            try {
                // get the array of character that represents the current string
                currentString = this.defaultModel.getText (0, this.defaultModel.getLength ()).toCharArray ();
                // sort the characters in the array
                Arrays.sort (currentString);
                // see if there is a decimal point in it
                //if (Arrays.binarySearch (currentString, '.') >= 0) this.defaultModel.setHasPoint (true);
                this.defaultModel.setHasPoint (Arrays.binarySearch (currentString, '.') >= 0);
            } catch (BadLocationException e2) {
                // TODO: catch this error
            }
        }
    }
   
    //-----------------------------------
    static class FloatDocument extends PlainDocument {
        private boolean hasDecimalPoint = false;
        private boolean allowNegative = false;

        //-----------------------------------
        public AbstractDocument.AttributeContext getAC () {
            return super.getAttributeContext ();
        }

        //-----------------------------------
        public void setHasPoint (boolean state) {
            this.hasDecimalPoint = state;
        }

        //-----------------------------------
        public void setAllowNegative (boolean state) {
            this.allowNegative = state;
        }

        //-----------------------------------
        public void insertString (int offset, String string, AttributeSet attributeSet) throws BadLocationException {
            char numericString[] = null;
            int charIndex        = 0;

            if (string == null) return;
            numericString = string.toCharArray ();

            for (charIndex=0;charIndex<numericString.length;charIndex++) {
                if (Character.isDigit (numericString[charIndex]) == false) {
                    if ((numericString[charIndex] == '-') && (offset == 0) && (this.allowNegative == true)) { 
                        super.insertString (offset, new String (numericString), attributeSet);
                        return;
                    } else if ((numericString[charIndex] == '.') && (this.hasDecimalPoint == false)) {
                        this.hasDecimalPoint = true;
                        super.insertString (offset, new String (numericString), attributeSet);
                        return;
                    } else {
                        return;
                    }
                }
                super.insertString (offset, new String (numericString), attributeSet);
            } 	       
        }
     }

    // unused events
    public void focusGained (FocusEvent e) {}
    public void changedUpdate(DocumentEvent e) {}
    public void insertUpdate (DocumentEvent e) {}
}