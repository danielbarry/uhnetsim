import Utils.Assertion;
import Utils.Print;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JComboBox;

@SuppressWarnings("unqualified-field-access")
public class NetSimIO{
    private TrainingSet blank;
    private PrintWriter out = null;
    
    public NetSimIO(NetSimFrame netFrame, NetSim.NetworkType type, String output, int size, int runs, int pattern, int numPatterns, float bias, boolean duplicates){
        //netFrame.setVisible(true);
        
        Print.debug("Recieved all data correctly");
        
        Print.debug("Checking constructor input");
        Assertion.test(netFrame!=null);
        Assertion.test(type!=null);
        Assertion.test(size>0);
        Assertion.test(runs>=0);
        
        try{ if(output!=null)out = new PrintWriter(new BufferedWriter(new FileWriter(output, true))); }
        catch(IOException e){ Print.err("Output Network file failed to load"); }
        if(out!=null)Print.debug("Output file doesn't exist");
        
        Print.debug("Initialise Net Wizard");
            initNetWizard(netFrame, size, pattern, type);

        long total = 0;
        
        for(int x=0; x<runs; x++){
            int p = 0;
            int trained = 0 ;
            while(trained==0&&p<numPatterns){
                Print.debug("Generate Random Patterns");
                initPatterns(netFrame, p, bias, duplicates);
                
                generatePatterns(netFrame, ++p, bias, duplicates);
                trained = trainPatterns(netFrame);
            }
            Print.debug("p>>" + p);
            total += p;
        }
        
        Print.debug("Output Data"); 
        String outString = type.getType() + ',' + size + ',' + runs + ',' + pattern + ',' + numPatterns + ',' + bias + ',' + duplicates + ',' + (total/runs);
        if(out!=null)output(outString);
        else Print.debug(outString);
        
        if(out!=null)out.close(); //Close file if open
        Print.debug("--Finished simulations--");
        System.exit(0); //Close to allow other programs
    }
    
    private void generatePatterns(NetSimFrame netFrame, int numPatterns, float bias, boolean duplicates){
        //Auto pattern generator
        PatternGeneratorFrame patternGen = netFrame.getPatternGenerator();
        
        //Refill blank set
        TrainingSet trainSet = (TrainingSet)(accessPrivateField(netFrame, "trainingSet"));
        trainSet = blank;
        
        //Remove old pattterns
        while(trainSet.numPatterns()>0){
            trainSet.removePattern(0); //Remove old patterns
        }
        
        //Set params for patterns
        Print.debug("Generating Patterns");
        patternGen.generatePattern(numPatterns, bias, duplicates); //Number of patterns, Pattern bias, Allow Duplicates
        Print.debug("Patterns Generated");
    }
    
    private int trainPatterns(NetSimFrame netFrame){
        //Auto train network
        Network network = netFrame.getNetwork();
        
        //Setup train method
        network.setRunOperation (Network.TRAIN);
        
        //Run until trained
        Print.debug("Training Patterns");
        network.run();
        Print.debug("Patterns Trained");
        
        //Setup stable method
        network.setRunOperation (Network.ALL_STABLE);
        
        //Run until stable
        Print.debug("Stabilizing Patterns");
        network.run(false); //Don't display
        Print.debug("Patterns Stable");
        
        //Find out how many patterns are stable
        int stable = network.getStableCount();
        Print.debug("Stable->" + stable + (stable>0 ? " and not stable." : " and stable."));
        return stable;
    }
    
    private void initPatterns(NetSimFrame netFrame, int numPatterns, float bias, boolean duplicates){
        //Auto pattern generator
        PatternGeneratorFrame patternGen = netFrame.getPatternGenerator();
        
        //Clear pre-existing patterns
        TrainingSet trainSet = (TrainingSet)(accessPrivateField(netFrame, "trainingSet"));
        //trainSet.removePattern(0); //Re-clear set
        //trainSet.removePattern(0); //Re-clear set
        //Remove old pattterns
        while(trainSet.numPatterns()>0){
            trainSet.removePattern(0); //Remove old patterns
        }
        
        blank = trainSet;
    }
    
    private void initNetWizard(NetSimFrame netFrame, int size, int pattern, NetSim.NetworkType type){
        //Auto NetSim Wizard
        NetWizard netWizard = (NetWizard)(accessPrivateField(netFrame, "netWizard"));
        //netWizard.setVisible(true);
        
        //Set values of network
        //Assertion.test(setPrivateField(netWizard, "numUnits", size));
        NumericField sizeField = (NumericField)(accessPrivateField(netWizard, "networkSize"));
        sizeField.setText(Integer.toString(size));
        Assertion.test(setPrivateField(netWizard, "patternType", pattern));
        JComboBox combo = (JComboBox)(accessPrivateField(netWizard, "learningRuleChooser"));
        combo.setSelectedIndex(type.ordinal());
        
        //Cut straight to finnish
        accessPrivateMethod(netWizard, "processCard2", new Object[]{}); //Make sure rule is loaded in
        NavPanel navPanelNetWizard = netWizard.getNavPanel();
        Assertion.test(setPrivateField(navPanelNetWizard, "action", NavPanel.FINISH));
        Assertion.test(setPrivateField(netWizard, "finished", true));
        
        //Make sure we now exit the window so that the other windows are generated
        Assertion.test(netWizard.finishWizard());
        Assertion.test(netWizard.hasFinished());
        
        //Generate Network
        Print.debug("Generating network");
        netFrame.actionPerformed(null); //Emulate some action
        Print.debug("Network generated");
    }
    
    private void output(String output){ out.println(output); }
    
    private Object accessPrivateMethod(Object obj, String name, Object[] params){
        try{
            Method method = obj.getClass().getDeclaredMethod(name);
            method.setAccessible(true);
            return method.invoke(obj, params);
        }catch(NoSuchMethodException e){ return null; }
        catch(IllegalAccessException e){ return null; }
        catch(InvocationTargetException e){ return null; }
    }
    
    private Object accessPrivateField(Object obj, String name){
        try{
            Field field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            return field.get(obj);
        }catch(NoSuchFieldException e){ return null; }
        catch(IllegalAccessException e){ return null; }
    }
    
    private boolean setPrivateField(Object obj, String name, Object val){
        try{
            Field field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            field.set(obj, val);
            return true;
        }catch(NoSuchFieldException e){ return false; }
        catch(IllegalAccessException e){ return false; }
    }
}