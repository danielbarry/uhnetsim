


abstract public class AbstractLearningRule {
    //-----------------------------------
    abstract public int train
	  (Network network, TrainingSet trainingSet);

    //-----------------------------------
    abstract public int train
	  (Network network, TrainingSet trainingSet, int maxIterations, double learningRate, double target);
	  //For error-correction training algorithms the target is the mean squared error target 
	  //For perceptron training the target is the training threshold
	  //maxIterations is the maximum number of times the training set will be presented to the network
	  //before training is stopped
	  
    //-----------------------------------
    abstract public String getName ();
    
}