/**
 * A class that implements the Storkey learning rule.
 * 
 * @author Steve Hunt 
 * @version 1.0
 * 
 */

public class LR01Storkey extends AbstractLearningRule {
	final static String PLUGIN_NAME = "Storkey Rule";
	//----------------------------------------
	public int train
	  (Network network, TrainingSet trainingSet)
	{
        int iterationCount   = 0;
        int numUnits         = 0;
        int unitCount        = 0;
        int inputCount       = 0;
        int numPatterns      = 0;
        int patternCount     = 0;
        double currentWeight = 0;
        double newWeight     = 0;
        double weightDelta   = 0;
        double [] netField   = null;
        
        numUnits = network.numUnits ();
        numPatterns = trainingSet.numPatterns ();
        netField = new double [numUnits];
        
        // for every pattern
        for (patternCount=0;patternCount<numPatterns;patternCount++) {

            // first work out the net field for each unit in the network for this pattern
            // for every unit
            for (unitCount=0;unitCount<numUnits;unitCount++) {
                netField [unitCount] = 0.0;
                
                // for every input
                for (inputCount=0;inputCount<numUnits;inputCount++) {
                
                    // skip over self-connections
                    if (unitCount == inputCount) continue;
                
                    netField [unitCount]
                    = netField [unitCount]
                      + (  network.getWeight (unitCount, inputCount)
                         * trainingSet.getUnit (patternCount, inputCount) );
                }
            }
            
            // now we can update the weights for this pattern
            // for every unit
            for (unitCount=0;unitCount<numUnits;unitCount++) {
            
                // for every input
                for (inputCount=0;inputCount<numUnits;inputCount++) {
                
                    // skip over self-connections
                    if (unitCount == inputCount) continue;

                    // retrieve the current weight for this unit/input pair
                    currentWeight = network.getWeight (unitCount, inputCount);

                    // calculate the weight change
                    weightDelta =
                      trainingSet.getUnit (patternCount, unitCount) * trainingSet.getUnit (patternCount, inputCount)
                    - netField [unitCount] * trainingSet.getUnit (patternCount, inputCount)
                    - netField [inputCount] * trainingSet.getUnit (patternCount, unitCount);
                    weightDelta = weightDelta / network.numUnits ();

                    // calculate the new weight
                    newWeight = currentWeight + weightDelta;

                    // update the weight for this pair
                    network.setWeight (unitCount, inputCount, newWeight);
                }
            }
        }
        iterationCount++;

        return iterationCount;
	}

	public int train
	  (Network network, TrainingSet trainingSet, int maxIterations, double learningRate, double target)
	{
        //---- This overloading of the 'train' method is included for future expansion only: do not use it
	    return 0;
	}

	//----------------------------------------
	public String getName () {
		return PLUGIN_NAME;
	}

}
