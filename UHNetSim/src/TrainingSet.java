

import java.io.*;
import java.util.*;

public class TrainingSet {
    public final static int SAVE_SUCCESS      = -1;
    public final static int LOAD_SUCCESS      = -1;
    public final static int FILE_NOT_FOUND    = 0;
    public final static int NO_PATTERN_SIZE   = 1;
    public final static int NO_BIT_FOUND      = 2;
    public final static int INVALID_BIT_VALUE = 3;
    public final static int PREMATURE_END     = 4;
    public final static int IO_EXCEPTION      = 5;
    public final static int NOT_KNOWN         = -1;
    public final static int BIPOLAR           = 0;
    public final static int BINARY            = 1;

    private int patternWidth = 0;
    private int patternType  = TrainingSet.NOT_KNOWN;
    private int highValue    = 1;
    private int lowValue     = -1;

    private ArrayList patterns = null;

    //-----------------------------------
    public TrainingSet () {
        this.patterns = new ArrayList ();
    }

    //-----------------------------------
    public TrainingSet (int width, int numPatterns) {
        int i;

        this.patternWidth = width;
        this.patterns = new ArrayList ();

        for (i=0;i<numPatterns;i++) {
            this.patterns.add (i, new int[width]);
        }
    }

    //-----------------------------------
    public void clear () {
        this.patternType = TrainingSet.NOT_KNOWN;
        this.patternWidth = 0;
        this.patterns.clear ();
    }

    //-----------------------------------
    public int getPatternType () {
        return this.patternType;
    }

    //-----------------------------------
    public void setPatternType (int patternType) {
        int i;

        if (patternType != TrainingSet.BIPOLAR && patternType != TrainingSet.BINARY) {
            System.out.println ("Attempted to set the pattern type to an illegal value in TrainingSet.setPatternType (int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        if (patternType == TrainingSet.BIPOLAR) {
            this.patternType = TrainingSet.BIPOLAR;
            this.lowValue = -1;
        } else {
            this.patternType = TrainingSet.BINARY;
            this.lowValue = 0;
        }
/*
        for (i=0;i<this.patterns.size ();i++) {
            Arrays.fill ((int[])this.patterns.get (i), this.lowValue);
        }
*/
    }

    //-----------------------------------
	public int getUnit (int patternIndex, int unitIndex) {
        int[] tempPattern = this.getPattern (patternIndex);

        if (patternIndex >= this.patterns.size ()) {
            System.out.println ("Attempted to access pattern that does not exist in TrainingSet.getUnit (" + patternIndex + ", " + unitIndex + ")");
            Thread.dumpStack ();
            System.exit (2);
        }
        if (unitIndex >= this.patternWidth) {
            System.out.println ("Attempted to access unit that does not exist in TrainingSet.getUnit (" + patternIndex + ", " + unitIndex + ")");
            Thread.dumpStack ();
            System.exit (2);
        }
        return tempPattern[unitIndex];
    }

    //-----------------------------------
	public int[] getPattern (int patternIndex) {
        int unitIndex     = 0;
        int[] tempPattern = null;
        int[] copyPattern = null;

        if (patternIndex >= this.patterns.size ()) {
            System.out.println ("Attempted to retrieve a pattern outside the pattern set in TrainingSet.getPattern (int)");
            Thread.dumpStack ();
            System.exit (2);
        }

        tempPattern = (int[])this.patterns.get (patternIndex);
        copyPattern = new int[this.patternWidth];

        for (unitIndex=0;unitIndex<this.patternWidth;unitIndex++) {
            copyPattern[unitIndex] = tempPattern[unitIndex];
        }

        return copyPattern;
    }

    //-----------------------------------
    public boolean getBooleanUnit (int patternIndex, int unitIndex) {
        int[] tempPattern = this.getPattern (patternIndex);

        if (patternIndex >= this.patterns.size ()) {
            System.out.println ("Attempted to access pattern that does not exist in TrainingSet.getBooleanUnit (int, int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        if (unitIndex >= this.patternWidth) {
            System.out.println ("Attempted to access unit that does not exist in TrainingSet.getBooleanUnit (int, int)");
            Thread.dumpStack ();
            System.exit (2);
        }

        return (tempPattern[unitIndex] == 1) ? true : false;
    }

    //-----------------------------------
    public boolean[] getBooleanPattern (int patternIndex) {
        int elementIndex      = 0;
        boolean[] tempPattern = new boolean[this.patternWidth];

        if (patternIndex >= this.patterns.size ()) {
            System.out.println ("Attempted to access a pattern outside the pattern set in TrainingSet.getBooleanPattern (int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        for (elementIndex=0;elementIndex<this.patternWidth;elementIndex++) {
            tempPattern[elementIndex] = this.getBooleanUnit (patternIndex, elementIndex);
        }

        return tempPattern;
    }

    //-----------------------------------
	public void toggleUnit (int patternIndex, int unitIndex) {
        if (patternIndex >= this.patterns.size ()) {
            System.out.println ("Attempted to access a pattern outside the pattern set in TrainingSet.toggleUnit (int, int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        if (unitIndex >= this.patternWidth) {
            System.out.println ("unitIndex is greater than the number of units in TrainingSet.toggleUnit (int, int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        if (this.getUnit (patternIndex, unitIndex) != this.highValue) {
            this.setUnit (patternIndex, unitIndex, this.highValue);
        } else {
            this.setUnit (patternIndex, unitIndex, this.lowValue);
        }
    }

    //-----------------------------------
	public void setUnit (int patternIndex, int unitIndex, int state) {
        if (patternIndex >= this.patterns.size ()) {
            System.out.println ("Attempted to access a pattern outside the pattern set in TrainingSet.setUnit (int, int, int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        if (unitIndex >= this.patternWidth) {
            System.out.println ("unitIndex is greater than the number of units in TrainingSet.setUnit (int, int, int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        int[] tempPattern = this.getPattern (patternIndex);

        tempPattern[unitIndex] = state;

        this.setPattern (patternIndex, tempPattern);
    }

    //-----------------------------------
    public void setPattern (int patternIndex, int[] pattern) {
        int unitIndex    = 0;
        int[] newPattern = new int[this.patternWidth];

        // see if the pattern exists
        if (patternIndex >= this.patterns.size ()) {
            System.out.println ("Attempted to set a pattern which does not exist in TrainingSet.setPattern (int, boolean[])");
            Thread.dumpStack ();
            System.exit (2);
        }
        // see if the pattern is of the correct length
        if (pattern.length != this.patternWidth) {
            System.out.println ("Attempted to set pattern of incorrect length in TrainingSet.setPattern (int, boolean[])");
            Thread.dumpStack ();
            System.exit (2);
        }
        for (unitIndex=0;unitIndex<this.patternWidth;unitIndex++) {
            newPattern[unitIndex] = pattern[unitIndex];
        }
        this.patterns.set (patternIndex, newPattern);
    }

    //-----------------------------------
	public void addPattern () {
        int unitIndex    = 0;
        int[] newPattern = new int[this.patternWidth];

        Arrays.fill (newPattern, this.lowValue);

        this.patterns.add (newPattern);
    }

    //-----------------------------------
	public void removePattern (int patternIndex) {
        if (patternIndex >= this.patterns.size ()) {
            System.out.println ("Attempted to remove pattern that does not exist in TrainingSet.removePattern (int)");
            Thread.dumpStack ();
            System.exit (2);
        }
        this.patterns.remove (patternIndex);
    }

    //-----------------------------------
    public int loadTrainingSet (File file) {
        FileReader fileReader           = null;
        StreamTokenizer streamTokenizer = null;
        int unitIndex                   = 0;
        int[] tempPattern               = null;

        // open the file if we can
        try {
            fileReader = new FileReader (file);
        } catch (FileNotFoundException e) {
            return TrainingSet.FILE_NOT_FOUND;
        }
        streamTokenizer = new StreamTokenizer (fileReader);
        streamTokenizer.eolIsSignificant(false);

        // find out the size of the patterns, it's on the first line
        try {
            if (streamTokenizer.nextToken () != StreamTokenizer.TT_NUMBER) {
                System.out.println (streamTokenizer.sval);
                fileReader.close ();
                return TrainingSet.NO_PATTERN_SIZE;
            }
            this.patternWidth = (int)streamTokenizer.nval;
            // retrieve each pattern in turn
            while (true) {
                tempPattern = new int[patternWidth];
                for (unitIndex=0;unitIndex<patternWidth;unitIndex++) {
                    streamTokenizer.nextToken ();
                    if (streamTokenizer.ttype == StreamTokenizer.TT_EOF) {
                        if (unitIndex != 0) {
                            fileReader.close ();
                            return TrainingSet.PREMATURE_END;
                        } else {
                            fileReader.close ();
                            return TrainingSet.LOAD_SUCCESS;
                        }
                    } else if (streamTokenizer.ttype != StreamTokenizer.TT_NUMBER) {
                        fileReader.close ();
                        return TrainingSet.NO_BIT_FOUND;
                    }

                    if ((int)streamTokenizer.nval == 1) {
                        tempPattern[unitIndex] = 1;
                    } else if ((int)streamTokenizer.nval == -1) {
                        if (this.patternType == TrainingSet.NOT_KNOWN) {        // if the pattern type isn't known...
                            this.setPatternType (TrainingSet.BIPOLAR);          // ...set it to BIPOLAR
                        } else if (this.patternType == TrainingSet.BINARY) {    // else if we already know it's BINARY so...
                            fileReader.close ();                                // ...close the file and...
                            return TrainingSet.INVALID_BIT_VALUE;               // ...return with an error code
                        }
                        tempPattern[unitIndex] = -1;
                    } else if ((int)streamTokenizer.nval == 0) {
                        if (this.patternType == TrainingSet.NOT_KNOWN) {        // if the pattern type isn't known...
                            this.setPatternType (TrainingSet.BINARY);           // ...set it to BINARY
                        } else if (this.patternType == TrainingSet.BIPOLAR) {   // else if we already know it's BIPOLAR so...
                            fileReader.close ();                                // ...close the file and...
                            return TrainingSet.INVALID_BIT_VALUE;               // ...return with an error code
                        }
                        tempPattern[unitIndex] = 0;
                    } else {
                        fileReader.close ();
                        return TrainingSet.INVALID_BIT_VALUE;
                    }
                }
                this.addPattern ();
                this.setPattern (this.patterns.size () - 1, tempPattern);
            }
        } catch (IOException e) {
            return TrainingSet.IO_EXCEPTION;
        }
    }

    //-----------------------------------
    public int saveTrainingSet (File file) {
        int patternIndex      = 0;
        int unitIndex         = 0;
        int[] tempPattern     = null;
        FileWriter fileWriter = null;

        try {
        	fileWriter = new FileWriter (file);

            // write out the pattern size
            fileWriter.write (this.patternWidth + "\n");

            // write out the patterns
            for (patternIndex=0;patternIndex<patterns.size ();patternIndex++) {
                // get a pattern
                tempPattern = (int[])patterns.get (patternIndex);

                for (unitIndex=0;unitIndex<tempPattern.length;unitIndex++) {
                    if (tempPattern[unitIndex] == 1) {
                        fileWriter.write (" " + Integer.toString (this.highValue));
                    } else {
                        if (this.patternType == TrainingSet.BINARY) fileWriter.write (" ");
                        fileWriter.write (Integer.toString (this.lowValue));
                    }
                    if (unitIndex != tempPattern.length - 1) fileWriter.write (" ");
                }
                fileWriter.write ("\n");
            }
            fileWriter.close ();
            return TrainingSet.SAVE_SUCCESS;
        } catch (IOException e) {
            return TrainingSet.IO_EXCEPTION;
        }
    }

    //-----------------------------------
	public int numPatterns () {
        return this.patterns.size ();
    }

    //-----------------------------------
	public int getPatternWidth () {
        return this.patternWidth;
    }

    //-----------------------------------
    public void setPatternWidth (int patternWidth) {
        this.patternWidth = patternWidth;
    }

    //-----------------------------------
    public boolean isTrainingPattern(long pattern){
        boolean isTrainingPattern = false;

        for (int i = 0; i < patterns.size () && !isTrainingPattern; i++) {
             int[] tempPattern = (int[])patterns.get (i);
             isTrainingPattern = Network.getLongState(tempPattern) == pattern;
        }

        return isTrainingPattern;
    }
}
