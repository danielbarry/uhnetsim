

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;
import javax.swing.event.*;

public class UnitGrid extends JComponent implements MouseListener {
    private int unitPixelSize              = 20;
    private int xOffset                    = 0;
    private Dimension unitDimensions       = null;
    private Dimension canvasSize           = null;
    private boolean[][] unitState          = null;
    private boolean[][] grayUnit           = null;
    private EventListenerList listenerList = null;
    private boolean enabled                = true;

    // declaring these once for speed
    private Line2D.Double gridLine      = null;
    private Rectangle2D.Double bgRect   = null;
    private Rectangle2D.Double unitRect = null;

     //-----------------------------------
    public UnitGrid (int numUnits) {
        this.unitDimensions = this.calcUnitDimensions (numUnits);
        this.unitState = new boolean[this.unitDimensions.width][this.unitDimensions.height];
        this.grayUnit  = new boolean[this.unitDimensions.width][this.unitDimensions.height];
        this.canvasSize = this.calcCanvasSize (this.unitDimensions);
        this.setPreferredSize (this.canvasSize);
        this.listenerList = new EventListenerList ();
        this.addMouseListener (this);

        // some shapes needed by paintComponent
        this.gridLine = new Line2D.Double ();
        this.bgRect = new Rectangle2D.Double ();
        this.unitRect = new Rectangle2D.Double ();
    }

    //-----------------------------------
    private int calculateXOffset () {
        int xDiff = 0;

        xDiff = super.getWidth () - this.canvasSize.width;
        if (xDiff > 0) {
            if (xDiff % 2 != 0) {
                super.setSize (new Dimension (super.getWidth () + 1, super.getHeight ()));
                xDiff++;
            }
            return (int)xDiff / 2;
        } else {
            return 0;
        }
    }

    //-----------------------------------
    public void mouseClicked (MouseEvent e) {
        int gridX = 0;
        int gridY = 0;

        if (this.enabled == true) {
            if (((e.getX () - this.xOffset) % (this.unitPixelSize + 1) == 0) || ((e.getY () + 1) % (this.unitPixelSize + 1) == 0)) {
                // we clicked on a gridline so do nothing
            } else if ((e.getX () < this.xOffset) || (e.getX () > this.canvasSize.width + this.xOffset - 1)) {
                // we're outside the clickable area so do nothing
            } else {
                gridX = (int)((e.getX () - this.xOffset) / (this.unitPixelSize + 1));
                gridY = (int)(e.getY () / (this.unitPixelSize + 1));

                this.unitState[gridX][gridY] = !this.unitState[gridX][gridY];
                this.initGrayUnits();
                this.fireUnitGridUnitChanged (gridX, gridY);
                this.repaint ();
            }
        }
    }

    //-----------------------------------
    public void mouseEntered (MouseEvent e) {}
    public void mouseExited (MouseEvent e) {}
    public void mousePressed (MouseEvent e) {}
    public void mouseReleased (MouseEvent e) {}

    //-----------------------------------
    public void addUnitGridListener (UnitGridListener listener) {
        listenerList.add (UnitGridListener.class, listener);
    }

    //-----------------------------------
    public void removeUnitGridListener (UnitGridListener listener) {
        listenerList.remove (UnitGridListener.class, listener);
    }

    //-----------------------------------
    private void fireUnitGridPixelSizeChange () {
        int i                       = 0;
        Object[] listeners          = listenerList.getListenerList ();
        UnitGridEvent unitGridEvent = null;

        for (i=listeners.length-2;i>=0;i-=2) {
            if (listeners[i]==UnitGridListener.class) {
                if (unitGridEvent == null) unitGridEvent = new UnitGridEvent(this);
                ((UnitGridListener)listeners[i+1]).unitGridPixelSizeChange (unitGridEvent);
           }
        }
    }

    //-----------------------------------
    private void fireUnitGridUnitChanged (int unitX, int unitY) {
        int i                       = 0;
        Object[] listeners          = listenerList.getListenerList ();
        UnitGridEvent unitGridEvent = null;

        for (i=listeners.length-2;i>=0;i-=2) {
            if (listeners[i]==UnitGridListener.class) {
                if (unitGridEvent == null) unitGridEvent = new UnitGridEvent(this);

                // set up the event with the required information
                unitGridEvent.unitGridDimensions = this.unitDimensions;
                unitGridEvent.unitX = unitX;
                unitGridEvent.unitY = unitY;
                unitGridEvent.unitState = this.unitState[unitX][unitY];

                ((UnitGridListener)listeners[i+1]).unitGridUnitChanged (unitGridEvent);
           }
        }
    }

    //-----------------------------------
    public Dimension calcUnitDimensions (int numUnits) {
        int numUnitsX = 0;
        int numUnitsY = 0;
        int optimumY  = 0;
        int smallDiff = numUnits;
        int newDiff   = 0;

        // find the optimum way displaying the specified number of units
        for (numUnitsY=1; numUnitsY<=(int)(numUnits/2); numUnitsY++) {
            numUnitsX = numUnits / numUnitsY;

            if (numUnitsX * numUnitsY != numUnits) continue;

            newDiff = Math.abs ((int)(numUnitsX - numUnitsY));

            if (newDiff < smallDiff) {
                optimumY = numUnitsY;
                smallDiff = newDiff;
            }
        }
        return new Dimension (numUnits / optimumY, optimumY);
    }

    //-----------------------------------
    public Dimension getUnitDimensions () {
        return this.unitDimensions;
    }

    //-----------------------------------
    private Dimension calcCanvasSize (Dimension unitDimensions) {
        Dimension canvasSize = new Dimension ();

        canvasSize.width = (unitDimensions.width * this.unitPixelSize) + (unitDimensions.width + 1);
        canvasSize.height = (unitDimensions.height * this.unitPixelSize) + (unitDimensions.height - 1);

        return canvasSize;
    }

    //-----------------------------------
    protected void paintComponent (Graphics g) {
        int x = 0;
        int y = 0;

        Graphics2D g2 = (Graphics2D)g;

        this.xOffset = this.calculateXOffset ();

        // draw a plain background
        this.bgRect.setRect (0, 0, super.getWidth (), super.getHeight ());
        g2.setPaint (Color.lightGray );
        g2.fill (bgRect);

        g2.setPaint (Color.darkGray);
        // draw the grid
        for (x=0+this.xOffset;x<=this.canvasSize.width+this.xOffset;x+=(this.unitPixelSize+1)) {
            this.gridLine.setLine (x, 0, x, this.canvasSize.height - 1);
            g2.draw (gridLine);
        }
        for (y=this.unitPixelSize;y<this.canvasSize.height;y+=(this.unitPixelSize+1)) {
            this.gridLine.setLine (0 + this.xOffset, y, this.canvasSize.width - 1 + this.xOffset, y);
            g2.draw (gridLine);
        }

        // draw the units
        for (y=0;y<this.unitDimensions.height;y++) {
            for (x=0;x<this.unitDimensions.width;x++) {
                if (this.unitState[x][y] == true) {
                    this.unitRect.setRect ((x * this.unitPixelSize) + (x + 1) + this.xOffset, (y * this.unitPixelSize) + y, this.unitPixelSize, this.unitPixelSize);
                    g2.setPaint (Color.black);
                    g2.fill (this.unitRect);
                }else{
                    this.unitRect.setRect ((x * this.unitPixelSize) + (x + 1) + this.xOffset, (y * this.unitPixelSize) + y, this.unitPixelSize, this.unitPixelSize);
                    g2.setPaint (Color.white);
                    g2.fill (this.unitRect);
                }
                if (this.grayUnit[x][y]){
                    this.unitRect.setRect ((x * this.unitPixelSize) + (x + 1) + this.xOffset, (y * this.unitPixelSize) + y, this.unitPixelSize, this.unitPixelSize);
                    g2.setPaint (Color.gray);
                    g2.fill (this.unitRect);
                }else{
                }
            }
        }
    }

    //-----------------------------------
    public void decreaseUnitPixelSize () {
        if (this.unitPixelSize > 10) {
            this.unitPixelSize -= 10;
            this.canvasSize = this.calcCanvasSize (this.unitDimensions);
            this.setPreferredSize (this.canvasSize);
            this.fireUnitGridPixelSizeChange ();
         }
    }

    //-----------------------------------
    public void increaseUnitPixelSize () {
        if (this.unitPixelSize < 60) {
            this.unitPixelSize += 10;
            this.canvasSize = this.calcCanvasSize (this.unitDimensions);
            this.setPreferredSize (this.canvasSize);
            this.fireUnitGridPixelSizeChange ();
         }
    }

    //-----------------------------------
    public void setUnit (int unitX, int unitY, boolean state) {
        this.unitState[unitX][unitY] = state;
        initGrayUnits();
    }

    //-----------------------------------
    public void setUnits (boolean[] units) {
        if ((this.unitDimensions.width * this.unitDimensions.height) != units.length) {
            System.out.println ("Unit array length mismatch in UnitGrid.setUnits (boolean[])");
            Thread.dumpStack ();
            System.exit (2);
        } else {
            for (int y=0;y<this.unitDimensions.height;y++) {
                for (int x=0;x<this.unitDimensions.width;x++) {
                    this.unitState[x][y] = units[(y*this.unitDimensions.width) + x];
                    this.grayUnit[x][y] = false;
                }
            }
        }
    }

    //-----------------------------------
    public boolean[] getUnits () {
        int x, y;
        boolean[] tempPattern = new boolean[this.unitDimensions.width * this.unitDimensions.height];

        for (y=0;y<this.unitDimensions.height;y++) {
            for (x=0;x<this.unitDimensions.width;x++) {
                tempPattern[(y * this.unitDimensions.width) + x] = this.unitState[x][y];
            }
        }

        return tempPattern;
    }

    //-----------------------------------
    private void initGrayUnits(){
        for (int y=0; y<this.unitDimensions.height; y++) {
            for (int x=0; x<this.unitDimensions.width; x++) {
                this.grayUnit[x][y] = false;
            }
        }
    }

    //-----------------------------------
    public void setGrayUnits (boolean[] units) {
        if ((this.unitDimensions.width * this.unitDimensions.height) != units.length) {
            System.out.println ("GrayUnit array length mismatch in UnitGrid.setGrayUnits (boolean[])");
            Thread.dumpStack ();
            System.exit (2);
        } else {
            for (int y=0;y<this.unitDimensions.height;y++) {
                for (int x=0;x<this.unitDimensions.width;x++) {
                    this.grayUnit[x][y] = units[(y*this.unitDimensions.width) + x];
                }
            }
        }
    }

   //-----------------------------------
    public void setEnabled (boolean state) {
        enabled = state;
    }
}