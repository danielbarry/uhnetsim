


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Connections3DViewer extends JInternalFrame{

    NetSimFrame parent = null;

    Connections3DViewer(NetSimFrame parent){
        super("Connections-3D", true, true, true, true);
        setTitle(getTitle() + " ( "
                 + Experiments.formatPercentage(parent.getPrunedNetwork().getAxonalReduction()) + ", "
                 + Experiments.formatPercentage(parent.getPrunedNetwork().getLengthReduction()) + ", "
                 + Experiments.formatPercentage(parent.getPrunedNetwork().getSynapticReduction()) + " )"
        );

        this.parent = parent;

        Connections3DPanel panel = new Connections3DPanel(parent);
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add (panel, BorderLayout.CENTER);
        this.setSize( panel.DEFAULTWIDTH, panel.DEFAULTHEIGHT);
        this.setLocation(0, 100);
        parent.addChildFrame(this);
        this.setVisible(false);
    }

}

class Connections3DPanel extends JPanel implements MouseListener, MouseMotionListener {

    NetSimFrame parent = null;
    Connections3D md;
    boolean painted = true;
    float xfac;
    int prevx, prevy;
    float xtheta, ytheta;
    float scalefudge = 1;
    Matrix3D amat = new Matrix3D(), tmat = new Matrix3D();

    static int DEFAULTWIDTH = 400;
    static int DEFAULTHEIGHT = 400;

    Connections3DPanel(NetSimFrame parent){
        this.parent = parent;

	    amat.yrot(0);
    	amat.xrot(0);

	    md = new Connections3D ();
        buildNodes();
        buildConnections();
        md.findBB();
	    md.compress();
	    float xw = md.xmax - md.xmin;
	    float yw = md.ymax - md.ymin;
	    float zw = md.zmax - md.zmin;
	    if (yw > xw)
    		xw = yw;
	    if (zw > xw)
	    	xw = zw;
	    float f1 = DEFAULTWIDTH / xw;
	    float f2 = DEFAULTHEIGHT / xw;
	    xfac = 0.7f * (f1 < f2 ? f1 : f2) * scalefudge;

        addMouseListener(this);
    	addMouseMotionListener(this);
    }

    private void buildNodes(){
        int numCols = parent.getNetDisplay ().getUnitDimensions().width;
        int numRows = parent.getNetwork().numUnits() / numCols;
        float[][] z = new float[numRows][numCols];
        calculateZ(z);
        for(int i = 0; i < parent.getNetwork().numUnits(); i++){
            int x = i % numCols;
            int y = i / numCols;
            md.addVert(x, numRows - y - 1, z[y][x]);
        }
    }

    private void calculateZ(float[][] Z){
        int numCols = Z[0].length;
        int numRows = Z.length;
        float z = -1;
        for(int y = 0; y < numRows; y++){
            for(int x = 0; x < numCols; x++){
                if(x*2 == numCols);
                else if(x*2 < numCols) z++;
                else z--;
                Z[y][x] = z;
            }
            if((y+1)*2 == numRows) z--;
            else if((y+1)*2 < numRows);
            else z-=2;
        }
    }

    private void buildConnections(){

        for(int i = 0; i < parent.getNetwork().numUnits(); i++ ){
            for(int j = 0; j < parent.getNetwork().numUnits(); j++){
                if( !parent.getPrunedNetwork().isPruned(i*parent.getNetwork().numUnits()+j)){
                    md.add(i, j);
                }
            }
        }
    }

    public  void mouseClicked(MouseEvent e) {
    }

    public  void mousePressed(MouseEvent e) {
        prevx = e.getX();
        prevy = e.getY();
        e.consume();
    }

    public  void mouseReleased(MouseEvent e) {
    }

    public  void mouseEntered(MouseEvent e) {
    }

    public  void mouseExited(MouseEvent e) {
    }

    public  void mouseDragged(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();

        tmat.unit();
        float xtheta = (prevy - y) * 360.0f / getSize().width;
        float ytheta = (x - prevx) * 360.0f / getSize().height;
        tmat.xrot(xtheta);
        tmat.yrot(ytheta);
        amat.mult(tmat);
        if (painted) {
            painted = false;
            repaint();
        }
        prevx = x;
        prevy = y;
        e.consume();
    }

    public  void mouseMoved(MouseEvent e) {
    }

    public void paint(Graphics g) {
        Dimension d = getSize();
        g.setColor(Color.white);
        g.fillRect(0, 0, d.width, d.height);
        md.mat.unit();
        md.mat.translate(-(md.xmin + md.xmax) / 2,
            -(md.ymin + md.ymax) / 2,
			-(md.zmin + md.zmax) / 2);
        md.mat.mult(amat);
        md.mat.scale(xfac, -xfac, 16 * xfac / getSize().width);
        md.mat.translate(getSize().width / 2, getSize().height / 2, 8);
        md.transformed = false;
        md.paint(g);
        painted = true;
    }
}
