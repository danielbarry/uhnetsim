

/*
 * @(#)DynamicDiagram.java
 * WKAye
 */
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class Node {
    float x;
    float y;
    float dx;
    float dy;
    int label;
    int attraction;
    boolean isParasite;

    Node(int label){
        this.label = label;
        this.x = DynamicDiagram.PANELWIDTH * (float) Math.random();
        this.y = DynamicDiagram.PANELHEIGHT * (float) Math.random();
    }
}

class Edge {
    Node from = null;
    Node to = null;
    boolean isFinal = false;
}

public class DynamicDiagram extends JPanel implements Runnable, MouseListener, MouseMotionListener {

    NetSimFrame parent            = null;
    DynamicViewer viewer          = null;
    int numStates                 = 0;
    ArrayList edges               = null;
    int numEdges                  = 0;
    Integer finalState            = null;

    static final char PANELWIDTH  = 700;
    static final char PANELHEIGHT = 400;
    final Color BACKCOLOR         = Color.white;
    final Color FROMEDGECOLOR     = Color.lightGray;
    final Color TOEDGECOLOR       = Color.black;
    final Color CYCLECOLOR        = Color.black;
    final Color NODECOLOR         = Color.pink;
    final Color ATTRACTORCOLOR    = Color.red;
    final Color PARASITECOLOR     = Color.blue;
    final Color PICKCOLOR         = Color.green;
    final Color LABELCOLOR        = Color.black;
    final Color ATTRACTIONCOLOR   = Color.black;
    final int MINNODESIZE         = 6;
    final int MAXNODESIZE         = 206;
    final short MOUSESENSITIVITY  = 256;

    Thread relaxer                = null;
    Image offscreen               = null;
    Dimension offscreensize       = null;
    Graphics offgraphics          = null;
    Node pick                     = null;

	boolean labelVisible          = false;
    boolean attractionVisible     = false;
    byte refreshRate              = 90;
    byte refreshCounter           = 10;


    DynamicDiagram(NetSimFrame parent, DynamicViewer viewer) {
        this.parent = parent;
        this.viewer = viewer;
        numStates = (int)Math.pow(2.0, parent.getNetwork().numUnits());
        addMouseListener(this);
    }

    public void init(){
        if( finalState == null ){
            this.edges = viewer.dynamic.getEdges();
        }else{
            this.edges = viewer.dynamic.getEdges(finalState.intValue());
        }
        this.numEdges = edges.size();
        viewer.setResult(viewer.dynamic.getResult());
    }

    public void run() {
        Thread me = Thread.currentThread();
        if( edges == null ){ init(); }
        while (relaxer == me) {
            relax();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                break;
            }
        }
    }

    synchronized void relax() {
        for (int i = 0; i < numEdges; i++) {
            Edge e = (Edge)edges.get(i);
    	    double vx = e.to.x - e.from.x;
            double vy = e.to.y - e.from.y;
            double len = Math.sqrt(vx * vx + vy * vy);
            len = (len == 0) ? .0001 : len;
            double f = (50 - len) / (len * 3);
            double dx = f * vx;
            double dy = f * vy;

            e.to.dx += dx;
            e.to.dy += dy;
            e.from.dx += -dx;
            e.from.dy += -dy;
        }
    	for (int i = 0; i < numEdges; i++) {
            Node n1 = ((Edge)edges.get(i)).from;
            double dx = 0;
    	    double dy = 0;
            for (int j = 0; j < numEdges; j++) {
                if (i == j) { continue; }
                Node n2 = ((Edge)edges.get(j)).from;
                double vx = n1.x - n2.x;
                double vy = n1.y - n2.y;
                double len = vx * vx + vy * vy;
                if (len == 0) {
                    dx += Math.random();
                    dy += Math.random();
                } else if (len < 100*100) {
                    dx += vx / len;
                    dy += vy / len;
                }
            }
    	    double dlen = dx * dx + dy * dy;
            if (dlen > 0) {
                dlen = Math.sqrt(dlen) / 2;
                n1.dx += dx / dlen;
                n1.dy += dy / dlen;
            }
    	}
        Dimension d = getSize();
    	for (int i = 0; i < numEdges; i++) {
            Node n = ((Edge)edges.get(i)).from;
            n.x += Math.max(-5, Math.min(5, n.dx));
            n.y += Math.max(-5, Math.min(5, n.dy));
            if (n.x < 10) {
                n.x = 10;
            } else if (n.x > d.width - 10) {
                n.x = d.width - 10;
            }
            if (n.y < 10) {
                n.y = 10;
            } else if (n.y > d.height - 10) {
                n.y = d.height - 10;
            }
    	    n.dx /= 2;
            n.dy /= 2;
    	}
        if( (refreshCounter++) > (100-refreshRate)) {
    	    repaint();
            refreshCounter = 0;
        }
    }

    public synchronized void paint(Graphics g) {
    	Dimension d = getSize();
        if((offscreen==null)||(d.width!=offscreensize.width)
               || (d.height!=offscreensize.height)) {
            offscreen = createImage(d.width, d.height);
            offscreensize = d;
            if (offgraphics != null) {
                offgraphics.dispose();
            }
    	    offgraphics = offscreen.getGraphics();
            offgraphics.setFont(getFont());
    	}
        offgraphics.setColor(BACKCOLOR);
    	offgraphics.fillRect(0, 0, d.width, d.height);
        for (int i = 0 ; i < numEdges ; i++) {
            Edge e = (Edge)edges.get(i);
            if (e.from.label == e.to.label) continue;

            int x1 = (int)e.from.x;
            int y1 = (int)e.from.y;
            int x2 = (int)e.to.x;
    	    int y2 = (int)e.to.y;

            if( e.isFinal ){
                // 2 points cycle
                offgraphics.setColor(CYCLECOLOR);
                offgraphics.drawLine(x1, y1, x2, y2);
            }else{
                // state transition
                int midx = (x1 + x2) / 2;
                int midy = (y1 + y2) / 2;
                offgraphics.setColor(FROMEDGECOLOR);
                offgraphics.drawLine(x1, y1, midx, midy);
                offgraphics.setColor(TOEDGECOLOR);
                offgraphics.drawLine(midx, midy, x2, y2);
            }
        }
        FontMetrics fm = offgraphics.getFontMetrics();
    	for (int i = 0; i < numEdges ; i++) {
            Edge e = (Edge)edges.get(i);
            paintNode(offgraphics, fm, e.from, e.isFinal);
    	}
        g.drawImage(offscreen, 0, 0, null);
    }

    public void paintNode(Graphics g, FontMetrics fm, Node n, boolean isAttractor) {

        int nodeSize = (int)(((float)n.attraction/numStates)*(MAXNODESIZE-MINNODESIZE))+MINNODESIZE;
        g.setColor((n==pick)? PICKCOLOR: (isAttractor? (n.isParasite? PARASITECOLOR: ATTRACTORCOLOR): NODECOLOR));
        g.fillOval ((int)(n.x-nodeSize/2), (int)(n.y-nodeSize/2), nodeSize, nodeSize);

        if ( labelVisible ) {
        	g.setColor(LABELCOLOR);
        	g.drawString(Long.toHexString(n.label), (int)n.x, (int)(n.y + fm.getHeight()/2));
        }
        if ( attractionVisible ) {
        	g.setColor(ATTRACTIONCOLOR);
            byte percent = (byte)(n.attraction  * 100 / numStates);
            byte fraction = (byte)(n.attraction * 10000 / numStates - percent * 100);
            g.drawString( n.attraction + "(" + percent + "." + fraction + "%)", (int)n.x, (int)(n.y + fm.getHeight()/2));
        }
    }

    //1.1 event handling
    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
        addMouseMotionListener(this);

    	double bestdist = (double)MOUSESENSITIVITY;
        int x = e.getX();
    	int y = e.getY();
        pick = null;
        for (int i = 0 ; i < numEdges ; i++) {
            Node n = ((Edge)edges.get(i)).from;
    	    double dist = (n.x - x) * (n.x - x) + (n.y - y) * (n.y - y);
            if (dist < bestdist) {
                pick = n;
                bestdist = dist;
            }
    	}
        if( pick != null ){
            pick.x = x;
            pick.y = y;
        }
        repaint();
    	e.consume();
    }

    public void mouseReleased(MouseEvent e) {
        removeMouseMotionListener(this);

        if (pick != null) {
            parent.getNetwork().setLongState(pick.label);
            parent.getNetDisplay ().setUnits(parent.getNetwork().getBooleanUnits());
            parent.getNetDisplay ().refreshUnitGrid ();
        }
        repaint();
    	e.consume();
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
        if( pick != null ){
            pick.x = e.getX();
            pick.y = e.getY();
            repaint();
        }
        e.consume();
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void start() {
        relaxer = new Thread(this);
        relaxer.start();
    }

    public void stop() {
        relaxer = null;
    }

    public void toggleLabel(){
    	labelVisible = !labelVisible;
        repaint();
    }

    public boolean isLabelVisible(){
    	return labelVisible;
    }

    public void toggleAttraction(){
    	attractionVisible = !attractionVisible;
        repaint();
    }

    public boolean isAttractionVisible(){
    	return attractionVisible;
    }

    public void setRefreshRate(byte rate){
    	refreshRate = rate;
		repaint();
    }

    public byte getRefreshRate(){
    	return refreshRate;
    }

}

