/**
* A class that implements the Simple Delta learning rule
* (local learning with equal fields).
* 
* @author Steve Hunt 
* @version 1.0
* 
* NOTE:
* The 'train' method (declared below) contains the code to implement
* LMS rule (aka simple Delta rule) learning with a learning rate of
* 1/N and a target error of 0.1.
*
*/

import java.lang.*;

public class LR04SimpleDelta extends AbstractLearningRule
{
	final static String PLUGIN_NAME = "LMS (rate=1/N max err=0.1)";

	//----------------------------------------

	//----------------------------------------
	public int train (Network network, TrainingSet trainingSet) {
        int iterationCount   = 0;
        int numUnits         = 0;
        int unitCount        = 0;
        int inputCount       = 0;
        int numPatterns      = 0;
        int patternCount     = 0;
        double currentWeight = 0;
        double newWeight     = 0;
        double weightDelta   = 0;
        int changes          = 1;
        
        double errTarget     = 0.1;
        double error         = 100;
        double unitError     = 0;
        
        int maxTimes         = 1000;
        double [] netField   = null;
        int [] thisPattern   = null;
        
        numUnits = network.numUnits ();

        double lrnRate  = 1.0 / numUnits;
        
        numPatterns = trainingSet.numPatterns ();
        netField = new double [numUnits];
        thisPattern = new int [numUnits];

        while ( (iterationCount < maxTimes) && (error > errTarget) )
        {
            error = 0.0;
            
            // for every pattern
            for (patternCount=0;patternCount<numPatterns;patternCount++)
            {
                // first get the pattern
                // for every unit
                for (unitCount=0;unitCount<numUnits;unitCount++)
                {
                    thisPattern [unitCount] = trainingSet.getUnit (patternCount, unitCount);
                }
                    
                // next work out the net field for each unit in the network for this pattern
                // for every unit
                for (unitCount=0;unitCount<numUnits;unitCount++)
                {
                    netField [unitCount] = 0.0;
                 
                    // for every input
                    for (inputCount=0;inputCount<numUnits;inputCount++)
                    {
                        // skip over self-connections
                        if (unitCount == inputCount) continue;
                
                        netField [unitCount]
                        = netField [unitCount]
                          + (  network.getWeight (unitCount, inputCount)
                             * thisPattern [inputCount] );
                    }
                }

                // now change the weights for this pattern
                // for every unit
                for (unitCount=0;unitCount<numUnits;unitCount++)
                {                
                    unitError = 1.0 - ( netField[unitCount] * thisPattern[unitCount] );
                    if (unitError == 0.0) continue;
                    //otherwise the unit needs to be trained, so
                    {
                        // for every input
                        for (inputCount=0;inputCount<numUnits;inputCount++)
                        {
                            // skip over self-connections
                            if (unitCount == inputCount) continue;

                            // retrieve the current weight for this unit/input pair
                            currentWeight = network.getWeight (unitCount, inputCount);

                            // calculate the weight change
                            weightDelta = unitError
                                        * thisPattern[unitCount]
                                        * thisPattern[inputCount]
                                        * lrnRate;

                            // calculate the new weight
                            newWeight = currentWeight + weightDelta;

                            // update the weight for this pair
                            network.setWeight (unitCount, inputCount, newWeight);
                        }
                        error = error + Math.abs(unitError);
                    }
                }
            }
            iterationCount++;
        }
        return iterationCount;
	}

	//----------------------------------------

	public int train
	  (Network network, TrainingSet trainingSet, int maxIterations, double learningRate, double target)
	{
        //---- This overloading of the 'train' method is included for future expansion only: do not use it
	    return 0;
	}

	//----------------------------------------

	public String getName ()
	{
		return PLUGIN_NAME;
	}
	
}