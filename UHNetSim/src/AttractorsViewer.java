


import java.awt.*;
import javax.swing.*;
import java.util.*;

public class AttractorsViewer extends JInternalFrame{

    AttractorsViewer(Dynamic dynamic){
        super("Attractors comparison", true, true, true, true);
        AttractorsPanel panel = new AttractorsPanel(dynamic);

        this.setContentPane(panel);
        this.setSize(panel.DEFAULTWIDTH, panel.DEFAULTHEIGHT);
        this.setLocation(0, 0);
        this.setVisible(false);
     }
}

class AttractorsPanel extends JPanel{

    Dynamic dynamic          = null;
    int[][] summary          = null;
    static int DEFAULTWIDTH  = 500;
    static int DEFAULTHEIGHT = 300;
    int MaxCircleWidth       = 200;
    Color[] reds  = { new Color(200,0,0), new Color(225,0,0)
                    , new Color(255,0,0), new Color(255,75,75)
                    , new Color(255,100,100), new Color(255,125,125)
                    , new Color(255,150,150), new Color(255,175,175)
                    , new Color(255,200,200), new Color(255,225,225)};
    Color[] blues = { new Color(0,0,125), new Color(0,0,175)
                    , new Color(0,0,255), new Color(50,50,255)
                    , new Color(75,75,255), new Color(100,100,255)
                    , new Color(125,125,255), new Color(150,150,255)
                    , new Color(175,175,255),new Color(200,200,255)};
    int maxAttraction        = 0;
    int currentX             = 0;
    int currentY             = 0;
    int nextY                = 0;

    AttractorsPanel (Dynamic dynamic){
        this.dynamic = dynamic;
        summary = new int[dynamic.memories.size()][Dynamic.MAXEPOCH+1];

        int i = 0;
        for (Enumeration e = dynamic.memories.elements(); e.hasMoreElements(); i++){
            Memory m = (Memory)e.nextElement();
            for(int j = 0; j <= Dynamic.MAXEPOCH; j++){
                summary[i][j] = m.summary[j];
            }
            summary[i][0] *= (m.isParasite? -1: 1);
        }
        sort();
    }

    private void sort(){
        for(int i = summary.length; --i>=0; ){
            for(int j = 0; j < i; j++){
                if(Math.abs(summary[j][0]) < Math.abs(summary[j+1][0])){
                    for(int n = 0; n <= dynamic.MAXEPOCH; n++){
                        int temp = summary[j][n];
                        summary[j][n] = summary[j+1][n];
                        summary[j+1][n] = temp;
                    }
                }
            }
        }
    }

    public void paint(Graphics g){

        int diameter = MaxCircleWidth;
        Dimension d  = getSize();

        g.setColor(Color.white);
        g.fillRect (0, 0, d.width, d.height);

        currentX = currentY = 10;
        nextY = diameter + 20;
        maxAttraction = Math.abs(summary[0][0]);

        for(int i = 0; i < summary.length; i++){
            diameter = (int)(((float)Math.abs(summary[i][0])/maxAttraction) * MaxCircleWidth);
            if( (d.width - 10 - currentX) < diameter ){
                currentX = 10;
                currentY = nextY;
                nextY += (diameter + 10);
            }
            drawAttractor(g, i, diameter);
            currentX += (diameter + 10);
        }
    }

    public void drawAttractor(Graphics g, int i, int diameter){
        int attraction =  Math.abs(summary[i][0]);
        Color[] colors;
        if(summary[i][0] < 0){ colors = blues; }
        else{ colors = reds; }

        g.setColor(colors[9]);
        g.fillOval(currentX, currentY, diameter, diameter);

        for(int j = dynamic.MAXEPOCH; j > 1; j--){
            attraction -= summary[i][j];
            int smallD = (int)(((float)attraction / maxAttraction) * MaxCircleWidth);
            int x = currentX + (diameter-smallD)/2 ;
            int y = currentY + (diameter-smallD)/2 ;
            g.setColor(colors[j-2]);
            g.fillOval(x, y, smallD, smallD);
        }
    }

}
