


import java.lang.*;
import java.util.*;

public class LongArray{

    static final int INITIALCAPACITY = 256;
    static final byte INCREMENTFACTOR = 2;
    static final int MAXFACTOR = 1048576;
    long[] elements = null;
    LongArray nextArray = null;
    LongArray lastArray = null;
    int size;
    int firstIndex;
    int lastIndex;

    public LongArray(){
        this(INITIALCAPACITY);
    }

    public LongArray(int capacity){
        this(capacity, 0);
    }

    public LongArray(int capacity, int firstIndex){
        this.size = capacity;
        this.firstIndex = firstIndex;
        this.lastIndex = 0;
        elements = new long[capacity];
        lastArray = this;
        System.out.println("LargeArray : " + firstIndex + " + " + size );
    }

    public void add(long entry){
        if( lastArray.lastIndex == lastArray.size && lastArray.nextArray == null){
            int nextIncrement = ( lastArray.size * INCREMENTFACTOR );
            nextIncrement = (nextIncrement > MAXFACTOR ? MAXFACTOR: nextIncrement);
            lastArray.nextArray = new LongArray(nextIncrement, lastArray.firstIndex + lastArray.size);
            this.lastArray = lastArray.nextArray;
        }
        //System.out.println("add : " + lastArray.firstIndex + " + " + lastArray.lastIndex);
        lastArray.elements[lastArray.lastIndex++] = entry;
    }

    public long get(int index){
        if ( index < firstIndex + size ) {
            return elements[index - firstIndex];
        }else{
            return nextArray.get(index);
        }
    }

}
