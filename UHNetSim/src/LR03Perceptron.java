/**
 * A class that implements the Perceptron learning rule.
 * 
 * @author Steve Hunt 
 * @version 1.0
 * 
 * NOTE:
 * The 'train' method (declared below) contains the code to implement
 * Perceptron learning with a learning rate of 1/N and a training
 * threshold of 10.
 *
 */

public class LR03Perceptron extends AbstractLearningRule {
	final static String PLUGIN_NAME = "Perceptron (rate=1/N T=10)";

	//----------------------------------------
	public int train (Network network, TrainingSet trainingSet) {
        int iterationCount   = 0;
        int numUnits         = 0;
        int unitCount        = 0;
        int inputCount       = 0;
        int numPatterns      = 0;
        int patternCount     = 0;
        double currentWeight = 0;
        double newWeight     = 0;
        double weightDelta   = 0;
        int changes          = 1;
        
        double trnThreshold  = 10;
        int maxTimes         = 1000;
        double [] netField   = null;
        int [] thisPattern   = null;
        
        numUnits = network.numUnits ();

        double lrnRate = 1.0 / numUnits;

        numPatterns = trainingSet.numPatterns ();
        netField = new double [numUnits];
        thisPattern = new int [numUnits];

        // for every pattern
        while ( (iterationCount < maxTimes) && (changes > 0) )
        {
            changes = 0;
            
            for (patternCount=0;patternCount<numPatterns;patternCount++)
            {
                // first work out the net field for each unit in the network for this pattern
                // for every unit
                for (unitCount=0;unitCount<numUnits;unitCount++)
                {
                    thisPattern [unitCount] = trainingSet.getUnit (patternCount, unitCount);
                }
                    
                for (unitCount=0;unitCount<numUnits;unitCount++)
                {
                    netField [unitCount] = 0.0;
                 
                    // for every input
                    for (inputCount=0;inputCount<numUnits;inputCount++)
                    {
                        // skip over self-connections
                        if (unitCount == inputCount) continue;
                
                        netField [unitCount]
                        = netField [unitCount]
                          + (  network.getWeight (unitCount, inputCount)
                             * thisPattern [inputCount] );
                    }
                }

                // now change the weights for this pattern
                // for every unit
                for (unitCount=0;unitCount<numUnits;unitCount++)
                {                
                    if ( ( netField[unitCount] * thisPattern[unitCount] ) < trnThreshold )
                    //the unit needs to be trained, so
                    {
                        // for every input
                        for (inputCount=0;inputCount<numUnits;inputCount++)
                        {
                            // skip over self-connections
                            if (unitCount == inputCount) continue;

                            // retrieve the current weight for this unit/input pair
                            currentWeight = network.getWeight (unitCount, inputCount);

                            // calculate the weight change
                            weightDelta = thisPattern[unitCount]
                                        * thisPattern[inputCount]
                                        * lrnRate;

                            // calculate the new weight
                            newWeight = currentWeight + weightDelta;

                            // update the weight for this pair
                            network.setWeight (unitCount, inputCount, newWeight);
                        }
                        changes = changes + 1;
                    }
                }
            }
            iterationCount++;
        }
        return iterationCount;
	}
	
	public int train
	  (Network network, TrainingSet trainingSet, int maxIterations, double learningRate, double target)
	{
        //---- This overloading of the 'train' method is included for future expansion only: do not use it
	    return 0;
	}
	   

	//----------------------------------------
	public String getName () {
		return PLUGIN_NAME;
	}

}
