

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class NetWizard extends JInternalFrame implements ActionListener {
	// panel position constants
    final static int CARD0 = 0; // load network or start new
	final static int CARD1 = 1; // load existing network
	final static int CARD2 = 2; // choose network size and type
	final static int CARD3 = 3; // load training set or start new
	final static int CARD4 = 4; // load existing training set

    // pattern type constants
    final static int BIPOLAR = 0;
    final static int BINARY  = 1;

    // action constants
    final static int BEGINNEW     = 0;
    final static int LOADEXISTING = 1;

    private NavPanel navPanel     = null;
    private JPanel cardContainer  = null;

    // CARD0
    private boolean newNetwork    = true;

    // CARD1
    private JTextField txfNetworkFileLocation = null;
    private JFileChooser networkFileChooser   = null;
    private File networkFile                  = null;

    // CARD2
    private NumericField networkSize          = null;
    private int defaultNetworkSize            = 100;
    private int numUnits                      = 0;
    private JComboBox learningRuleChooser     = null;
    private AbstractLearningRule learningRule = null;

    //private JComboBox patternTypeChooser      = null;
    //private String[] patternTypes             = {"Bipolar", "Binary"};
    private int patternType                   = NetWizard.BIPOLAR;

    // CARD3
    private boolean newTrainingSet = true;

    // CARD4
    private JTextField txfTrainingSetFileLocation = null;
    private JFileChooser trainingSetFileChooser   = null;
    private File trainingSetFile                  = null;

    // other
    private String[] cardNames  = {"CARD0", "CARD1", "CARD2", "CARD3", "CARD4"};
    private String[] classNames = null;
    private String[] ruleNames  = null;
    private int currentCard     = NetWizard.CARD0;
    private int nextCard        = NetWizard.CARD1;
    private boolean finished    = false;
    private NetSimFrame parent  = null;

    //-----------------------------------
    public NetWizard (NetSimFrame parent) {
        super ("Network wizard", false, false, false, true);

        this.parent = parent;
        this.setVisible (false);
        this.findTrainingClasses ();
        this.initNavPanel ();
        this.initCards ();
        this.setSize (400, 200);
    }

    //-----------------------------------
    public boolean hasFinished () {
        return this.finished;
    }

    //-----------------------------------
    private void findTrainingClasses () {
        String[] fileNames  = null;
		AbstractLearningRule learningRule = null;
		LearningRuleLoader loader     = null;
		int fileCount       = 0;

		try {
            loader = new LearningRuleLoader(System.getProperty("user.dir"));
        } catch (FileNotFoundException e) {
        	Thread.dumpStack ();
        	System.exit (2);
        }

        // get all the class files in this directory
		fileNames = loader.findLearningRules ();
        this.classNames = new String[fileNames.length];
        this.ruleNames = new String[fileNames.length];

		// load the classes, one at a time
		for (fileCount=0;fileCount<fileNames.length;fileCount++) {
            learningRule = loader.loadLearningRule (fileNames[fileCount]);

			if (learningRule != null) {
                this.ruleNames[fileCount] = learningRule.getName ();
                this.classNames[fileCount] = learningRule.getClass ().getName ();
            }
		}
    }

    //-----------------------------------
    private void initNavPanel () {
        this.navPanel = new NavPanel ();
        this.navPanel.addActionListener (this);
        this.navPanel.setBtnBackState (false);
        this.getContentPane ().add (this.navPanel, BorderLayout.SOUTH);
    }

    //-----------------------------------
    public void actionPerformed (ActionEvent e) {
        // find out what the action was
        if (this.navPanel.getAction () == NavPanel.NEXT) {
            // determine the next card
            switch (this.currentCard) {
                case NetWizard.CARD0:
                    if (this.newNetwork == true) {
                        this.nextCard = NetWizard.CARD2;
                    } else {
                        this.nextCard = NetWizard.CARD1;
                    }
                    break;
                case NetWizard.CARD2:   // choose network size and type
                    this.processCard2 ();
                    break;
                default:
                    this.nextCard = this.currentCard + 1;
            }
        } else if (this.navPanel.getAction () == NavPanel.BACK) {
            switch (this.currentCard) {
                case NetWizard.CARD2:
                    this.nextCard = NetWizard.CARD0;
                    break;
                default:
                    this.nextCard = this.currentCard - 1;
            }
        }

        if (this.navPanel.getAction () == NavPanel.NEXT || this.navPanel.getAction () == NavPanel.BACK) {
            this.updateCards ();
        }
    }

    //-----------------------------------
    public void updateCards () {
        // set the buttons' state and text according to where we are
        if (this.nextCard == NetWizard.CARD0) {
            this.navPanel.setBtnNextType (NavPanel.NEXT);
            this.navPanel.setBtnNextText ("Next >>");
            this.navPanel.setBtnBackState (false);
            this.navPanel.setBtnNextState (true);
        } else if (this.nextCard == NetWizard.CARD1) {
            this.navPanel.setBtnNextType (NavPanel.FINISH);
            this.navPanel.setBtnNextText ("Finish");
            this.navPanel.setBtnBackState (true);
            this.navPanel.setBtnNextState (false);
        } else if (this.nextCard == NetWizard.CARD4) {
            this.navPanel.setBtnNextType (NavPanel.FINISH);
            this.navPanel.setBtnNextText ("Finish");
            this.navPanel.setBtnBackState (true);
            this.navPanel.setBtnNextState (false);
        } else if (this.nextCard == NetWizard.CARD3 && this.newTrainingSet == true) {
            this.navPanel.setBtnNextType (NavPanel.FINISH);
            this.navPanel.setBtnNextText ("Finish");
            this.navPanel.setBtnBackState (true);
            this.navPanel.setBtnNextState (true);
        } else {
            this.navPanel.setBtnNextType (NavPanel.NEXT);
            this.navPanel.setBtnNextText ("Next >>");
            this.navPanel.setBtnBackState (true);
            this.navPanel.setBtnNextState (true);
        }

        ((CardLayout)this.cardContainer.getLayout ()).show (this.cardContainer, this.cardNames[this.nextCard]);

        // make the new card current
        this.currentCard = this.nextCard;
    }

    //-----------------------------------
    private void initCards () {
        this.cardContainer = new JPanel (new CardLayout ());
        this.networkFileChooser = new JFileChooser ();
        this.trainingSetFileChooser = new JFileChooser ();

        this.cardContainer.add (this.initCard0 (), this.cardNames[NetWizard.CARD0]);
        this.cardContainer.add (this.initCard1 (), this.cardNames[NetWizard.CARD1]);
        this.cardContainer.add (this.initCard2 (), this.cardNames[NetWizard.CARD2]);
        this.cardContainer.add (this.initCard3 (), this.cardNames[NetWizard.CARD3]);
        this.cardContainer.add (this.initCard4 (), this.cardNames[NetWizard.CARD4]);

        this.getContentPane ().add (this.cardContainer, BorderLayout.CENTER);
    }

    //-----------------------------------
    private JPanel initCard0 () {
        JPanel card0                          = new JPanel ();
        GridBagLayout gridBagLayout           = new GridBagLayout ();
        GridBagConstraints gridBagConstraints = new GridBagConstraints ();
        ButtonGroup buttonGroup               = new ButtonGroup ();
        JRadioButton btnNew                   = new JRadioButton ("Begin a new network", true);
        JRadioButton btnLoad                  = new JRadioButton ("Load an existing network", false);

        // set up the actions for the radio buttons
    	btnLoad.addActionListener (new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
                newNetwork = false;
                newTrainingSet = false;
            }
	    });

    	btnNew.addActionListener (new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
                newNetwork = true;
                newTrainingSet = true;
            }
	    });

        btnNew.setSelected (true);
        buttonGroup.add (btnNew);
        buttonGroup.add (btnLoad);

        card0.setLayout (gridBagLayout);
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = GridBagConstraints.WEST;

        card0.add (btnNew, gridBagConstraints);
        card0.add (btnLoad, gridBagConstraints);

        return card0;
    }

    //-----------------------------------
    private JPanel initCard1 () {
        JPanel card1                          = new JPanel ();
        GridBagLayout gridBagLayout           = new GridBagLayout ();
        GridBagConstraints gridBagConstraints = new GridBagConstraints ();
        JButton btnBrowse                     = new JButton ("...");

        this.txfNetworkFileLocation = new JTextField ("", 15);

        btnBrowse.setMargin (new Insets (0, 0, 0, 0));
        // add tooltip to button
    	btnBrowse.addActionListener (new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
                if (networkFileChooser.showOpenDialog (null) == JFileChooser.APPROVE_OPTION) {
                    txfNetworkFileLocation.setText (networkFileChooser.getSelectedFile ().getName ());
                    networkFile = networkFileChooser.getSelectedFile ();
                    navPanel.setBtnNextState (true);
                }
            }
	    });

        card1.setLayout (gridBagLayout);
        card1.add (new JLabel ("Network file: "), gridBagConstraints);
        card1.add (this.txfNetworkFileLocation, gridBagConstraints);

        card1.add (btnBrowse, gridBagConstraints);

        return card1;
    }

    //-----------------------------------
    private JPanel initCard2 () {
        JPanel card2                          = new JPanel ();
        GridBagLayout gridBagLayout           = new GridBagLayout ();
        GridBagConstraints gridBagConstraints = new GridBagConstraints ();

        // card components
        this.networkSize = new NumericField (3);
        this.learningRuleChooser = new JComboBox (this.ruleNames);
/*
        this.patternTypeChooser = new JComboBox (this.patternTypes);
        this.patternTypeChooser.setSelectedIndex (this.patternType);
        this.patternTypeChooser.setEnabled(false);
*/
        card2.setLayout (gridBagLayout);

        gridBagConstraints.insets = new Insets (5, 0, 5, 0);
        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        gridBagConstraints.anchor = GridBagConstraints.WEST;
        card2.add (new JLabel ("Network size:                     "), gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        card2.add (this.networkSize, gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        card2.add (new JLabel ("Training rule:                    "), gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        card2.add (this.learningRuleChooser, gridBagConstraints);
/*
        gridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
        card2.add (new JLabel ("Pattern type:                     "), gridBagConstraints);

        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        card2.add (this.patternTypeChooser, gridBagConstraints);
*/
        return card2;
    }

    //-----------------------------------
    private JPanel initCard3 () {
        JPanel card3                          = new JPanel ();
        GridBagLayout gridBagLayout           = new GridBagLayout ();
        GridBagConstraints gridBagConstraints = new GridBagConstraints ();
        ButtonGroup buttonGroup               = new ButtonGroup ();
        JRadioButton btnNew                   = new JRadioButton ("Create a new training set", true);
        JRadioButton btnLoad                  = new JRadioButton ("Load an existing training set", false);

        // set up the actions for the radio buttons
        btnNew.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent e) {
                newTrainingSet = true;
                navPanel.setBtnNextType (NavPanel.FINISH);
                navPanel.setBtnNextText ("Finish");
            }
        });

    	btnLoad.addActionListener (new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
                newTrainingSet = false;
                navPanel.setBtnNextType (NavPanel.NEXT);
                navPanel.setBtnNextText ("Next >>");
                nextCard = NetWizard.CARD4;
            }
	    });

        btnNew.setSelected (true);
        buttonGroup.add (btnNew);
        buttonGroup.add (btnLoad);

        card3.setLayout (gridBagLayout);

        gridBagConstraints.insets = new Insets (5, 0, 5, 0);
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = GridBagConstraints.WEST;

        card3.add (btnNew, gridBagConstraints);
        card3.add (btnLoad, gridBagConstraints);

        return card3;
    }

    //-----------------------------------
    private JPanel initCard4 () {
        JPanel card4                          = new JPanel ();
        GridBagLayout gridBagLayout           = new GridBagLayout ();
        GridBagConstraints gridBagConstraints = new GridBagConstraints ();
        JButton btnBrowse                     = new JButton ("...");

        this.txfTrainingSetFileLocation = new JTextField ("", 15);

        btnBrowse.setMargin (new Insets (0, 0, 0, 0));
        // add tooltip to button
    	btnBrowse.addActionListener (new ActionListener () {
		    public void actionPerformed (ActionEvent e) {
                if (trainingSetFileChooser.showOpenDialog (null) == JFileChooser.APPROVE_OPTION) {
                    txfTrainingSetFileLocation.setText (trainingSetFileChooser.getSelectedFile ().getName ());
                    trainingSetFile = trainingSetFileChooser.getSelectedFile ();
                    navPanel.setBtnNextState (true);
                }
            }
	    });

        card4.setLayout (gridBagLayout);
        card4.add (new JLabel ("Training set file: "), gridBagConstraints);
        card4.add (this.txfTrainingSetFileLocation, gridBagConstraints);

        card4.add (btnBrowse, gridBagConstraints);

        return card4;
    }

    //-----------------------------------
    private void processCard2 () {
        LearningRuleLoader loader = null;

        try {
        	loader = new LearningRuleLoader (".");
        } catch (FileNotFoundException e) {
        	Thread.dumpStack ();
        	System.exit (2);
        }

        // load the requested learning rule
        this.learningRule = (AbstractLearningRule)loader.loadLearningRule (this.classNames[this.learningRuleChooser.getSelectedIndex ()] + ".class");

        if (this.learningRule == null) {
            System.out.println ("Chosen learning rule is invalid in Netwizard.processCard2 ()");
            Thread.dumpStack ();
            System.exit (2);
        }

        // validate the network size, must be non-zero and non-empty
        if (this.networkSize.getText ().compareTo ("") != 0 && this.networkSize.getText ().compareTo ("0") != 0) {
            // must be even
            this.numUnits = Integer.parseInt (this.networkSize.getText ());
            if ( validateSize() ) {
                this.nextCard = NetWizard.CARD3;
                navPanel.setBtnNextType (NavPanel.FINISH);
                navPanel.setBtnNextText ("Finish");
            } else {
                this.nextCard = NetWizard.CARD2;
                JOptionPane.showMessageDialog (this, "Network size must be even.");
            }
        } else {
            this.nextCard = NetWizard.CARD2;
            JOptionPane.showMessageDialog (this, "Network size must be completed and non-zero.");
        }
        // set the pattern type
        //this.patternType = this.patternTypeChooser.getSelectedIndex ();
    }

    //-----------------------------------
    private boolean validateSize(){
        boolean valid = true;

        //9 units (3x3 grid) and 25 units (5x5 grid) seem perfectly acceptable.
        //valid = this.numUnits % 2 == 0;

        valid = this.numUnits > 0;

        return valid;
    }

    //-----------------------------------
    public boolean finishWizard () {
        // create the network if required
        if (this.newNetwork == true) {
            this.parent.getNetwork ().buildNetwork (this.numUnits);
            this.parent.getNetwork ().setPatternType (this.patternType);
            this.parent.getNetwork ().clearCurrentState ();
            this.parent.getNetwork ().setLearningRule (this.learningRule);

            // create the training set if required
            if (this.newTrainingSet == true) {
                this.parent.getTrainingSet ().setPatternWidth (this.numUnits);
                this.parent.getTrainingSet ().setPatternType (this.patternType);
                this.parent.getTrainingSet ().addPattern ();
            } else {
                if (this.loadTrainingSet () != TrainingSet.LOAD_SUCCESS) return false;
            }

        } else {
            if (this.loadNetwork () != Network.LOAD_SUCCESS) return false;
            this.trainingSetFile = this.parent.getNetwork ().getTrainingSetFile ();
            if (this.loadTrainingSet () != TrainingSet.LOAD_SUCCESS) return false;
            // set the pattern type of the network according to that of the training set
            this.parent.getNetwork ().setPatternType (this.parent.getTrainingSet ().getPatternType ());
        }

        // doesn't load test set at the first place
        this.parent.getTestSet ().setPatternWidth (parent.getTrainingSet().getPatternWidth() );
        this.parent.getTestSet ().setPatternType ( parent.getTrainingSet().getPatternType());
        this.parent.getTestSet ().addPattern ();

        return true;
    }

    //-----------------------------------
    private int loadNetwork () {
        int returnVal = 0;

        returnVal = this.parent.getNetwork ().loadNetwork (this.networkFile);

        switch (returnVal) {
            case Network.FILE_NOT_FOUND:
                this.nextCard = NetWizard.CARD1;
                this.txfNetworkFileLocation.setText ("");
                JOptionPane.showMessageDialog (this, "Network file not found", "Loading network", JOptionPane.ERROR_MESSAGE);
                break;
            case Network.NO_TRAINING_SET:
                this.nextCard = NetWizard.CARD1;
                this.txfNetworkFileLocation.setText ("");
                JOptionPane.showMessageDialog (this, "Training set not specified in file", "Loading network", JOptionPane.ERROR_MESSAGE);
                break;
           case Network.NO_TRAINING_RULE:
                this.nextCard = NetWizard.CARD1;
                this.txfNetworkFileLocation.setText ("");
                JOptionPane.showMessageDialog (this, "Training rule not specified in file", "Loading network", JOptionPane.ERROR_MESSAGE);
                break;
            case Network.INVALID_TRAINING_RULE:
                this.nextCard = NetWizard.CARD1;
                this.txfNetworkFileLocation.setText ("");
                JOptionPane.showMessageDialog (this, "Invalid training rule in file", "Loading network", JOptionPane.ERROR_MESSAGE);
                break;
             case Network.NO_SIZE:
                this.nextCard = NetWizard.CARD1;
                this.txfNetworkFileLocation.setText ("");
                JOptionPane.showMessageDialog (this, "Network size not specified in file", "Loading network", JOptionPane.ERROR_MESSAGE);
                break;
            case Network.INVALID_SIZE:
                this.nextCard = NetWizard.CARD1;
                this.txfNetworkFileLocation.setText ("");
                JOptionPane.showMessageDialog (this, "Specified file size was invalid", "Loading network", JOptionPane.ERROR_MESSAGE);
                break;
            case Network.INVALID_WEIGHT:
                this.nextCard = NetWizard.CARD1;
                this.txfNetworkFileLocation.setText ("");
                JOptionPane.showMessageDialog (this, "Weights contain invalid value", "Loading network", JOptionPane.ERROR_MESSAGE);
                break;
            case Network.PREMATURE_END:
                this.nextCard = NetWizard.CARD1;
                this.txfNetworkFileLocation.setText ("");
                JOptionPane.showMessageDialog (this, "Not enough weights in file", "Loading network", JOptionPane.ERROR_MESSAGE);
                break;
            case Network.IO_EXCEPTION:
                this.nextCard = NetWizard.CARD1;
                this.txfNetworkFileLocation.setText ("");
                JOptionPane.showMessageDialog (this, "I/O exception occurred, reason unknown", "Loading network", JOptionPane.ERROR_MESSAGE);
                break;
            default:
        }
        return returnVal;
    }

    //-----------------------------------
    private int loadTrainingSet () {
        int returnVal = 0;

        returnVal = this.parent.getTrainingSet ().loadTrainingSet (this.trainingSetFile);

        switch (returnVal) {
            case TrainingSet.FILE_NOT_FOUND:
                if (this.newNetwork == true) {
                    this.nextCard = NetWizard.CARD4;
                    this.txfTrainingSetFileLocation.setText ("");
                } else {
                    this.nextCard = NetWizard.CARD1;
                    this.txfNetworkFileLocation.setText ("");
                }
                JOptionPane.showMessageDialog (this, "Training set file not found", "Loading training set", JOptionPane.ERROR_MESSAGE);
                break;
            case TrainingSet.NO_PATTERN_SIZE:
                if (this.newNetwork == true) {
                    this.nextCard = NetWizard.CARD4;
                    this.txfTrainingSetFileLocation.setText ("");
                } else {
                    this.nextCard = NetWizard.CARD1;
                    this.txfNetworkFileLocation.setText ("");
                }
                JOptionPane.showMessageDialog (this, "Training pattern size not found in file", "Loading training set", JOptionPane.ERROR_MESSAGE);
                break;
            case TrainingSet.NO_BIT_FOUND:
                if (this.newNetwork == true) {
                    this.nextCard = NetWizard.CARD4;
                    this.txfTrainingSetFileLocation.setText ("");
                } else {
                    this.nextCard = NetWizard.CARD1;
                    this.txfNetworkFileLocation.setText ("");
                }
                JOptionPane.showMessageDialog (this, "Training pattern bit not found", "Loading training set", JOptionPane.ERROR_MESSAGE);
                break;
            case TrainingSet.INVALID_BIT_VALUE:
                if (this.newNetwork == true) {
                    this.nextCard = NetWizard.CARD4;
                    this.txfTrainingSetFileLocation.setText ("");
                } else {
                    this.nextCard = NetWizard.CARD1;
                    this.txfNetworkFileLocation.setText ("");
                }
                JOptionPane.showMessageDialog (this, "Training pattern bit has incorrect value", "Loading training set", JOptionPane.ERROR_MESSAGE);
                break;
            case TrainingSet.PREMATURE_END:
                if (this.newNetwork == true) {
                    this.nextCard = NetWizard.CARD4;
                    this.txfTrainingSetFileLocation.setText ("");
                } else {
                    this.nextCard = NetWizard.CARD1;
                    this.txfNetworkFileLocation.setText ("");
                }
                JOptionPane.showMessageDialog (this, "Pattern/s incorrect length", "Loading training set", JOptionPane.ERROR_MESSAGE);
                break;
            case TrainingSet.IO_EXCEPTION:
                if (this.newNetwork == true) {
                    this.nextCard = NetWizard.CARD4;
                    this.txfTrainingSetFileLocation.setText ("");
                } else {
                    this.nextCard = NetWizard.CARD1;
                    this.txfNetworkFileLocation.setText ("");
                }
                JOptionPane.showMessageDialog (this, "I/O exception occurred, reason unknown", "Loading training set", JOptionPane.ERROR_MESSAGE);
                break;
            default:
                // check the pattern type
                if ((this.parent.getTrainingSet ().getPatternType () != this.parent.getNetwork ().getPatternType ()) && (this.parent.getNetwork ().getPatternType () != Network.NOT_KNOWN)) {
                    if (this.newNetwork == true) {
                        this.nextCard = NetWizard.CARD4;
                        this.txfTrainingSetFileLocation.setText ("");
                    } else {
                        this.nextCard = NetWizard.CARD1;
                        this.txfNetworkFileLocation.setText ("");
                    }
                    JOptionPane.showMessageDialog (this, "Specified training contains patterns of the wrong type for this network", "Loading training set", JOptionPane.ERROR_MESSAGE);
                    // this training set is invalid so clear it out
                    this.parent.getTrainingSet ().clear ();
                    return TrainingSet.IO_EXCEPTION;
                }
                // check the width of the patterns
                if (this.parent.getTrainingSet ().getPatternWidth () != this.parent.getNetwork ().numUnits ()) {
                    if (this.newNetwork == true) {
                        this.nextCard = NetWizard.CARD4;
                        this.txfTrainingSetFileLocation.setText ("");
                    } else {
                        this.nextCard = NetWizard.CARD1;
                        this.txfNetworkFileLocation.setText ("");
                    }
                    JOptionPane.showMessageDialog (this, "Training set patterns and network are different sizes", "Loading training set", JOptionPane.ERROR_MESSAGE);
                    // this training set is invalid so clear it out
                    this.parent.getTrainingSet ().clear ();
                    returnVal = TrainingSet.IO_EXCEPTION;
                }
        }
        return returnVal;
    }

    //-----------------------------------
    public NavPanel getNavPanel () {
        return this.navPanel;
    }
}
