

import Utils.Assertion;
import Utils.Print;

public class NetSim{
    private static NetworkType network = NetworkType.h;
    private static String input = null, output = null;
    private static int size = 5, runs = 1, pattern = 0, numPatterns = 0;
    private static float bias = 0.5f;
    private static boolean duplicates = false;
    
    public enum Arg{
        a(0),
        b(1),
        c(0),
        d(0),
        h(0),
        n(1),
        p(1),
        r(1),
        s(1),
        z(1);
        
        private final int numArgs;
        
        Arg(int numArgs){ this.numArgs = numArgs; }
        
        int getNumArgs(){ return numArgs; }
    }
    
    public enum NetworkType{
        l("Hopfield"),
        p("Hopfield"),
        s("Perceptron"),
        h("Perceptron");
        
        private final String type;
        
        NetworkType(String type){ this.type = type; }
        
        String getType(){ return type; }
    }
    
    public static void main(String[] args){
        NetSimFrame app = null;
        app = new NetSimFrame();   // create an instance of the application
        if(args.length>0){ //Run terminal data collection version
            //###################################
            Print.debug("Converting arguments");
            for(int x=0; x<args.length; x++){
                String arg = args[x];
                if(arg.charAt(0)=='-'){ //Arg
                    Arg command = Arg.h;
                    try{ command = Arg.valueOf(arg.substring(1)); }
                    catch(IllegalArgumentException e){ Print.err("Invalid Argument detected"); }
                    switch(command){
                        case a :    Print.debug("Setting assert mode");
                                    Print.println("Assertion mode set.");
                                    Assertion.setAssert(true); //Make sure that assertions are supposed to be set!
                            break;
                        case b :    Print.debug("Setting pattern bias");
                                    try{ bias = Float.parseFloat(args[x + 1]); }
                                    catch(NumberFormatException e){ Print.err("Invalid float"); }
                            break;
                        case c :    Print.debug("Setting 'allow duplicate patterns' on");
                                    duplicates = true;
                            break;
                        case d :    Print.debug("Setting debug mode");
                                    Print.setDebug(true);
                                    Print.debug("Debug mode set");
                            break;
                        case n :    Print.debug("Setting network type");
                                    try{ network = NetworkType.valueOf(args[x + 1]); }
                                    catch(IllegalArgumentException e){
                                        command = Arg.h;
                                        Print.err("Invalid Argument detected");
                                    }catch(ArrayIndexOutOfBoundsException e){ Print.err("Invalid Argument detected"); }
                            break;
                        case p :    Print.debug("Setting pattern type");
                                    try{ pattern = Integer.parseInt(args[x + 1]); }
                                    catch(NumberFormatException e){ Print.err("Invalid integer"); }
                            break;
                        case r :    Print.debug("Setting number of runs");
                                    try{
                                        runs = Integer.parseInt(args[x + 1]);
                                        if(runs<0)Print.err("Integer < 0");
                                    }catch(NumberFormatException e){ Print.err("Invalid integer"); }
                            break;
                        case s :    Print.debug("Setting network size");
                                    try{
                                        size = Integer.parseInt(args[x + 1]);
                                        if(size<1)Print.err("Integer < 1");
                                    }catch(NumberFormatException e){ Print.err("Invalid integer"); }
                            break;
                        case z :    Print.debug("Setting number of patterns to test");
                                    try{ numPatterns = Integer.parseInt(args[x + 1]); }
                                    catch(NumberFormatException e){ Print.err("Invalid integer"); }
                            break;
                        case h :
                        default :   Print.debug("Printing help");
                                    Print.println("");
                                    Print.println("uhnetsim [OPTION]... [OUTPUT]");
                                    Print.println("[OPTION]s are as follows:");
                                    Print.println("  -a  assertion mode");
                                    Print.println("  -b  pattern bias");
                                    Print.println("  -c  pattern duplicates allowed mode");
                                    Print.println("  -d  set debug mode");
                                    Print.println("  -h  this help message");
                                    Print.println("  -n  network type:");
                                    Print.println("    h -> hopfield");
                                    Print.println("    s -> storkey");
                                    Print.println("    p -> perceptron");
                                    Print.println("    l -> lms");
                                    Print.println("  -p  pattern type:");
                                    Print.println("    0 -> bipolar");
                                    Print.println("    1 -> binary");
                                    Print.println("  -r  set runs");
                                    Print.println("    integer >= 0");
                                    Print.println("  -s  network size");
                                    Print.println("    integer > 0");
                                    Print.println("  -z  patterns to learn");
                                    Print.println("");
                                    Print.println("[OUTPUT] is optional and will save the network's ability to learn");
                                    Print.println("");
                                    Print.debug("Exiting");
                                    System.exit(0);
                    }
                    x += command.getNumArgs();
                }else{
                    if(output==null)output = arg;
                    else Print.err("Unknown parameter.");
                }
            }

            //###################################
            Print.debug("Checking Arguments for NetSim");
            //Test input if asertion mode has been set
            Assertion.test(Assertion.getStatus()); //Are we supposed to be printing assertions?
            Assertion.test(network!=null); //Check to see if we have a network type
            
            //###################################
            Print.debug("Passing information to NetSimIO");
            Print.debug("Output file " + (output==null ? "not " : "") + "passed");
            NetSimIO netSimIO = new NetSimIO(app, network, output, size, runs, pattern, numPatterns, bias, duplicates);
        }else{ //Display GUI version
            app.go();
        }
    }
}