


import javax.swing.*;

public class OutputFrame extends JInternalFrame{

    NetSimFrame parent = null;
    JTextArea output   = null;

    OutputFrame(NetSimFrame parent){
        super("Output Window", true, false, true, true);
        this.parent = parent;

        output = new JTextArea(10, 25);
        JScrollPane scroll = new JScrollPane(output);
        setContentPane(scroll);
        setSize(300, 250);
        setLocation(500, 0);
        parent.addChildFrame(this);
        setVisible(false);
    }

    public void clear(){
        output.setText ("");
    }

    public void setText(String text){
        output.setText(text);
    }

    public void appendText(String text){
        output.append(text);
    }

    public boolean save(String path){
        return true;
    }

}
