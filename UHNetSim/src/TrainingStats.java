

public class TrainingStats {
    private String learningRule = null;
    private int numPatterns     = 0;
    private String trainingTime = null;
    private int numIterations   = 0;

    //-----------------------------------
    public void init () {
        this.learningRule = null;
        this.numPatterns = 0;
        this.trainingTime = null;
        this.numIterations = 0;
    }

    //-----------------------------------
    public void setLearningRule (String learningRule) {
        this.learningRule = new String (learningRule);
    }

    //-----------------------------------
    public void setNumPatterns (int numPatterns) {
        this.numPatterns = numPatterns;
    }

    //-----------------------------------
    public void setTrainingTime (String trainingTime) {
        this.trainingTime = trainingTime;
    }

    //-----------------------------------
    public void setNumIterations (int numIterations) {
        this.numIterations = numIterations;
    }

    //-----------------------------------
    public String getLearningRule () {
        return this.learningRule;
    }

    //-----------------------------------
    public String getNumPatterns () {
        return Integer.toString (this.numPatterns);
    }

    //-----------------------------------
    public String getTrainingTime () {
        return this.trainingTime;
    }

    //-----------------------------------
    public String getNumIterations () {
        return (Integer.toString (this.numIterations));
    }
}
