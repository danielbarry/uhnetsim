#!/bin/bash
#Compile Script

echo "++Running on $1 cores with $2 runs"

#Process for each node
for (( x=0; x<=8; x++ ))
do
  bash core.sh ${x} ${2} &
done
echo "++Terminated"
