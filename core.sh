#!/bin/bash
#Compile Script

echo "##Running on $1 cores with $2 runs"

#Process for each node
for ((s=4; s<=200; s+=4))
do
  #Vary bias
  for ((b=1; b<=9; b++))
  do
    #Vary pattern
    #for ((p=1; p<=30; p++))
    #do
      p=1000 #Test up to a maximum of a 1000 patterns before failure
      bash build.sh -b 0.$b -c -n h -p 0 -r $2 -s $s -z $p ../data${1}.csv
    #done
    echo ">>Bias:0.$b h 0 Runs:$2 Size:$s Patterns:iterated"
  done
done
echo "##Terminated"
